clear;clc;close all;
target_x = 180; 
angle_x = 180;
cnt1 = 0;
Vmax = 30;
Amax = 36;
flag = 0;
if  target_x >= cnt1
    X1 = angle_x - cnt1; 
    flag = 1;
else 
    X1 = cnt1 - angle_x; 
    flag = 0;
end

if X1 >= Vmax^2 / Amax
    Ta1 = Vmax / Amax;
    Time = X1 / Vmax + Ta1;
    v1 = Vmax;
    a1 = Amax;  
 else 
    Ta1 = sqrt(X1 / Amax);
    Time = 2 * Ta1;
    v1 = sqrt(X1 * Amax);
    a1 = Amax; 
end
ttt = int64(Time+1);
for t=0.005:0.005:double(ttt)
    if t <= Ta1 
      a = (a1 / 2.0) * t *t;
      if flag == 1
          A(int64(t*100)) = cnt1 + a;   
      else
          A(int64(t*100)) = cnt1 - a;
      end       
    elseif ((Ta1 < t) && (t <= (Time - Ta1))) ;
      a = v1 * (t - Ta1) + (a1) / 2.0 * Ta1 *Ta1;
      if flag == 1
          A(int64(t*100)) = cnt1 + a;
      else
          A(int64(t*100)) = cnt1 - a;
      end      
    elseif (((Time - Ta1) < t) && (t <= Time )) 
      a =  v1 * (Time - Ta1) - (a1 / 2.0) * (t- Time) * (t- Time);
      if flag == 1
          A(int64(t*100)) = cnt1 + a;
      else
          A(int64(t*100)) = cnt1 - a;
      end                
    elseif t > Time 
        A(int64(t*100)) = angle_x;   
%       if flag == 1
%           A(int64(t*100)) = cnt1 + X1;   
%       else
%           A(int64(t*100)) = cnt1 - X1;   
%       end
      
%       break;
    end   
end 
hold on; grid on;
plot(A,'R')
   
figure
hold on; grid on;
vel_x = [0, Ta1, Time-Ta1, Time];
vel_y = [0, Vmax, Vmax, 0];
plot(vel_x, vel_y, 'R');


%% 假設此軸比較近
target2 = 150; 
angle2 = 150;
cnt2 = 0;
Vmax2 = 20;
Amax2 = 144;
flag2 = 0;

if  target2 >= cnt2
    X2 = angle2 - cnt2; 
    flag2 = 1;
else 
    X2 = cnt2 - angle2; 
    flag2 = 0;
end

% %% 全軸加速度都用遠軸的AMax
% if X2 > X1
%     Amax2 = 144;
% else
%     Amax2 = Amax;
% end
% Ta2 = Time/2 - sqrt(Time^2 / 4 - X2 / Amax2); % = 0.2133
% v2 = Amax2 * Ta2; % 30.7163
% a2 = Amax2; %144

% 最遠的用Amax 其餘推算
Ta2 = Ta1;  % = 0.5
a2 = X2 / (Time*Ta2 - Ta2^2); % = 18
v2 = a2*Ta2; % = 9

%% 模擬
Vmax = Vmax2;
Amax = Amax2;
Ta1 = Ta2;
a1 = a2;
v1 = v2;
cnt1 = cnt2;
flag = flag2 ;
angle_x = angle2;
ttt = int64(Time+1);

for t=0.005:0.005:double(ttt)
    if t <= Ta1 
      a = (a1 / 2.0) * t *t;
      if flag == 1
          A(int64(t*100)) = cnt1 + a;   
      else
          A(int64(t*100)) = cnt1 - a;
      end       
    elseif ((Ta1 < t) && (t <= (Time - Ta1))) ;
      a = v1 * (t - Ta1) + (a1) / 2.0 * Ta1 *Ta1;
      if flag == 1
          A(int64(t*100)) = cnt1 + a;
      else
          A(int64(t*100)) = cnt1 - a;
      end      
    elseif (((Time - Ta1) < t) && (t <= Time )) 
      a =  v1 * (Time - Ta1) - (a1 / 2.0) * (t- Time) * (t- Time);
      if flag == 1
          A(int64(t*100)) = cnt1 + a;
      else
          A(int64(t*100)) = cnt1 - a;
      end                
    elseif t > Time 
        A(int64(t*100)) = angle_x;  
%       if flag == 1
%           A(int64(t*100)) = cnt1 + X1;   
%       else
%           A(int64(t*100)) = cnt1 - X1;   
%       end
      
%       break;
    end   
end 
figure
hold on; grid on;
plot(A,'R')
% 
% figure
% hold on; grid on;
% plot(gradient(A)*100,'R')
   
figure
hold on; grid on;
vel_x = [0, Ta1, Time-Ta1, Time];
vel_y = [0, Vmax, Vmax, 0];
plot(vel_x, vel_y, 'R');
    