clear;clc;close all;
deg2count = [87, 78, 67, 56];
target = [100*deg2count(1), 30*deg2count(2), -45*deg2count(3), 45*deg2count(4)];
angle = [100, 30, -45, 45];
cnt = [0, 0, 0, 0]; %規劃前的當下位置, 不一定是零
X = [0, 0, 0, 0];
s_factor = [1, 1, 1, 1]; % S系數, 輸入0會退化成Tcurve, 輸入1為純S曲線, 非1時設定的值會符合 (Time >= 2*Ta > 0) && (Ta > 2*Ts >=0 )
Vmax = [18, 36, 36, 36]; %最高速度(deg/s) 假設為馬達
Amax = [36, 144, 144, 144]; %最高加速度 (deg/s^2)
flag = [0, 0, 0, 0];
n = 4; % number of axis

for i=1:n
   if  target(i) >= cnt(i)
       X(i) = angle(i) - cnt(i); 
       flag(i) = 1;
   else 
       X(i) = cnt(i) - angle(i); 
       flag(i) = 0;
   end
end

%% S曲線運算, 使用最大加速度、最大速度(最短時間), 給定X1, Vmax, Amax, s_factor
for i=1:n
    Aave(i) = (1 - s_factor(i) / 2) * Amax(i); % 整體der平均加速度           
    if X(i) >= (Vmax(i)^2 / Aave(i)) % 計算區間
        Ta(i) = Vmax(i) / Aave(i); % 等速度開始之時間
        Time(i) = X(i) / Vmax(i) + Ta(i); % 總時間
        Ts(i) = (s_factor(i) / 2) * Ta(i); % 等加速度開始之時間        
    else % 若距離過短導致無法達到最高速就需將Vmax下降一些, 此時值不會是Vmax
        Ta(i) = sqrt(X(i) / Aave(i));
        Time(i) = 2 * Ta(i);
        Ts(i) = (s_factor(i) / 2) * Ta(i); 
    end
    v(i) = X(i) / (Time(i) - Ta(i)); % 驗算最大速度, 若距離過短不會等於Vmax
    a(i) = v(i) / (Ta(i) - Ts(i)); % 驗算最大加速度    
    if Ts(i) == 0  
        j(i) = 0; % 避免計算j時分母為零
    else
        j(i) = Amax(i) / Ts(i); % 急衝度, 此處不是 Aave
    end   
end
%% 比較哪軸總時間長 , 只有單軸的Scurve 故意把第一軸距離設成最遠就好
nBig = 0;  % number of big time axis
if (Time(1) >= Time(2))
    nBig = 1;
elseif (Time(2) >= Time(1))
    nBig = 2;
end
for i=1:n  %% S曲線運算, 給定X1, Time, Ta, Ts(利用遠軸)
    if (i== nBig)
    else
        Ta(i) = Ta(nBig);      % check 賦值
        Ts(i) = (s_factor(i) / 2) * Ta(i); 
        Time(i) = Time(nBig);
        v(i) = X(i) / (Time(i) - Ta(i)); % 給定時間時的最大速度
        a(i) = v(i) / (Ta(i) - Ts(i)); % 給定時間時的最大加速度
    end
    if Ts(i) == 0  
        j(i) = 0; % 避免計算j時分母為零
    else
        j(i) = a(i) / Ts(i); % 急衝度, 此處不是 Aave
    end
end
%% 各物理因子之初始值
for i=1:n
    
    t0(i,:) = [Ts(i), Ta(i)-Ts(i), Ta(i), Time(i) - Ta(i), Time(i) - Ta(i) + Ts(i), Time(i) - Ts(i), Time(i)];
       
    a0(i,:) = [a(i), a(i), 0, 0, -a(i), -a(i), 0];    
    
    v0(i,1) = 0.5*j(i)*t0(i,1)^2; 
    v0(i,2) = a0(i,1)*(t0(i,2)-t0(i,1))+v0(i,1); 
    v0(i,3) = v(i);
    v0(i,4) = v(i);
    v0(i,5) = -0.5*j(i)*(t0(i,5)-t0(i,4))^2 + v0(i,4); 
    v0(i,6) = a0(i,5)*(t0(i,6)-t0(i,5)) + v0(i,5);
    v0(i,7) = 0.5*j(i)*(t0(i,7)-t0(i,6))^2 + a0(i,6)*(t0(i,7)-t0(i,6))+v0(i,6);
    
    p0(i,1) = 1/6*j(i)*t0(i,1)^3; 
    p0(i,2) =0.5*a0(i,1)*(t0(i,2)-t0(i,1))^2 + v0(i,1)*(t0(i,2)-t0(i,1))+p0(i,1);
    p0(i,3) = -(1/6)*j(i)*(t0(i,3)-t0(i,2))^3+0.5*a0(i,2)*(t0(i,3)-t0(i,2))^2+v0(i,2)*(t0(i,3)-t0(i,2))+p0(i,2);
    p0(i,4) = v0(i,3)*(t0(i,4)-t0(i,3))+p0(i,3);
    p0(i,5) = -(1/6)*j(i)*(t0(i,5)-t0(i,4))^3+v0(i,4)*(t0(i,5)-t0(i,4))+p0(i,4);
    p0(i,6) = 0.5*a0(i,5)*(t0(i,6)-t0(i,5))^2+v0(i,5)*(t0(i,6)-t0(i,5))+p0(i,5);
    p0(i,7) = (1/6)*j(i)*(t0(i,7)-t0(i,6))^3+0.5*a0(i,6)*(t0(i,7)-t0(i,6))^2+v0(i,6)*(t0(i,7)-t0(i,6))+p0(i,6);       
end

%% 模擬開始
at=0; % acc tmp
vt=0; % vel tmp
pt=0; % pos tmp
ttt = int64(Time+1);
for t=0:0.005:double(ttt)  
    for i=1:n
        k = int64(t*200+1); %矩陣位址
        if t <= t0(i,1)
            at(i)=j(i)*t;
            vt(i) = 0.5*j(i)*t^2; 
            pt(i) = j(i)*t^3*(1/6); 
            if flag(i) ==1        
            else     
                at(i) = at(i)*-1;
                vt(i) = vt(i)*-1;
                pt(i) = pt(i)*-1;
            end                  
            aa(i,k) = at(i);
            vv(i,k) = vt(i);
            pp(i,k) = cnt(i) + pt(i);
        elseif ((t0(i,1) < t) && (t < t0(i,2)))
            at(i) = a0(i,1); 
            vt(i) = a0(i,1)*(t-t0(i,1))+v0(i,1);
            pt(i) = 0.5*a0(i,1)*(t-t0(i,1))^2 + v0(i,1)*(t-t0(i,1))+p0(i,1);
            if flag(i) == 1           
            else
                at(i) = at(i)*-1;
                vt(i) = vt(i)*-1;
                pt(i) = pt(i)*-1;
            end
            aa(i,k) = at(i);
            vv(i,k) = vt(i);
            pp(i,k) = cnt(i) + pt(i);
        elseif ((t0(i,2) <= t) && (t <= t0(i,3)))
            at(i)=-j(i)*(t-t0(i,2))+a0(i,2); 
            vt(i)=-0.5*j(i)*(t-t0(i,2))^2 + a0(i,2)*(t-t0(i,2)) +v0(i,2); 
            pt(i) = -(1/6)*j(i)*(t-t0(i,2))^3+0.5*a0(i,2)*(t-t0(i,2))^2+v0(i,2)*(t-t0(i,2))+p0(i,2);
            if flag(i) ==1             
            else
                at(i) = at(i)*-1;
                vt(i) = vt(i)*-1;
                pt(i) = pt(i)*-1;
            end       
            aa(i,k) = at(i);
            vv(i,k) = vt(i);
            pp(i,k) = cnt(i) + pt(i);
        elseif ((t0(i,3) < t) && (t < t0(i,4)))
            at(i) = a0(i,3); 
            vt(i) = v0(i,3);
            pt(i) = v0(i,3)*(t-t0(i,3))+p0(i,3);
            if flag(i) ==1            
            else 
                at(i) = at(i)*-1;
                vt(i) = vt(i)*-1;
                pt(i) = pt(i)*-1;
            end                    
            aa(i,k) = at(i);
            vv(i,k) = vt(i);
            pp(i,k) = cnt(i) + pt(i);
        elseif ((t0(i,4) <= t) && (t <= t0(i,5)))   
            at(i)=-j(i)*(t-t0(i,4)) +a0(i,4);
            vt(i)=-0.5*j(i)*(t-t0(i,4))^2 + v0(i,4); 
            pt(i) = -(1/6)*j(i)*(t-t0(i,4))^3+v0(i,4)*(t-t0(i,4))+p0(i,4);        
            if flag(i) ==1
            else            
                at(i) = at(i)*-1;
                vt(i) = vt(i)*-1;
                pt(i) = pt(i)*-1;
            end                   
            aa(i,k) = at(i);
            vv(i,k) = vt(i);        
            pp(i,k) = cnt(i) + pt(i);            
        elseif ((t0(i,5) < t) && (t < t0(i,6)))
            at(i) = a0(i,5); 
            vt(i) = a0(i,5)*(t-t0(i,5)) + v0(i,5);
            pt(i) = 0.5*a0(i,5)*(t-t0(i,5))^2+v0(i,5)*(t-t0(i,5))+p0(i,5);
            if flag(i) ==1            
            else            
                at(i) = at(i)*-1;
                vt(i) = vt(i)*-1;
                pt(i) = pt(i)*-1;
            end        
            aa(i,k) = at(i);
            vv(i,k) = vt(i);        
            pp(i,k) = cnt(i) + pt(i);            
        elseif ((t0(i,6) <= t) && (t <= t0(i,7)))
            at(i)=j(i)*(t-t0(i,6))+a0(i,6);
            vt(i)=0.5*j(i)*(t-t0(i,6))^2 + a0(i,6)*(t-t0(i,6))+v0(i,6);
            pt(i) = (1/6)*j(i)*(t-t0(i,6))^3+0.5*a0(i,6)*(t-t0(i,6))^2+v0(i,6)*(t-t0(i,6))+p0(i,6);
            if flag(i) ==1                        
            else                        
                at(i) = at(i)*-1;
                vt(i) = vt(i)*-1;
                pt(i) = pt(i)*-1;
            end
            aa(i,k) = at(i);
            vv(i,k) = vt(i);
            pp(i,k) = cnt(i) + pt(i);
        elseif t > t0(i,7) 
            pp(i,k) = angle(i);
        end   
    end                    
end 

for i=1:n
    figure(6*i-5)
    hold on; grid on;
    plot(pp(i,:),'R')
    
    figure(6*i-4)
    hold on; grid on;
    time_x(i,:) = [0, t0(i,1), t0(i,2), t0(i,3), t0(i,4), t0(i,5), t0(i,6), t0(i,7)];
    p_y(i,:) = [0, p0(i,1), p0(i,2), p0(i,3), p0(i,4), p0(i,5), p0(i,6), p0(i,7)];
    plot(time_x(i,:), p_y(i,:), 'R-*');
    
    figure(6*i-3)        
    hold on; grid on;
    plot(vv(i,:),'R')
    
    figure(6*i-2)
    hold on; grid on;
    time_x(i,:) = [0, t0(i,1), t0(i,2), t0(i,3), t0(i,4), t0(i,5), t0(i,6), t0(i,7)];
    vel_y(i,:) = [0, v0(i,1), v0(i,2), v0(i,3), v0(i,4), v0(i,5), v0(i,6), v0(i,7)];
    plot(time_x(i,:), vel_y(i,:), 'R-*');
    
    figure(6*i-1)
    hold on; grid on;
    plot(aa(i,:),'R')
    
    figure(6*i-0)
    hold on; grid on;
    time_x(i,:) = [0, t0(i,1), t0(i,2), t0(i,3), t0(i,4), t0(i,5), t0(i,6), t0(i,7)];
    % acc_y(i,:) = [0, Aave(i), Aave(i), 0, 0,-Aave(i), -Aave(i),0];
    acc_y(i,:) = [0, a0(i,1), a0(i,2), a0(i,3), a0(i,4), a0(i,5), a0(i,6), a0(i,7)];
    plot(time_x(i,:), acc_y(i,:), 'R-*');
    
end 

%% S曲線運算, 給定X1, Time, Ta, Ts
% Time = 1.0541;
% Ta = 0.5270 
% Ts = (s_factor / 2) * Ta;
% vmax = X1 / (Time - Ta); % 給定時間時的最大速度
% amax = vmax / (Ta - Ts); % 給定時間時的最大加速度
% if Ts == 0  
%     j = 0; % 避免計算j時分母為零
% else
%     j = amax / Ts; % 急衝度, 此處不是 Aave
% end  
