clear;clc;close all;
%% 設定參數
deg2count(1) = 87;
angle(1) = 30; % reference  for 正數
target(1) = angle(1)*deg2count(1); % deg2count
cnt1(1) = 0;  %規劃前的當下位置, 不一定是零
flag(1) = 0;
if  target(1) >= cnt1(1)
    X1(1) = angle(1) - cnt1(1); 
    flag(1) = 1;
else
    X1(1) = cnt1(1) - angle(1); 
    flag(1) = 0;
end
s_factor(1) = 1;% S系數, 輸入0會退化成Tcurve, 輸入1為純S曲線, 非1時設定的值會符合 (Time >= 2*Ta > 0) && (Ta > 2*Ts >=0 )
%% S曲線運算, 使用最大加速度、最大速度(最短時間), 給定X1, Vmax, Amax, s_factor
Vmax(1) = 18; %最高速度(deg/s) 假設為馬達
Amax(1) = 36; %最高加速度 (deg/s^2)
Aave(1) = (1 - s_factor(1) / 2) * Amax(1); % 整體der平均加速度

target = target(1);
angle = angle(1);
cnt1 = cnt1(1);
X1 = X1(1);
Vmax = Vmax(1);
Amax = Amax(1);
Aave = Aave(1);
s_factor = s_factor(1);
flag = flag(1);

if abs(X1) >= (Vmax^2 / Aave)  % 計算區間
    Ta = Vmax / Aave; % 等速度開始之時間
    Time = abs(X1) / Vmax + Ta; % 總時間
    Ts = (s_factor / 2) * Ta; % 等加速度開始之時間
else % 若距離過短導致無法達到最高速就需將Vmax下降一些, 此時值不會是Vmax
    Ta = sqrt(abs(X1) / Aave);
    Time = 2 * Ta;
    Ts = (s_factor / 2) * Ta; 
end
vmax(1) = X1 / (Time - Ta); % 驗算最大速度, 若距離過短不會等於Vmax
amax(1) = vmax / (Ta - Ts); % 驗算最大加速度
vmax = vmax(1);
amax = amax(1);
if Ts == 0  
    j = 0; % 避免計算j時分母為零
else
    j = Amax / Ts; % 急衝度, 此處不是 Aave
end   
%% S曲線運算, 給定X1, Time, Ta, Ts
% Time = 1.0541;
% Ta = 0.5270 
% Ts = (s_factor / 2) * Ta;
% vmax = X1 / (Time - Ta); % 給定時間時的最大速度
% amax = vmax / (Ta - Ts); % 給定時間時的最大加速度
% if Ts == 0  
%     j = 0; % 避免計算j時分母為零
% else
%     j = amax / Ts; % 急衝度, 此處不是 Aave
% end  
%%
t1 = Ts;
t2 = Ta-Ts;
t3 = Ta;
t4 = Time - Ta;
t5 = Time - Ta + Ts;
t6 = Time - Ts;
t7 = Time;

% at1=j*t1; 
% at2=at1; 
% at3=-j*(t3-t2)+at2; 
% at4=at3; 
% at5=-j*(t5-t4) +at4;
% at6=at5; 
% at7=j*(t7-t6)+at6;

a1=amax; 
a2=amax; 
a3=0; 
a4=0; 
a5=-amax;
a6=-amax;
a7=0;

v1=0.5*j*t1^2; 
v2=a1*(t2-t1)+v1; 
v3=vmax;
v4=vmax;
v5=-0.5*j*(t5-t4)^2 + v4; 
v6=a5*(t6-t5) + v5;
v7=0.5*j*(t7-t6)^2 + a6*(t7-t6)+v6;

p1 = 1/6*j*t1^3; 
p2 =0.5*a1*(t2-t1)^2 + v1*(t2-t1)+p1;
p3 = -(1/6)*j*(t3-t2)^3+0.5*a2*(t3-t2)^2+v2*(t3-t2)+p2;
p4 = v3*(t4-t3)+p3;
p5 = -(1/6)*j*(t5-t4)^3+v4*(t5-t4)+p4;
p6 = 0.5*a5*(t6-t5)^2+v5*(t6-t5)+p5;
p7 = (1/6)*j*(t7-t6)^3+0.5*a6*(t7-t6)^2+v6*(t7-t6)+p6;

at1=0; at2=0; at3=0; at4=0; at5=0; at6=0; at7=0;
vt1=0; vt2=0; vt3=0; vt4=0; vt5=0; vt6=0; vt7=0;
xt1=0; xt2=0; xt3=0; xt4=0; xt5=0; xt6=0; xt7=0;

ttt = int64(Time+1);
for t=0:0.005:double(ttt)  
    k = int64(t*200+1); %矩陣位址
    if t <= t1
        at1=j*t;
        vt1 = 0.5*j*t^2; 
        xt1 = j*t^3*(1/6); 
        if flag ==1        
        else     
            at1 = at1*-1;
            vt1 = vt1*-1;
            xt1 = xt1*-1;
        end                  
        aa(k) = at1;
        vv(k) = vt1;
        xx(k) = cnt1 + xt1;
    elseif ((t1 < t) && (t < t2))
        at2=a1; 
        vt2 = a1*(t-t1)+v1;
        xt2 =0.5*a1*(t-t1)^2 + v1*(t-t1)+p1;
        if flag == 1           
        else
            at2 = at2*-1;
            vt2 = vt2*-1;
            xt2 = xt2*-1;
        end
        aa(k) = at2;
        vv(k) = vt2;
        xx(k) = cnt1 + xt2;
    elseif ((t2 <= t) && (t <= t3)) 
        at3=-j*(t-t2)+a2; 
        vt3=-0.5*j*(t-t2)^2 + a2*(t-t2) +v2; 
        xt3 = -(1/6)*j*(t-t2)^3+0.5*a2*(t-t2)^2+v2*(t-t2)+p2;
        if flag ==1             
        else
            at3 = at3*-1;
            vt3 = vt3*-1;
            xt3 = xt3*-1;
        end       
        aa(k) = at3;
        vv(k) = vt3;
        xx(k) = cnt1 + xt3;
    elseif ((t3 < t) && (t < t4))
        at4=a3; 
        vt4= v3;
        xt4 = v3*(t-t3)+p3;
        if flag ==1            
        else 
            at4 = at4*-1;
            vt4 = vt4*-1;
            xt4 = xt4*-1;
        end                    
        aa(k) = at4;
        vv(k) = vt4;
        xx(k) = cnt1 + xt4;
    elseif ((t4 <= t) && (t <= t5))   
        at5=-j*(t-t4) +a4;
        vt5=-0.5*j*(t-t4)^2 + v4; 
        xt5 = -(1/6)*j*(t-t4)^3+v4*(t-t4)+p4;        
        if flag ==1
        else            
            at5 = at5*-1;
            vt5 = vt5*-1;
            xt5 = xt5*-1;
        end                   
        aa(k) = at5;
        vv(k) = vt5;        
        xx(k) = cnt1 + xt5;
    elseif ((t5 < t) && (t < t6))
        at6=a5; 
        vt6=a5*(t-t5) + v5;
        xt6 = 0.5*a5*(t-t5)^2+v5*(t-t5)+p5;
        if flag ==1            
        else            
            at6 = at6*-1;
            vt6 = vt6*-1;
            xt6 = xt6*-1;
        end        
        aa(k) = at6;
        vv(k) = vt6;        
        xx(k) = cnt1 + xt6;
    elseif ((t6 <= t) && (t <= t7))
        at7=j*(t-t6)+a6;
        vt7=0.5*j*(t-t6)^2 + a6*(t-t6)+v6;
        xt7 = (1/6)*j*(t-t6)^3+0.5*a6*(t-t6)^2+v6*(t-t6)+p6;
        if flag ==1                        
        else                        
            at7 = at7*-1;
            vt7 = vt7*-1;
            xt7 = xt7*-1;
        end
        aa(k) = at7;
        vv(k) = vt7;
        xx(k) = cnt1 + xt7;
    elseif t > t7 
        xx(k) = angle;
%         if flag ==1
%             xx(k) = cnt1 + X1;
%         else
%             xx(k) = cnt1 - X1;
%         end
%       break;
    end   
end 

figure
hold on; grid on;
plot(xx,'R')

figure
hold on; grid on;
time_x = [0, t1, t2, t3, t4, t5, t6, t7];
xd_y = [0, p1, p2, p3, p4, p5, p6, p7];
plot(time_x, xd_y, 'R-*');

figure
hold on; grid on;
plot(vv,'R')

figure
hold on; grid on;
time_x = [0, t1, t2, t3, t4, t5, t6, t7];
vel_y = [0, v1, v2, v3, v4, v5, v6, v7];
plot(time_x, vel_y, 'R-*');

figure
hold on; grid on;
plot(aa,'R')

figure
hold on; grid on;
time_x = [0, t1, t2, t3, t4, t5, t6, t7];
% acc_y = [0, Aave, Aave, 0, 0,-Aave, -Aave,0];
acc_y = [0, a1, a2, a3, a4, a5, a6, a7];
plot(time_x, acc_y, 'R-*');

%% 以下只是複製上面的 嘗試多軸同動Scurve, 假設上面比較遠
%% 設定參數
deg2count(2) = 40.13;
angle(2) = 15; % reference  for 正數
target(2) = angle(2)*deg2count(2); % deg2count
cnt1(2) = 0;  %規劃前的當下位置, 不一定是零
flag(2) = 0;
if  target(2) >= cnt1(2)
    X1(2) = angle(2) - cnt1(2); 
    flag(2) = 1;
else
    X1(2) = cnt1(2) - angle(2); 
    flag(2) = 0;
end
s_factor(2) = 0;% S系數, 輸入0會退化成Tcurve, 輸入1為純S曲線, 非1時設定的值會符合 (Time >= 2*Ta > 0) && (Ta > 2*Ts >=0 )
%% S曲線運算, 使用最大加速度、最大速度(最短時間), 給定X1, Vmax, Amax, s_factor
Vmax(2) = 36; %最高速度(deg/s) 假設為馬達
Amax(2) = 144; %最高加速度 (deg/s^2)
Aave(2) = (1 - s_factor(2) / 2) * Amax(2); % 整體der平均加速度

% if abs(X1) >= (Vmax^2 / Aave)  % 計算區間
%     Ta = Vmax / Aave; % 等速度開始之時間
%     Time = abs(X1) / Vmax + Ta; % 總時間
%     Ts = (s_factor / 2) * Ta; % 等加速度開始之時間
% else % 若距離過短導致無法達到最高速就需將Vmax下降一些, 此時值不會是Vmax
%     Ta = sqrt(abs(X1) / Aave);
%     Time = 2 * Ta;
%     Ts = (s_factor / 2) * Ta; 
% end
% vmax = X1 / (Time - Ta); % 驗算最大速度, 若距離過短不會等於Vmax
% amax = vmax / (Ta - Ts); % 驗算最大加速度
% 
% if Ts == 0  
%     j = 0; % 避免計算j時分母為零
% else
%     j = Amax / Ts; % 急衝度, 此處不是 Aave
% end   
%% 假設第一軸比較遠  (放棄推導XD)
% Ta(2) = Ta;
% amax(2) = X1(2) / Time*Ta(2) - Ta(2)^2;
% vmax(2) = amax(2) * Ta(2);
% Ts(2) = (s_factor(2) / 2) * Ta(2); 

% Ta(2) = Time/2 -sqrt( Time^2/4 - X1(2) / Amax(2) )
% vmax(2) = Amax(2) * Ta(2);
% amax(2) = Amax(2);]
% Ts(2) = (s_factor(2) / 2) * Ta(2); 
% 
% Ts(2) = Ts;
% Ta(2) = Ts(2) / (s_factor(2) / 2);
% amax(2) = X1(2) / Time*Ta(2) - Ta(2)^2;
% vmax(2) = amax(2) * Ta(2);

% Ta(2) = Time/2 -sqrt( Time^2/4 - X1(2) / Amax(2) );
% Ts(2) = (s_factor(2) / 2) * Ta(2); 
% amax(2) = X1(2) / Time*Ta(2) - Ta(2)^2;
% vmax(2) = amax(2) * Ta(2);

% if Ts(2) == 0  
%     j(2) = 0; % 避免計算j時分母為零
% else
%     j(2) = amax(2) / Ts(2); % 急衝度, 此處不是 Aave
% end 

%% 假設第一軸比較遠, S曲線運算, 給定X1, Time, Ta, Ts(利用遠軸)
Ta(2) = Ta;
Ts(2) = (s_factor(2) / 2) * Ta(2); 
vmax(2) = X1(2) / (Time - Ta(2)); % 給定時間時的最大速度
amax(2) = vmax(2) / (Ta(2) - Ts(2)); % 給定時間時的最大加速度
if Ts(2) == 0  
    j(2) = 0; % 避免計算j時分母為零
else
    j(2) = amax(2) / Ts(2); % 急衝度, 此處不是 Aave
end
%%
Ta = Ta(2);
Ts = Ts(2);
target = target(2);
angle = angle(2);
cnt1 = cnt1(2);
X1 = X1(2);
vmax = vmax(2);
amax = amax(2);
Aave = Aave(2);
s_factor = s_factor(2);
flag = flag(2);
j = j(2);


%% S曲線運算, 給定X1, Time, Ta, Ts
% Time = 1.0541;
% Ta = 0.5270 
% Ts = (s_factor / 2) * Ta;
% vmax = X1 / (Time - Ta); % 給定時間時的最大速度
% amax = vmax / (Ta - Ts); % 給定時間時的最大加速度
% if Ts == 0  
%     j = 0; % 避免計算j時分母為零
% else
%     j = amax / Ts; % 急衝度, 此處不是 Aave
% end  
%%
t1 = Ts;
t2 = Ta-Ts;
t3 = Ta;
t4 = Time - Ta;
t5 = Time - Ta + Ts;
t6 = Time - Ts;
t7 = Time;

% at1=j*t1; 
% at2=at1; 
% at3=-j*(t3-t2)+at2; 
% at4=at3; 
% at5=-j*(t5-t4) +at4;
% at6=at5; 
% at7=j*(t7-t6)+at6;

a1=amax; 
a2=amax; 
a3=0; 
a4=0; 
a5=-amax;
a6=-amax;
a7=0;

v1=0.5*j*t1^2; 
v2=a1*(t2-t1)+v1; 
v3=vmax;
v4=vmax;
v5=-0.5*j*(t5-t4)^2 + v4; 
% v5=0.5*j*(t5-t4)^2; 
v6=a5*(t6-t5) + v5;
v7=0.5*j*(t7-t6)^2 + a6*(t7-t6)+v6;

p1 = 1/6*j*t1^3; 
p2 =0.5*a1*(t2-t1)^2 + v1*(t2-t1)+p1;
p3 = -(1/6)*j*(t3-t2)^3+0.5*a2*(t3-t2)^2+v2*(t3-t2)+p2;
p4 = v3*(t4-t3)+p3;
p5 = -(1/6)*j*(t5-t4)^3+v4*(t5-t4)+p4;
p6 = 0.5*a5*(t6-t5)^2+v5*(t6-t5)+p5;
p7 = (1/6)*j*(t7-t6)^3+0.5*a6*(t7-t6)^2+v6*(t7-t6)+p6;

at1=0; at2=0; at3=0; at4=0; at5=0; at6=0; at7=0;
vt1=0; vt2=0; vt3=0; vt4=0; vt5=0; vt6=0; vt7=0;
xt1=0; xt2=0; xt3=0; xt4=0; xt5=0; xt6=0; xt7=0;

ttt = int64(Time+1);
for t=0:0.005:double(ttt)  
    k = int64(t*200+1); %矩陣位址
    if t <= t1
        at1=j*t;
        vt1 = 0.5*j*t^2; 
        xt1 = j*t^3*(1/6); 
        if flag ==1        
        else     
            at1 = at1*-1;
            vt1 = vt1*-1;
            xt1 = xt1*-1;
        end                  
        aa(k) = at1;
        vv(k) = vt1;
        xx(k) = cnt1 + xt1;
    elseif ((t1 < t) && (t < t2))
        at2=a1; 
        vt2 = a1*(t-t1)+v1;
        xt2 =0.5*a1*(t-t1)^2 + v1*(t-t1)+p1;
        if flag == 1           
        else
            at2 = at2*-1;
            vt2 = vt2*-1;
            xt2 = xt2*-1;
        end
        aa(k) = at2;
        vv(k) = vt2;
        xx(k) = cnt1 + xt2;
    elseif ((t2 <= t) && (t <= t3)) 
        at3=-j*(t-t2)+a2; 
        vt3=-0.5*j*(t-t2)^2 + a2*(t-t2) +v2; 
        xt3 = -(1/6)*j*(t-t2)^3+0.5*a2*(t-t2)^2+v2*(t-t2)+p2;
        if flag ==1             
        else
            at3 = at3*-1;
            vt3 = vt3*-1;
            xt3 = xt3*-1;
        end       
        aa(k) = at3;
        vv(k) = vt3;
        xx(k) = cnt1 + xt3;
    elseif ((t3 < t) && (t < t4))
        at4=a3; 
        vt4= v3;
        xt4 = v3*(t-t3)+p3;
        if flag ==1            
        else 
            at4 = at4*-1;
            vt4 = vt4*-1;
            xt4 = xt4*-1;
        end                    
        aa(k) = at4;
        vv(k) = vt4;
        xx(k) = cnt1 + xt4;
    elseif ((t4 <= t) && (t <= t5))   
        at5=-j*(t-t4) +a4;
        vt5=-0.5*j*(t-t4)^2 + v4; 
        xt5 = -(1/6)*j*(t-t4)^3+v4*(t-t4)+p4;        
        if flag ==1
        else            
            at5 = at5*-1;
            vt5 = vt5*-1;
            xt5 = xt5*-1;
        end                   
        aa(k) = at5;
        vv(k) = vt5;        
        xx(k) = cnt1 + xt5;
    elseif ((t5 < t) && (t < t6))
        at6=a5; 
        vt6=a5*(t-t5) + v5;
        xt6 = 0.5*a5*(t-t5)^2+v5*(t-t5)+p5;
        if flag ==1            
        else            
            at6 = at6*-1;
            vt6 = vt6*-1;
            xt6 = xt6*-1;
        end        
        aa(k) = at6;
        vv(k) = vt6;        
        xx(k) = cnt1 + xt6;
    elseif ((t6 <= t) && (t <= t7))
        at7=j*(t-t6)+a6;
        vt7=0.5*j*(t-t6)^2 + a6*(t-t6)+v6;
        xt7 = (1/6)*j*(t-t6)^3+0.5*a6*(t-t6)^2+v6*(t-t6)+p6;
        if flag ==1                        
        else                        
            at7 = at7*-1;
            vt7 = vt7*-1;
            xt7 = xt7*-1;
        end
        aa(k) = at7;
        vv(k) = vt7;
        xx(k) = cnt1 + xt7;
    elseif t > t7 
        xx(k) = angle;
%         if flag ==1
%             xx(k) = cnt1 + X1;
%         else
%             xx(k) = cnt1 - X1;
%         end
%       break;
    end   
end 

figure
hold on; grid on;
plot(xx,'R')

figure
hold on; grid on;
time_x = [0, t1, t2, t3, t4, t5, t6, t7];
xd_y = [0, p1, p2, p3, p4, p5, p6, p7];
plot(time_x, xd_y, 'R-*');

figure
hold on; grid on;
plot(vv,'R')

figure
hold on; grid on;
time_x = [0, t1, t2, t3, t4, t5, t6, t7];
vel_y = [0, v1, v2, v3, v4, v5, v6, v7];
plot(time_x, vel_y, 'R-*');

figure
hold on; grid on;
plot(aa,'R')

figure
hold on; grid on;
time_x = [0, t1, t2, t3, t4, t5, t6, t7];
% acc_y = [0, Aave, Aave, 0, 0,-Aave, -Aave,0];
acc_y = [0, a1, a2, a3, a4, a5, a6, a7];
plot(time_x, acc_y, 'R-*');

