clear;clc;close all;
deg2count = [87, 78];
target = [180*deg2count(1), 45*deg2count(2)];
angle = [180, 45];
cnt = [0, 0];
X = [0, 0];
Vmax = [18, 36]; %30, 20
Amax = [36, 144]; %36, 144
flag = [0, 0];
n = 2; % number of axis,  
 
for i=1:n
   if  target(i) >= cnt(i);
       X(i) = angle(i) - cnt(i); 
       flag(i) = 1;
   else 
       X(i) = cnt(i) - angle(i); 
       flag(i) = 0;
   end
end

for i=1:n
   if X(i) >= Vmax(i)^2 / Amax(i)
       Ta(i) = Vmax(i) / Amax(i);
       Time(i) = X(i) / Vmax(i) + Ta(i);
       v(i) = Vmax(i);       
   else
       Ta(i) = sqrt(X(i) / Amax(i));
       Time = 2 * Ta(i);
       v(i) = sqrt(X(i) * Amax(i));       
   end
   a(i) = Amax(i);
end

% 只有單軸的Tcurve 故意把第一軸距離設成最遠就好
nBig = 0;  % number of big time axis
if (Time(1) >= Time(2))
    nBig = 1;
elseif (Time(2) >= Time(1))
    nBig = 2;
end

for i=1:n
    if (i== nBig)
    else
        Ta(i) = Ta(nBig);      % check 賦值
        Time(i) = Time(nBig);
        a(i) = X(i) / (Time(i)*Ta(i) - Ta(i)^2);
        v(i) = a(i) * Ta(i);
    end
end


ttt = int64(Time(i));
for t=0:0.005:double(ttt)
    for i=1:n
        k = int64(t*200+1); %矩陣位址
        if t <= Ta(i) 
            at(i) = (a(i) / 2.0) * t *t;
            if flag(i) == 1
                A(i, k) = cnt(i) + at(i);  
            else
                A(i, k) = cnt(i) - at(i);
            end
        elseif ((Ta(i) < t) && (t <= (Time(i) - Ta(i))))
            at(i) = v(i) * (t - Ta(i)) + (a(i)) / 2.0 * Ta(i) *Ta(i);
            if flag(i) == 1
                A(i, k) = cnt(i) + at(i);  
            else
                A(i, k) = cnt(i) - at(i)
            end      
        elseif (((Time(i) - Ta(i)) < t) && (t <= Time(i) )) 
            at(i) =  v(i) * (Time(i) - Ta(i)) - (a(i) / 2.0) * (t- Time(i)) * (t- Time(i)); 
            if flag(i) == 1
                A(i, k) = cnt(i) + at(i);  
            else
                A(i, k) = cnt(i) - at(i)
            end
        elseif t > Time(i)
            A(i, k) = angle(i);   
%             if flag(i) == 1
%                 A(i, k) = cnt(i) + X(i);   
%             else
%                 A(i, k) = cnt(i) - X(i);   
%             end
%             break;
        end   
    end
end

for i=1:2
    figure(2*i-1)
    hold on; grid on;
    plot(A(i,:),'R')
    figure(2*i)
    hold on; grid on;    
    vel_x(i,:) =[0, Ta(i), Time(i)-Ta(i), Time(i)];
    vel_y(i,:) = [0, Vmax(i), Vmax(i), 0];
    plot(vel_x(i,:), vel_y(i,:), 'R');
        
end  



    