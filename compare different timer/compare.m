%%
clear;clc;close all;
a = load('#1.txt');
b = load('#3.txt');

tref1 = a(:,2);
tref2 = b(:,2);
figure
hold on; grid on;
plot(tref1, 'r');
plot(tref2, 'b');
%%
a = load('#5.txt');
b = load('#7.txt');

tref1 = a(:,2);
tref2 = b(:,2);
figure
hold on; grid on;
plot(tref1, 'r');
plot(tref2, 'b');
plot(tref1 - tref2, 'g')
%%
a = load('#1.txt');
b = load('#5.txt');

tref1 = a(:,2);
tref2 = b(:,2);
figure
hold on; grid on;
plot(tref1, 'r');
plot(tref2, 'b');
%%
a = load('#1.txt');
b = load('#7.txt');

tref1 = a(:,2);
tref2 = b(:,2);
figure
hold on; grid on;
plot(tref1, 'r');
plot(tref2, 'b');