#include "robot_arm.h"
//變數排列順序有稍微照著程式執行順序
//Uart receive IRQ
volatile alt_u8 txdata = 0; //分別用來保存需要寫入到uart IP核的一些數據, 發送數據, alt_u8 = unsigned char 
volatile alt_u8 rxdata = 0; //以及保存需要從uart IP核裡面讀出的一個數據, 接收數據, 給全域可以方便debug
const alt_u8 receive_finish = '!';  //接收完成的特殊功能符號
volatile int flag_size = 0;
volatile int data_size = 0;
volatile int scan_idx = 0;
volatile alt_u8 uart_recevie_list[URM][URN];  //先浪費記憶體  有機會再改寫法ㄏ abcde[0] 為0  這樣之後joint1 for index[1]
volatile int URMT = 1; //Uart Receive M Temp
volatile int flag_uart_receive_finish = 0;
//Updata reference value from UART
int plus_or_negative = 0;
volatile int float_index = 0;
volatile int flag_float_index = 0;
volatile float double_data[6] = {0};  //保留小數點後兩位且含小數點最多6位數, 也就是最多到百位數999.99
volatile float double_final_value = 0.0;
volatile float refVal_list[RVM][RVN]; 
volatile float pendulum_degree = 0.0;
volatile int flag_uart_upd_ref_finish = 0;

volatile int edge_capture;
volatile int cnt_isr_Xms = 1;  //第一次執行IRQ 當然是5ms後的事情...
volatile int flag_print = 0;
volatile int time_print = 1200;  //default, 5ms * 1200 = 6s, 
volatile double time;  //Timer內的執行時間
int LR_FB_tmp = 1;  //ISR利用LR_tmp, No_tmp進行控制(維持)某軸時, 其他軸想透過Forward, Backward移動但不得更改到LR_tmp, No_tmp與之對應所新增的變數
int No_FB_tmp = 1;  //也因此在SingleControlMenu所設定的即從LR_tmp, No_tmp改為LR_FB_tmp, No_FB_tmp
int LR_tmp = 1;  //arm No 手臂編號, default:right arm
int No_tmp = 1;  //joint No 關節編號
int MoveSingleJoint = 1;  //default 單軸
float TIME = 0.0;  //Tcurve、Scurve 規劃總時間, 花最久時間的軸的總時間, ISR內統一用此比較不另行Time = pmotor->Time
float fsmc_vector[11] = {[0] = 0.0, 0.0};

volatile int led_light = 1;
float sample_time = 0.02;  //0.005, 0.01, 0.02
float control_freq = 50; // 1 / 0.02 = 50

//想不開再用, 想要5ms可打印也可控制,就把值存下來, 但是秒數會限制 ex: 0.005s * 2000 = 10s, 最多存到10秒, 可以存更久但總記憶體大小只有400KB爆掉的話編譯時會提醒你
//先用全域的宣告編譯, 看是否會超過400Kytes, 防副程式中堆疊的溢出, 目前不會有動態新增, 所以也可以直接全域變數XD, 目前假設雙手
//存起來再print只使用於UartTest, SingleJoint, ArmMove都是直接printf出來, 由SingleJoint驗證速度加速度用matlab後處理, ArmMove只printf count, err, controlValue

/********************************UART配置區********************************/
/********************************UART-配置一*******************************/
/* 
 * 用於 雙手同動儲存8軸的值, URDT需改1500
 * Info: (robot_arm.elf) 350 KBytes program size (code + initialized data).
 * Info:                 49 KBytes free for stack + heap.
 * 使用UART-配置一時
 * 需註解"////////////UART-配置二////////////"雙引號內框起來的頭尾處, 善用ctrl+F
 */

////////////記憶體存放註解區-配置一////////////
/*
volatile int num = 0;
//volatile float Tref_arr[2][4][1000];  //只存前四軸(移動關節) 旋轉關節可待print完前四再做控制
volatile float time_arr[URDT] = {0.0};  //0.02*1500 = 30秒, 若不夠(無法收斂)再將time也後處理
volatile int count_arr[2][4][URDT];  // int max  -2147483648 ~ 2147483647
volatile int error_arr[2][4][URDT];  
//volatile short diff_arr[2][4][1000];  // short max -32768 ~ 32767
//volatile short error_diff_arr[2][4][1000];
//volatile short diff_d_arr[2][4][1000];
//volatile short error_diff_d_arr[2][4][1000];   
volatile short controlVal_arr[2][4][URDT]; //需注意J2控制量, 大於50%就會溢位然後就可能發生無法預期的事情 ㄏㄏ 
volatile float Tref_arr[2][4][URDT];  //為了方便, 就不用在TX2另外用fstream存.txt檔了
*/
////////////記憶體存放註解區-配置一////////////

/********************************UART-配置二*******************************/
/*
 * 用於以下兩個case用, URDT需改12000或20000
 * segway速度及位置控制用, 因只需動左手第二關節
 * segway平衡控制時實驗多軸同動工作手Tcurve及Scurve, 共需動左手第二關節, 右手一二三四關節, 此實驗主要是看平衡手控制效果及segway的狀態故也只存左手第二軸
 * Info: (robot_arm.elf) 343 KBytes program size (code + initialized data).
 * Info:                 56 KBytes free for stack + heap.
 * 使用UART-配置二時
 * 需註解"////////////記憶體存放註解區-配置一////////////"雙引號內框起來的頭尾處, 善用ctrl+F
 * 其單軸控制程式獨立出來, 為SingleJointInit_UART, SingleJoint_FSMC_UART_ISR
 */

////////////記憶體存放註解區-配置二////////////

volatile int num = 0;
//volatile float time_arr[URDT] = {0.0};  //12000*0.02=240sec, 若不夠(無法收斂)再將time也後處理
//volatile int count_arr[URDT];  // int max  -2147483648 ~ 2147483647
//volatile int error_arr[URDT];    
//volatile short controlVal_arr[URDT]; //需注意J2控制量, 大於50%就會溢位然後就可能發生無法預期的事情 ㄏㄏ 

////volatile float time_arr[URDT] = {0.0};  //time後處理, 20000*0.002=40sec
volatile int count_arr[URDT]; 
volatile int error_arr[URDT];    
volatile short controlVal_arr[URDT]; 

////////////記憶體存放註解區-配置二////////////

/********************************UART配置區********************************/

volatile float fuck_u_tmp = 0;  //for FSMC Controller tmp u
volatile int fuck_PD_tmp = 0;
volatile float integral = 0;
volatile float integral_old = 0;
volatile float integral_ant = 0;
volatile float integral_max = 4095*0.2;

volatile int p_tmp = 0;
volatile int i_tmp = 0;
volatile int i_anti_tmp = 0;
volatile int d_tmp = 0;

volatile int XXX = 0;
float weight=0.0,weight_sum=0.0;

/*********************************
E6C2-C 歐姆龍編碼器
  count: 1*2000*4 = 8000
  count/degree: 8000/360.0 = 22.2222  
maxon DCX26L GB KL 24V: 
  count: 987*1024*4 = 4042752 
  count/degree: 4042752/360.0 = 11229.86666666667 * 2  = 22459.73333333334
  pwm freq:195.3125kHz
祥儀IG360516 12V:                                
  count: 516*7*4 = 14448                     231*1024*4
  count/degree: 14448/360.0 = 40.133333333.. 
  pwm freq:762.939453125Hz
maxon DC-max22S GB SL 24V: 
  count: 690*512*4 = 946176 
  count/degree: 946176/360.0 = 2628.2666666666666666666666666667
  pwm freq:12.20703125kHz
maxon DC-max22S GB SL 24V: 
  count: 231*1024*4 = 946176 
  count/degree: 946176/360.0 = 2628.2666666666666666666666666667. 
  pwm freq:12.20703125kHz
祥儀IG220370 24V: 
  count: 370*3*4 = 4440 
  count/degree: 4440/360.0 = 12.333333333.. 
  pwm freq:762.939453125Hz
祥儀IG220370 24V: 
  count: 370*3*4 = 4440 
  count/degree: 4440/360.0 = 12.333333333..   
  pwm freq:762.939453125Hz
***********************************/

/*********************************
舊的3.4軸
maxon RE-max21 GB 2WE 24V: 
  count: 690*512*4 = 1413120 
  count/degree: 1413120/360.0 = 3925.3333333... 
  pwm freq:6.103515625kHz
maxon A-max26 GB 2WE 24V: 
  count: 690*512*4 = 1413120 
  count/degree: 1413120/360.0 = 3925.3333333... 
  pwm freq:6.103515625kHz
  
***********************************/

/***********************************
 * 面向前面(camera視角) 以復歸姿勢來看
 * Forward   
 *    Left  Right
 * 1   
 * 2  往外  往內
 * 3
 * 4  往上  往下
 * 5  逆時針 逆時針
 * 6  張開  張開
 * 
 * Backward
 *    Left  Right
 * 1  
 * 2  往內  往外
 * 3  
 * 4  往下  往上
 * 5  順時針 順時針
 * 6  夾緊  夾緊
 * 
 *  x, 50, 35, 20, 10, 10  minPwm 大概抓ㄉ
 *  x, 40, 20, 15, 15
 * *///////////////////////////////

//左邊的除了第一軸其他軸的dir_forward、dir_backward要自己調 我當時馬達送修 ㄏㄏ 索性不接
Motor motorL1 = {
  'L', 1, 1, 255, 0, 1, 0, 255*0.05, 255*0.5, 0.0, 180.0,
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
  0.5, 10.0, 10.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0, 0.04, 1.0,
  0.02, 0.001, 0.02, 0.002, 0.0, 0.0, 255*0.1,
  0.0, 0.0, 18.0, 9.0, 0.0, 0.0, 1.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID,  
  deg2count1L, MOTOR_L_DIR1_BASE, MOTOR_L_PWM1_BASE, MOTOR_L_COUNT1_BASE, MOTOR_L_RST_COUNT1_BASE
};
Motor motorL2 = {//65535*0.175
  'L', 2, 2, 65535, 0, 1, 0, 0, 65535*0.7, -20.0, 45.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 65535*1.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 36.0, 144.0, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count2, MOTOR_L_DIR2_BASE, MOTOR_L_PWM2_BASE, MOTOR_L_COUNT2_BASE, MOTOR_L_RST_COUNT2_BASE
};
Motor motorL3 = {//4095*0.01
  'L', 3, 4, 4095, 1, 0, 0, 0, 4095*0.5, -90.0, 0.0,
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 36.0, 144.0, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count3, MOTOR_L_DIR3_BASE, MOTOR_L_PWM3_BASE, MOTOR_L_COUNT3_BASE, MOTOR_L_RST_COUNT3_BASE
};
Motor motorL4 = {//4095*0.01
  'L', 4, 8, 4095, 1, 0, 0, 0, 4095*0.5, 0.0, 105.0,
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,  
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 36.0, 144.0, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count4, MOTOR_L_DIR4_BASE, MOTOR_L_PWM4_BASE, MOTOR_L_COUNT4_BASE, MOTOR_L_RST_COUNT4_BASE
};
Motor motorL5 = {//65535*0.01
  'L', 5, 16, 65535, 1, 0, 0, 0, 65535*0.5, 0.0, 180.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 40.0, 40.0, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count5, MOTOR_L_DIR5_BASE, MOTOR_L_PWM5_BASE, MOTOR_L_COUNT5_BASE, MOTOR_L_RST_COUNT5_BASE
};
Motor motorL6 = {//65535*0.01
  'L', 6, 32, 65535, 1, 0, 0, 0, 65535*0.5, 0.0, 0.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 40.0, 40.0, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Abs, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count6, MOTOR_L_DIR6_BASE, MOTOR_L_PWM6_BASE, MOTOR_L_COUNT6_BASE, MOTOR_L_RST_COUNT6_BASE
};
 
Motor motorR1 = {//255*0.05
  'R', 1, 64, 255, 0, 1, 0, 0, 255*0.7, 0.0, 180.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,  
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 255*1.0, 0.0, 0.1, 
  0.5, 7.0, 4.5, 0.04, 0.0, 0.0, 255*0.1,
  0.0, 0.0, 18.0, 9.0, 0.0, 0.0, 1.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count1, MOTOR_R_DIR1_BASE, MOTOR_R_PWM1_BASE, MOTOR_R_COUNT1_BASE, MOTOR_R_RST_COUNT1_BASE
};
Motor motorR2 = {//65535*0.175
  'R', 2, 128, 65535, 0, 1, 0, 0, 65535*0.8, -20.0, 45.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,  
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 65535*1.0, 0.0, 1.0,
  2000.0, 0.0, 2000.0, 0.0, 0.0, 0.0, 65535*0.1,
  0.0, 0.0, 12.0, 24.0, 0.0, 0.0, 1.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count2, MOTOR_R_DIR2_BASE, MOTOR_R_PWM2_BASE, MOTOR_R_COUNT2_BASE, MOTOR_R_RST_COUNT2_BASE
};
Motor motorR3 = {//4095*0.1
  'R', 3, 256, 4095, 1, 0, 1, 0, 4095*0.7, -90.0, 0.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,  
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 4095*1.0, 0.0, 0.1,
  0.2, 0.08, 0.8, 0.1, 0.0, 0.0, 4095*0.1,
  0.0, 0.0, 36.0, 36.0, 0.0, 0.0, 1.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Abs, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count3, MOTOR_R_DIR3_BASE, MOTOR_R_PWM3_BASE, MOTOR_R_COUNT3_BASE, MOTOR_R_RST_COUNT3_BASE
};
Motor motorR4 = {//4095*0.25
  'R', 4, 512, 4095, 1, 0, 1, 0, 4095*0.7, 0.0, 105.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 4095*1.0, 0.0, 0.1,  
  0.3, 0.08, 0.6, 0.1, 0.0, 0.0, 4095*0.1,
  0.0, 0.0, 36.0, 36.0, 0.0, 0.0, 1.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Abs, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count4, MOTOR_R_DIR4_BASE, MOTOR_R_PWM4_BASE, MOTOR_R_COUNT4_BASE, MOTOR_R_RST_COUNT4_BASE
};
Motor motorR5 = {//65535*0.01
  'R', 5, 1024, 65535, 1, 0, 1, 0, 65535*0.5, 0.0, 180.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 40.0, 40.0, 0.0, 0.0, 1.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Rel, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count5, MOTOR_R_DIR5_BASE, MOTOR_R_PWM5_BASE, MOTOR_R_COUNT5_BASE, MOTOR_R_RST_COUNT5_BASE
};
Motor motorR6 = {//65535*0.01
  'R', 6, 2048, 65535, 1, 0, 1, 0, 65535*0.5, 0.0, 0.0, 
  0.0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,  
  0.0, 0.0, 0.0, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 40.0, 40.0, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, {0.0}, {0.0}, {0.0}, {0.0},
  FSMC_Mode_Abs, FSMC_ruleSym, PID_Mode_PID, Control_Mode_PID, 
  deg2count6, MOTOR_R_DIR6_BASE, MOTOR_R_PWM6_BASE, MOTOR_R_COUNT6_BASE, MOTOR_R_RST_COUNT6_BASE
};

PtrMotor motor_list[2][7] = {
  {0, &motorL1, &motorL2, &motorL3, &motorL4, &motorL5, &motorL6},
  {0, &motorR1, &motorR2, &motorR3, &motorR4, &motorR5, &motorR6}
};

//-------------------Menu set---------------------------------------
static void MenuBegin( alt_8 *title )
{
  //printf("\n\n");
  printf("----------------------------------\n");
  printf("Nios II Board Diagnostics\n");
  printf("----------------------------------\n");
  printf(" %s\n",title);
}

/**********************************************************************
 * static void MenuItem( alt_8 letter, alt_8 *string )
 * 
 * Function to define a menu entry.
 *  - Maps a character (defined by 'letter') to a description string
 *    (defined by 'string').
 *
 **********************************************************************/
static void MenuItem( alt_8 letter, alt_8 *name )
{
  printf("     %c:  %s\n" ,letter, name);
}

/* void MenuEnd(alt_u8 lowLetter, alt_u8 highLetter)
 * 
 * Function which defines the menu exit/end conditions.
 *  In this context, the character 'q' always means 'exit'.
 *    The loop, which grabs input from STDIN (via the getchar() function)
 *    continues until either a 'q' or a character outside of the 
 *    range, enclosed by 'lowLetter' and 'highLetter', is reached.
 */
 
static int MenuEnd( alt_u8 lowLetter, alt_u8 highLetter )
{
  static int ch = 23;
  printf("     q:  Exit\n");
  printf("----------------------------------\n");
  printf("\nSelect Choice (%c-%c): [Followed by <enter>] ",lowLetter,highLetter);
  while ((ch = getchar()) >= 0)
  {
    if( ch >= 'A' && ch <= 'Z' )
      ch += 'a' - 'A';
    if( ch == 27 )
      ch = 'q';
    if(ch == 'q' || ( ch >= lowLetter && ch <= highLetter ))
    {
      printf( "%c\n", ch);
      break;
    }    
  }
  return (alt_u8) ch;
}
//------------------------Menu set end-------------------------------------
void put_char( char c )
{
volatile int * JTAG_UART_ptr = (int *) 0x00101510; // JTAG UART address
int control;
control = *(JTAG_UART_ptr + 1); // read the JTAG_UART control register
if (control & 0xFFFF0000) // if space, write character, else ignore
*(JTAG_UART_ptr) = c;
}


int main()
{  
  //printf("%x, %x, %x, %x \n", &motorL1, &motorR3, motor_list[L][1], motor_list[R][3]);
  //printf("%f, %f, %f, %f \n", motorL1.deg2count, motorR3.deg2count, motor_list[L][1]->deg2count, motor_list[R][3]->deg2count);
  
  //* Declare variable for received character. 
  int ch;
  Init_PWM(); //reset pwm value  
  //put_char('o');
//  int tt = 65536;
//  int tt2 = 65540;
//  int tt3 = 2147483647;
//  int tt4 = 2147483650;
//  int tt5 = -2147483647;
//  printf("%d %d %d %d %d\n", tt, tt2, tt3, tt4, tt5);  //, int max  -2147483648 ~ 2147483647
//  short ff = 65536;
//  short ff2 = 65535;
//  short ff3 = -65536;
//  short ff4 = -65535;
//  printf("%d %d %d %d \n", ff, ff2, ff3, ff4);  //, short max -32768 ~ 32767
  
  while(1)
  {       
    ch = TopMenu();
    if (ch == 'q')
    {
      printf("\nExiting from Board Diagnostics.\n");
      // Send EOT to nios2-terminal on the other side of the link.
      printf("%c",EOT);
      break;
    }
  }
  
  return 0;
}


  
static alt_u8 TopMenu(void)
{  
  alt_u8 ch;     
  /* Output the top-level menu to STDOUT */  
  while (1)
  {
    MenuBegin("Main Menu");
    MenuItem( 'a', "Single Initial Menu");
    MenuItem( 'b', "Arm control Menu");  //single arm, 下位機PTP、軌跡(若之後有人生出運動學)    
    MenuItem( 'c', "Uart Receive TX2");  
    MenuItem( 'd', "Printf test");
    MenuItem( 'e', "Code Cal Time");
        
    ch = MenuEnd( 'a', 'h' );  
    switch(ch)
    {
      MenuCase('a',SingleControlMenu);
      MenuCase('b',ArmMenu);
      MenuCase('c',UartReceiveTX2);
      MenuCase('d',PrintfTest);
      MenuCase('e',CodeCalTime);
    }
  
    if (ch == 'q') break;      
  }
  return( ch );
}

static void SingleControlMenu(void)
{
  alt_u8 ch = '\0';
  while(1)
  {
   int tmp;  //Temporary
   MenuBegin("Single Joint FSMC Position Control Menu:");    
   
   printf("Please choice arm No, Left=0, Right=1:");
   scanf("%d", &tmp);   
   LR_FB_tmp = tmp;
   if (LR_FB_tmp == L) ch = 'L';
   else if (LR_FB_tmp == R) ch = 'R';
   printf("You choice arm No:%d, %c\n", LR_FB_tmp, ch);
    
   printf("Please choice joint No, 1, 2, 3, 4, 5, 6");
   scanf("%d", &tmp);
   No_FB_tmp = tmp;   
   printf("You choice joint No:%d\n", No_FB_tmp);
       
   MenuItem('a', "Single Joint Menu");
   MenuItem('b', "Show All Counters.");
   ch = MenuEnd('a','b');
   switch(ch)
   {
     MenuCase('a', SingleJointMenu);
     MenuCase('b', ShowAllCounters);
   }
   if (ch == 'q') break; 
  }
}

static void SingleJointMenu(void)
{
  alt_u8 ch;
  
  while(1)
  {
    MenuBegin("Joint Setting Menu");
    MenuItem('a', "Initial joint");
    MenuItem('b', "Forward rotation");
    MenuItem('c', "Backward rotation");
    MenuItem('d', "Stop rotation");
    MenuItem('e', "Reset count");
    MenuItem('f', "Step control");
    MenuItem('g', "Stop all rotation");    //因為新買的3.4軸0volt會往下垂所以需要hold住, 但為了避免突發狀況才設此策略可以緊急停止全部馬達
    MenuItem('h', "Set parameter FSMC");
    MenuItem('i', "Set parameter PID");
    MenuItem('j', "Reset Cpu");
    ch=MenuEnd('a','j');
            
    switch(ch)
    {
      MenuCase('a', SingleInitJoint);
      MenuCase('b', SingleForwardJoint);
      MenuCase('c', SingleBackwardJoint);
      MenuCase('d', SingleStopJoint);
      MenuCase('e', SingleResetCounter);
      MenuCase('f', SingleStepControl);
      MenuCase('g', StopAllJoint);
      MenuCase('h', SingleSetParameter_FSMC);    
      MenuCase('i', SingleSetParameter_PID);
      MenuCase('j', HAL_PLATFORM_RESET);        
    }          
      
    if (ch == 'q') break;
              
  }
}

static void ShowAllCounters(void)
{
  int cntL1 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_L_COUNT1_BASE);
  int cntL2 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_L_COUNT2_BASE);
  int cntL3 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_L_COUNT3_BASE);
  int cntL4 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_L_COUNT4_BASE);
  int cntL5 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_L_COUNT5_BASE);
  int cntL6 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_L_COUNT6_BASE);
  int cntR1 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_R_COUNT1_BASE);
  int cntR2 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_R_COUNT2_BASE);
  int cntR3 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_R_COUNT3_BASE);
  int cntR4 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_R_COUNT4_BASE);
  int cntR5 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_R_COUNT5_BASE);
  int cntR6 = IORD_ALTERA_AVALON_PIO_DATA(MOTOR_R_COUNT6_BASE);
  printf("cntL1=%d, cntL2=%d, cntL3=%d, cntL4=%d, cntL5=%d, cntL6=%d\n", cntL1, cntL2, cntL3, cntL4, cntL5, cntL6);
  printf("cntR1=%d, cntR2=%d, cntR3=%d, cntR4=%d, cntR5=%d, cntR6=%d\n", cntR1, cntR2, cntR3, cntR4, cntR5, cntR6);   
}

static void ArmMenu(void)
{
  alt_u8 ch = '\0';
  int tmp = 0;
  while(1)
  {        
    printf("Please choice arm No, Left=0, Right=1:");
    scanf("%d", &tmp);    
    LR_FB_tmp = tmp;
    if (LR_FB_tmp == L) ch = 'L';
    else if (LR_FB_tmp == R) ch = 'R';
    printf("You choice arm No:%d, %c\n", LR_FB_tmp, ch);
    MenuBegin("Arm Control Menu:");                         
    MenuItem('a', "PTP");    
    MenuItem('b', "Trajectory planning in joint space");
    MenuItem('c', "Trajectory planning in cartesian space");
    MenuItem('d', "Stop all rotation");  
    ch = MenuEnd('a','d');
    switch(ch)
    {
        MenuCase('a', ArmPTP_Control);
        MenuCase('b', ArmTrajectoryPlan_J_Control);
        MenuCase('c', ArmTrajectoryPlan_C_Control);
        MenuCase('d', StopAllJoint);
    }
    
    if ( ch=='q' ) break;
  }
}

static void ArmPTP_Control(void)
{  
  int tmp = 0;
  float degree[N + 1] = {[0] = 0.0, 0.0};  //陣列沒有初始化裡面的值都是垃圾, 甚至會有無法預期的現象發生
  int move_mode = -1;
  alt_8 *ch = '\0';  
     
  while( ch == '\0' )
  {
    printf("Please choice move_mode, Normal=0, Tcurve=1, Scurve=2");
    scanf("%d", &tmp);    
    move_mode = tmp;
    if (move_mode == Move_Mode_Normal) ch = "Move_Mode_Normal";
    else if (move_mode == Move_Mode_Tcurve) ch = "Move_Mode_Tcurve";
    else if (move_mode == Move_Mode_Scurve) ch = "Move_Mode_Scurve";  
    else  printf("ERROR!! invalid move_mode \n");              
  }
  printf("You choice move mode:%s\n", ch); 
  
  /* 之後也可以在這裡增加各軸control mode的設定
   * 目前是透過SingleStepControl設定完,   
   * Arm_ISR, Arm_Tcurve_ISR, Arm_Scurve_ISR
   * 直接使用, 所以可以發現
   * SingleJoint_PID_ISR, SingleJoint_Tcurve_PID_ISR, SingleJoint_Scurve_PID_ISR相對於
   * SingleJoint_FSMC_ISR.....是多餘的ㄏㄏ  之後精簡程式碼再做修改
   * */
    
  int joint_No = 1;
  for (joint_No = 1; joint_No < N + 1 ; joint_No++)
  {
    PtrMotor pmotor = motor_list[LR_FB_tmp][joint_No];
                 
    printf("Please input desired angle to rotate joint%d( %.1f ~ %.1f degree ):",joint_No, pmotor->limitLower, pmotor->limitUpper);        
    scanf("%d", &tmp);
    if( tmp > pmotor->limitUpper || tmp < pmotor->limitLower )
    {
      printf("ERROR!! ");
      printf("%d ", tmp);
      printf("Input angle out off range!\n");
      joint_No--;
    }
    else
    {
      degree[joint_No] = tmp;   //未來也許有人推出運動學公式就加在此處, 但程式架構應該會需要微調(關節座標、卡氏座標)      
    }         
  }
  LR_tmp = LR_FB_tmp;  
  ArmMove(degree, move_mode);
}


static void ArmMove(float *_degree, int _move_mode)
{
  time = 0.0;
  cnt_isr_Xms = 1;
  flag_print = 0;
 
  printf("default degree:1 for exit limit switch\n");
  int joint_No = 1;
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][joint_No];
    Init_Controller_Variable(pmotor);    
    pmotor->refVal = _degree[joint_No] * pmotor->deg2count;   
    printf("ArmMove: Moving joint%d degree:%f, count:%d\n", joint_No, _degree[joint_No], pmotor->refVal);     
  }
  printf("\n");    
  ArmInit((void*)TIMER_20MS_BASE, TIMER_20MS_IRQ, TIMER_20MS_FREQ, _move_mode);  
  
//  while(cnt_isr_Xms <= 1000)  //1000*0.02 = 20 秒  
  while( cnt_isr_Xms <= time_print )
  {     
    if( flag_print== 1 )    
    {
      printf("%.2f ", time);           
      for (joint_No = 1; joint_No < N + 1; joint_No++)
      {
        PtrMotor pmotor = motor_list[LR_tmp][joint_No];                                  
        printf("%d %d %d ", pmotor->count, pmotor->error, pmotor->controlVal);                                                                                                                
      }     
      printf("\n");
      flag_print = 0;         
    }
  }
  printf("flag_print %d\n", flag_print);
  // disable timer interrupt
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  usleep(2000000);
  
  time = 0.0;
  cnt_isr_Xms = 1;
  flag_print = 0;
  
  float degree[N + 1] = {[0] = 0.0, 0.0, 0.0, 0.0, 90.0, 0.0};  //陣列沒有初始化裡面的值都是垃圾, 甚至會有無法預期的現象發生
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][joint_No];  
    Init_Controller_Variable(pmotor);      
    pmotor->refVal = degree[joint_No] * pmotor->deg2count;   
    printf("ArmMove: Moving joint%d degree:%f, count:%d\n", joint_No, degree[joint_No], pmotor->refVal);     
  }
  printf("\n");    
  ArmInit((void*)TIMER_20MS_BASE, TIMER_20MS_IRQ, TIMER_20MS_FREQ, _move_mode);  
  while( cnt_isr_Xms <= time_print )
  {     
    if( flag_print== 1 )    
      flag_print = 0;         
  }
  printf("flag_print %d\n", flag_print);  
  // disable timer interrupt
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  //stop joint, 不呼叫Init_pwm()原因為預留後續可能左手已做完任務而右手還沒(若不包在同一個timer isr的話), 且有可能兩隻手臂需要不同timer才可同時運作(若放同Timer計算時間會超過5ms的話)
//  for (joint_No = 1; joint_No < N + 1; joint_No++)  
//  {
//    PtrMotor pmotor = motor_list[LR_tmp][joint_No];
//    IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, 0); 
//  }
}

static void ArmInit(void* base, alt_u32 irq, alt_u32 freq, int _move_mode)
{
  /* set the system clock frequency */
  alt_sysclk_init (freq);

  /* set to free running mode */
  IOWR_ALTERA_AVALON_TIMER_CONTROL (base,
            ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  |
            ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
            ALTERA_AVALON_TIMER_CONTROL_START_MSK);

  /* register the interrupt handler, and enable the interrupt */
  if (_move_mode == Move_Mode_Normal)
  {    
    time_print = 300;  //0.02 * 300 = 6s
    flag_print = 0;
    alt_irq_register (irq, base, Arm_ISR);
  }
  else if (_move_mode == Move_Mode_Tcurve)
  {
    ArmInit_Tcurve();
    time_print = round(TIME+1) * 50;  //1cnt / 0.005ms => 200cnt / 1s
    flag_print = 0; 
//    usleep(2000000);
    alt_irq_register (irq, base, Arm_Tcurve_ISR);
  }
  else if (_move_mode == Move_Mode_Scurve)
  {
    ArmInit_Scurve();
    time_print = round(TIME+1) * 50;  //1cnt / 0.005ms => 200cnt / 1s,  0.02ms => 50, 0.01ms => 100
    flag_print = 0;    
//    usleep(2000000); 
    alt_irq_register (irq, base, Arm_Scurve_ISR);
  }
}

static void ArmInit_Tcurve(void)
{  
  //陣列沒有初始化裡面的值都是垃圾
  float xd[N + 1] = {[0] = 0.0, 0.0} ;//目標和當前位置的位移量, unit:degree, 因每軸必須在同一個scale下進行比較
  int  flag[N + 1] = {[0] = 0, 0}; 
  float Ta[N + 1] = {[0] = 0.0, 0.0};
  float v[N + 1] = {[0] = 0.0, 0.0};
  float a[N + 1] = {[0] = 0.0, 0.0};     
  float Time[N + 1] = {[0] = 0.0, 0.0};  
  
  int joint_No = 1;
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][joint_No];
    pmotor->Tcnt = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);  //速度規劃前的當前位置
    
    float ref = pmotor->refVal;  //不會在初始化計算時被動產生的而是使用者給定的
    signed int  cnt = pmotor->Tcnt;
    float deg2count = pmotor->deg2count;    
    float Vmax = pmotor->Vmax;
    float Amax = pmotor->Amax;       
    
    if (ref >= cnt)
    {
      xd[joint_No] = (ref - cnt) / deg2count;
      flag[joint_No] = 1;
    }
    else
    {
      xd[joint_No] = (cnt - ref) / deg2count;
      flag[joint_No] = 0;
    }
    pmotor->Tflag = flag[joint_No];    
         
    if (xd[joint_No] >= powf(Vmax,2) / Amax)
    {
      Ta[joint_No] = Vmax / Amax;
      Time[joint_No] = xd[joint_No] / Vmax + Ta[joint_No];
      v[joint_No] = Vmax;
    }   
    else
    {
      Ta[joint_No] = sqrt(xd[joint_No] / Amax);
      Time[joint_No] = 2 * Ta[joint_No];
      v[joint_No] = sqrt(xd[joint_No] * Amax);
    }
    a[joint_No] = Amax;
  }
  
  alt_u8 ch = '\0';
  if (LR_tmp == L) ch = 'L';
  else if (LR_tmp == R) ch = 'R';
  printf("arm %c\n", ch);
  printf("各軸各自規劃所花的時間\n");
  printf("位移, 總時間, 加速度段開始時間, 最大速度, 最大加速度\n");  
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {    
    printf("joint%d\n", joint_No);
    printf("xd = %.3f, Time = %.3f, Ta = %.3f, v = %.3f, a = %.3f\n", xd[joint_No], Time[joint_No], Ta[joint_No], v[joint_No], a[joint_No]);    
  }  
  
  int Tbig_No = compare5(Time[1], Time[2], Time[3], Time[4], Time[5]);
  printf("Cost biggest Time joint%d\n", Tbig_No);
  
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][joint_No];
    if (joint_No == Tbig_No) ;
    else
    {
      Ta[joint_No] = Ta[Tbig_No];
      Time[joint_No] = Time[Tbig_No];
      a[joint_No] = xd[joint_No] / (Time[joint_No]*Ta[joint_No] - powf(Ta[joint_No],2));
      v[joint_No] = a[joint_No] * Ta[joint_No];
    }    
    pmotor->Ta = Ta[joint_No];
    pmotor->v = v[joint_No];
    pmotor->a = a[joint_No];
    pmotor->Time = Time[joint_No];
    TIME = Time[joint_No];
  }    
  printf("比較完各軸時間後所規劃的v、a\n");
  printf("位移, 總時間, 加速度段開始時間, 最大速度, 最大加速度\n");  
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][joint_No];
    printf("joint%d\n", joint_No);
    printf("xd = %.3f, Time = %.3f, Ta = %.3f, v = %.3f, a = %.3f\n", xd[joint_No], Time[joint_No], Ta[joint_No], v[joint_No], a[joint_No]);            
    printf("xd = %.3f, Time = %.3f, Ta = %.3f, v = %.3f, a = %.3f\n", xd[joint_No], pmotor->Time, pmotor->Ta, pmotor->v, pmotor->a);
  }              
}

static void ArmInit_Scurve(void)
{
  //陣列沒有初始化裡面的值都是垃圾
  float xd[N + 1] = {[0] = 0.0, 0.0} ;//目標和當前位置的位移量, unit:degree, 因每軸必須在同一個scale下進行比較
  int  flag[N + 1] = {[0] = 0, 0}; 
  float Ta[N + 1] = {[0] = 0.0, 0.0};
  float Ts[N + 1] = {[0] = 0.0, 0.0};
  float v[N + 1] = {[0] = 0.0, 0.0};
  float a[N + 1] = {[0] = 0.0, 0.0};
  float j[N + 1] = {[0] = 0.0, 0.0};      
  float Time[N + 1] = {[0] = 0.0, 0.0};  
  float Aave[N + 1] = {[0] = 0.0, 0.0};  
  
  int j_No = 1;  // joint_No
  for (j_No = 1; j_No < N + 1; j_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    pmotor->Tcnt = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);  //速度規劃前的當前位置
    
    float ref = pmotor->refVal;
    signed int  cnt = pmotor->Tcnt;
    float deg2count = pmotor->deg2count;
    float s_factor = pmotor->s_factor;    
    float Vmax = pmotor->Vmax;
    float Amax = pmotor->Amax;    
    Aave[j_No] = (1.0 - 0.5*s_factor) * Amax;
    
    if (ref >= cnt)
    {
      xd[j_No] = (ref - cnt) / deg2count;
      flag[j_No] = 1;
    }
    else
    {
      xd[j_No] = (cnt - ref) / deg2count;
      flag[j_No] = 0;
    }
    pmotor->Tflag = flag[j_No];    
    pmotor->Aave =  Aave[j_No];
         
    if (xd[j_No] >= powf(Vmax,2) / Aave[j_No])
    {
      Ta[j_No] = Vmax / Aave[j_No];
      Time[j_No] = xd[j_No] / Vmax + Ta[j_No];
      Ts[j_No] = (0.5 * s_factor) * Ta[j_No];
    }   
    else
    {
      Ta[j_No] = sqrt(xd[j_No] / Aave[j_No]);
      Time[j_No] = 2 * Ta[j_No];
      Ts[j_No] = (0.5 * s_factor) * Ta[j_No];
    }
    v[j_No] = xd[j_No] / (Time[j_No] - Ta[j_No]);
    a[j_No] = v[j_No] / (Ta[j_No] - Ts[j_No]);
    
    if (Ts[j_No] == 0)  j[j_No] = 0; // 避免計算j時分母為零
    else  j[j_No] = Amax / Ts[j_No]; // 急衝度, 此處不是 Aave
  }
  
  alt_u8 ch = '\0';
  if (LR_tmp == L) ch = 'L';
  else if (LR_tmp == R) ch = 'R';
  printf("arm %c\n", ch);
  printf("各軸各自規劃所花的時間\n");
  printf("位移, 總時間, 等加速度段開始時間, 等速度段開始時間\n");
  for (j_No = 1; j_No < N + 1; j_No++)
  {    
    printf("joint%d\n", j_No);
    printf("xd = %.3f, Time = %.3f, Ts = %.3f, Ta = %.3f\n", xd[j_No], Time[j_No], Ts[j_No], Ta[j_No]);                
  }    
  printf("\n");
  printf("最大速度, 最大加速度, 急衝度\n");
  for (j_No = 1; j_No < N + 1; j_No++)
  {    
    printf("joint%d\n", j_No);
    printf("v = %.3f, a = %.3f, j = %.3f\n", v[j_No], a[j_No], j[j_No]);                
  }
  printf("\n");  
  
  int Tbig_No = compare5(Time[1], Time[2], Time[3], Time[4], Time[5]);
  printf("Cost biggest Time joint%d\n\n", Tbig_No);    
 
  for (j_No = 1; j_No < N + 1; j_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    float s_factor = pmotor->s_factor;    
    if (j_No == Tbig_No) ;
    else
    {
      Ta[j_No] = Ta[Tbig_No];
      Ts[j_No] = (0.5 * s_factor) * Ta[j_No];
      Time[j_No] = Time[Tbig_No];
      v[j_No] = xd[j_No] / (Time[j_No] - Ta[j_No]);
      a[j_No] = v[j_No] / (Ta[j_No] - Ts[j_No]);      
    }
    if (Ts[j_No] == 0)  j[j_No] = 0; // 避免計算j時分母為零
    else  j[j_No] = a[j_No] / Ts[j_No]; // 急衝度, 此處不是 Aave 
    pmotor->Ta = Ta[j_No];
    pmotor->Ts = Ts[j_No];
    pmotor->v = v[j_No];
    pmotor->a = a[j_No];
    pmotor->j = j[j_No];
    pmotor->Time = Time[j_No];
    TIME = Time[j_No];
  }    
  printf("比較完各軸時間後所規劃的v、a\n");
  printf("位移, 總時間, 等加速度段開始時間, 等速度段開始時間\n");
  for (j_No = 1; j_No < N + 1; j_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    printf("joint%d\n", j_No);
    printf("xd = %.3f, Time = %.3f, Ts = %.3f, Ta = %.3f\n", xd[j_No], Time[j_No], Ts[j_No], Ta[j_No]);            
    printf("xd = %.3f, Time = %.3f, Ts = %.3f, Ta = %.3f\n", xd[j_No], pmotor->Time, pmotor->Ts, pmotor->Ta);
  }    
  printf("\n");
  printf("最大速度, 最大加速度, 急衝度\n");
  for (j_No = 1; j_No < N + 1; j_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    printf("joint%d\n", j_No);
    printf("v = %.3f, a = %.3f, j = %.3f\n", v[j_No], a[j_No], j[j_No]);            
    printf("v = %.3f, a = %.3f, j = %.3f\n", pmotor->v, pmotor->a, pmotor->j);
  }
  printf("\n");
    
  float t0[N + 1][8];
  float a0[N + 1][8];
  float v0[N + 1][8];
  float p0[N + 1][8];
  memset(t0, 0.0, sizeof(t0[0][0]) * 6 * 8);  
  memset(a0, 0.0, sizeof(a0[0][0]) * 6 * 8);
  memset(v0, 0.0, sizeof(v0[0][0]) * 6 * 8);
  memset(p0, 0.0, sizeof(p0[0][0]) * 6 * 8);
    
  for (j_No = 1; j_No < N + 1; j_No++)
  {    
    t0[j_No][0] = 0.0;
    t0[j_No][1] = Ts[j_No];
    t0[j_No][2] = Ta[j_No] - Ts[j_No];
    t0[j_No][3] = Ta[j_No];
    t0[j_No][4] = Time[j_No] - Ta[j_No];
    t0[j_No][5] = Time[j_No] - Ta[j_No] + Ts[j_No];
    t0[j_No][6] = Time[j_No] - Ts[j_No];
    t0[j_No][7] = Time[j_No];    
    
    a0[j_No][0] = 0.0;
    a0[j_No][1] = a[j_No];
    a0[j_No][2] = a[j_No];
    a0[j_No][3] = 0.0;
    a0[j_No][4] = 0.0;
    a0[j_No][5] = -a[j_No];
    a0[j_No][6] = -a[j_No];
    a0[j_No][7] = 0.0;
    
    v0[j_No][0] = 0.0;
    v0[j_No][1] = 0.5*j[j_No]*powf(t0[j_No][1],2);
    v0[j_No][2] = a0[j_No][1]*(t0[j_No][2]-t0[j_No][1])+v0[j_No][1];
    v0[j_No][3] = v[j_No];
    v0[j_No][4] = v[j_No];
    v0[j_No][5] = -0.5*j[j_No]*powf((t0[j_No][5]-t0[j_No][4]),2)+v0[j_No][4];
    v0[j_No][6] = a0[j_No][5]*(t0[j_No][6]-t0[j_No][5])+v0[j_No][5];
    v0[j_No][7] = 0.5*j[j_No]*powf((t0[j_No][7]-t0[j_No][6]),2)+a0[j_No][6]*(t0[j_No][7]-t0[j_No][6])+v0[j_No][6];
    
    p0[j_No][0] = 0.0;
    p0[j_No][1] = (1.0/6)*j[j_No]*powf(t0[j_No][1],3);
    p0[j_No][2] = 0.5*a0[j_No][1]*powf((t0[j_No][2]-t0[j_No][1]),2)+v0[j_No][1]*(t0[j_No][2]-t0[j_No][1])+p0[j_No][1];
    p0[j_No][3] = -(1.0/6)*j[j_No]*powf((t0[j_No][3]-t0[j_No][2]),3)+0.5*a0[j_No][2]*powf((t0[j_No][3]-t0[j_No][2]),2)+v0[j_No][2]*(t0[j_No][3]-t0[j_No][2])+p0[j_No][2];
    p0[j_No][4] = v0[j_No][3]*(t0[j_No][4]-t0[j_No][3])+p0[j_No][3];
    p0[j_No][5] = -(1.0/6)*j[j_No]*powf((t0[j_No][5]-t0[j_No][4]),3)+v0[j_No][4]*(t0[j_No][5]-t0[j_No][4])+p0[j_No][4];      
    p0[j_No][6] = 0.5*a0[j_No][5]*powf((t0[j_No][6]-t0[j_No][5]),2)+v0[j_No][5]*(t0[j_No][6]-t0[j_No][5])+p0[j_No][5];
    p0[j_No][7] = (1.0/6)*j[j_No]*powf((t0[j_No][7]-t0[j_No][6]),3)+0.5*a0[j_No][6]*powf((t0[j_No][7]-t0[j_No][6]),2)+v0[j_No][6]*(t0[j_No][7]-t0[j_No][6])+p0[j_No][6];          
  }
  
  int i = 0;  
  for (j_No = 1; j_No < N + 1; j_No++)  //拆4次for loop是為了方便觀比較各軸
  {
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    printf("joint%d 所規劃的時間點:\n", j_No);
    printf("t0, t1, t2, t3, t4, t5, t6, t7\n"); //8個時間點7個時段    
    for (i = 0; i < 8; i++)  
    {
      printf("%.3f, ", t0[j_No][i]);
      pmotor->t0[i] = t0[j_No][i];     
    }
    printf("\n");
  }
  printf("\n");
  
  for (j_No = 1; j_No < N + 1; j_No++)
  {        
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    printf("joint%d 各時間點的加速度:\n", j_No);    
    printf("a0, a1, a2, a3, a4, a5, a6, a7\n");     
    for (i = 0; i < 8; i++)  
    {
      printf("%.3f, ", a0[j_No][i]);
      pmotor->a0[i] = a0[j_No][i];     
    }
    printf("\n");
  }
  printf("\n");
  
  for (j_No = 1; j_No < N + 1; j_No++)
  {  
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    printf("joint%d 各時間點的速度:\n", j_No);          
    printf("v0, v1, v2, v3, v4, v5, v6, v7\n");    
    for (i = 0; i < 8; i++)  
    {
      printf("%.3f, ", v0[j_No][i]);
      pmotor->v0[i] = v0[j_No][i];     
    }
    printf("\n");
  }
  printf("\n");
  
  for (j_No = 1; j_No < N + 1; j_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][j_No];
    printf("joint%d 各時間點的位置:\n", j_No);          
    printf("p0, p1, p2, p3, p4, p5, p6, p7\n");    
    for (i = 0; i < 8; i++) 
    {
    printf("%.3f, ", p0[j_No][i]);
    pmotor->p0[i] = p0[j_No][i];     
    }
    printf("\n");
  }
  printf("\n");        
}

static void Arm_ISR(void* base, alt_u32 id)
{
  time = (double)(sample_time * cnt_isr_Xms); //time  
  int j_No = 1;
  for (j_No = 1; j_No < N + 1; j_No++)
  { 
    PtrMotor pmotor = motor_list[LR_tmp][j_No];        
    // get encoder signal    
    pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);
    pmotor->diff = pmotor->count - pmotor->count_old;
    pmotor->diff_d = pmotor->diff - pmotor->diff_old;      
    pmotor->displacement += pmotor->diff;
    pmotor->error = pmotor->refVal - pmotor->displacement;        
    pmotor->error_diff = pmotor->error - pmotor->error_old;
    pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;           
    if (pmotor->Ctrl_type == Control_Mode_PID)
    {
      if (pmotor->PID_type == PID_Mode_PID)          
        PID(pmotor);      
      else if (pmotor->PID_type == PID_Mode_PD)      
        PD(pmotor);      
      else if (pmotor->PID_type == PID_Mode_PI)     
        PI(pmotor);                      
    }
    else  //(pmotor->Ctrl_type == Control_Mode_FSMC)
    {
      if (pmotor->FSMC_type == FSMC_Mode_Rel)    
        RelFSMC(pmotor);
      else  //FSMC_Mode_Abs     
        AbsFSMC(pmotor);          
    }
    pmotor->count_old = pmotor->count;
    pmotor->diff_old = pmotor->diff; 
    pmotor->error_old = pmotor->error;
    pmotor->error_diff_old = pmotor->error_diff;     
  }    
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();
}

static void Arm_Tcurve_ISR(void* base, alt_u32 id)
{
  time = (double)(sample_time * cnt_isr_Xms); //time
    
  //陣列沒有初始化裡面的值都是垃圾
  int xd[N + 1] = {[0] = 0, 0} ;  //Tcurve所規劃的點(較遠的軸以最大速度規劃), 每個時間點各軸所要各別追的點, 最終會在同時間點一起到達各自所要到的點  
    
  int joint_No = 1;
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {            
    PtrMotor pmotor = motor_list[LR_tmp][joint_No];
    float deg2count = pmotor->deg2count;
    float Time = pmotor->Time;
    float Ta = pmotor->Ta;
    float v = pmotor->v;
    float a = pmotor->a;
    int   cnt = pmotor->Tcnt;
    
    if (time < Ta)
    {
      if (pmotor->Tflag ==1 )  xd[joint_No] = cnt + Tpart1(a, time)*deg2count;  //當時規劃的scale是degree, 要再轉回count
      else  xd[joint_No] = cnt - Tpart1(a, time)*deg2count;
    }
    else if ((time >= Ta) && (time <= (Time - Ta)))
    {
      if (pmotor->Tflag == 1)  xd[joint_No] = cnt + Tpart2(a, v, Ta, time)*deg2count;
      else  xd[joint_No] = cnt - Tpart2(a, v, Ta, time)*deg2count;
    }
    else if ((time > (Time - Ta)) && (time < Time))
    {
      if (pmotor->Tflag == 1)  xd[joint_No] = cnt + Tpart3(a, v, Ta, time)*deg2count;
      else  xd[joint_No] = cnt - Tpart3(a, v, Ta, time)*deg2count;
    }
    else if (time >= Time)
    {
      xd[joint_No] = pmotor->refVal;
    }
    pmotor->Tref = xd[joint_No];  //為了check 速度規劃曲線所追的位置軌跡
    
    // get encoder signal          
    pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);
    pmotor->diff = pmotor->count - pmotor->count_old;
    pmotor->diff_d = pmotor->diff - pmotor->diff_old;       
    pmotor->displacement += pmotor->diff;
    pmotor->error = xd[joint_No] - pmotor->displacement;        
    pmotor->error_diff = pmotor->error - pmotor->error_old;
    pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;
    if (pmotor->Ctrl_type == Control_Mode_PID)
    {
      if (pmotor->PID_type == PID_Mode_PID)          
        PID(pmotor);      
      else if (pmotor->PID_type == PID_Mode_PD)      
        PD(pmotor);      
      else if (pmotor->PID_type == PID_Mode_PI)     
        PI(pmotor);                      
    }
    else  //(pmotor->Ctrl_type == Control_Mode_FSMC)
    {
      if (pmotor->FSMC_type == FSMC_Mode_Rel)    
        RelFSMC(pmotor);
      else  //FSMC_Mode_Abs     
        AbsFSMC(pmotor);          
    }   
    pmotor->count_old = pmotor->count;
    pmotor->diff_old = pmotor->diff;   
    pmotor->error_old = pmotor->error;
    pmotor->error_diff_old = pmotor->error_diff;            
  }  
  
  /*分別計算完各軸現在時間所要追的位置後再分別計算控制量控
  計算控制量控第1軸  計算控制量控第2軸 計算控制量控第3軸 計算控制量控第4軸 計算控制量控第5軸 和
  計算第1軸要追哪 計算控制量控第1軸 計算第1軸要追哪 計算控制量控第1軸 計算第1軸要追哪 計算控制量控第1軸 ...
  有一點不太一樣
  */
  
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();      
}

static void Arm_Scurve_ISR(void* base, alt_u32 id)
{  
  time = (double)(sample_time * cnt_isr_Xms); //time
  
  //陣列沒有初始化裡面的值都是垃圾
  int xd[N + 1] = {[0] = 0, 0} ;  //Scurve所規劃的點(較遠的軸以最大速度規劃), unit:count, 每個時間點各軸所要各別追的點, 最終會在同時間點一起到達各自所要到的點  
  
  int joint_No = 1;
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  {
    PtrMotor pmotor = motor_list[LR_tmp][No_tmp];
    float deg2count = pmotor->deg2count;
    float j = pmotor->j;  
    float at, vt, pt;  //acc_tmp, vel_tmp, pos_tmp;
    float *t0 = pmotor->t0; //same with, float *t0 = &pmotor->t0[0];
    float *a0 = pmotor->a0;
    float *v0 = pmotor->v0; 
    float *p0 = pmotor->p0; 
    signed int cnt = pmotor->Tcnt;  //check current position
    
    if (time <= t0[1])
    {
      at = j*time;
      vt = 0.5*j*powf(time,2);
      pt = (1.0/6)*j*powf(time,3);
      if (pmotor->Tflag ==1 );  //當時規劃的scale是degree, 要再轉回count
      else 
      {
        at = at*-1.0;
        vt = vt*-1.0;
        pt = pt*-1.0;       
      }
      xd[joint_No] = cnt + pt*deg2count;
    }
    else if ((t0[1] < time) && (time < t0[2]))
    {
      at = a0[1]; 
      vt = a0[1]*(time-t0[1])+v0[1];
      pt = 0.5*a0[1]*powf((time-t0[1]),2)+v0[1]*(time-t0[1])+p0[1];
      if (pmotor->Tflag ==1 ); 
      else 
      {
        at = at*-1.0;
        vt = vt*-1.0;
        pt = pt*-1.0;       
      }
      xd[joint_No] = cnt + pt*deg2count;
    }
    else if ((t0[2] <= time) && (time <= t0[3]))
    {
      at = -j*(time-t0[2])+a0[2]; 
      vt = -0.5*j*powf((time-t0[2]),2)+a0[2]*(time-t0[2])+v0[2]; 
      pt = -(1.0/6)*j*powf((time-t0[2]),3)+0.5*a0[2]*powf((time-t0[2]),2)+v0[2]*(time-t0[2])+p0[2];
      if (pmotor->Tflag ==1 ); 
      else 
      {
        at = at*-1.0;
        vt = vt*-1.0;
       pt = pt*-1.0;       
      }
      xd[joint_No] = cnt + pt*deg2count;
    }
    else if ((t0[3] < time) && (time < t0[4]))
    {
      at = a0[3]; 
      vt = v0[3];
      pt = v0[3]*(time-t0[3])+p0[3];
      if (pmotor->Tflag ==1 ); 
      else 
      {
        at = at*-1.0;
        vt = vt*-1.0;
        pt = pt*-1.0;       
      }
      xd[joint_No] = cnt + pt*deg2count;
    }
    else if ((t0[4] <= time) && (time <= t0[5]))
    {
      at = -j*(time-t0[4])+a0[4];
      vt = -0.5*j*powf((time-t0[4]),2)+v0[4]; 
      pt = -(1.0/6)*j*powf((time-t0[4]),3)+v0[4]*(time-t0[4])+p0[4];
      if (pmotor->Tflag ==1 ); 
      else 
      {
        at = at*-1.0;
        vt = vt*-1.0;
        pt = pt*-1.0;       
      }
      xd[joint_No] = cnt + pt*deg2count;
    }
    else if ((t0[5] < time) && (time < t0[6]))
    {
      at = a0[5]; 
      vt = a0[5]*(time-t0[5])+v0[5];
      pt = 0.5*a0[5]*powf((time-t0[5]),2)+v0[5]*(time-t0[5])+p0[5];
      if (pmotor->Tflag ==1 ); 
      else 
      {
        at = at*-1.0;
        vt = vt*-1.0;
        pt = pt*-1.0;       
      }
      xd[joint_No] = cnt + pt*deg2count;
    }
    else if ((t0[6] <= time) && (time <= t0[7]))
    {
      at = j*(time-t0[6])+a0[6];
      vt = 0.5*j*powf((time-t0[6]),2)+a0[6]*(time-t0[6])+v0[6];
      pt = (1.0/6)*j*powf((time-t0[6]),3)+0.5*a0[6]*powf((time-t0[6]),2)+v0[6]*(time-t0[6])+p0[6];
      if (pmotor->Tflag ==1 ); 
      else 
      {
        at = at*-1.0;
        vt = vt*-1.0;
        pt = pt*-1.0;       
      }
      xd[joint_No] = cnt + pt*deg2count;
    }
    else if (t0[7] < time)
    {
      xd[joint_No] = pmotor->refVal;
    }
    pmotor->Tref = xd[joint_No];  //為了check 速度規劃曲線所追的位置軌跡           
  }
      
  /*分別計算完各軸現在時間所要追的位置後再分別計算控制量控
   計算控制量控第1軸  計算控制量控第2軸 計算控制量控第3軸 計算控制量控第4軸 計算控制量控第5軸 和
   計算第1軸要追哪 計算控制量控第1軸 計算第1軸要追哪 計算控制量控第1軸 計算第1軸要追哪 計算控制量控第1軸 ...
   有一點不太一樣
  */
  joint_No = 1;
  for (joint_No = 1; joint_No < N + 1; joint_No++)
  { 
    PtrMotor pmotor = motor_list[LR_tmp][joint_No];
    // get encoder signal    
    pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);             
    pmotor->diff = pmotor->count - pmotor->count_old;
    pmotor->diff_d = pmotor->diff - pmotor->diff_old;      
    pmotor->displacement += pmotor->diff;
    pmotor->error = xd[joint_No] - pmotor->displacement;        
    pmotor->error_diff = pmotor->error - pmotor->error_old;
    pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;        
    if (pmotor->Ctrl_type == Control_Mode_PID)
    {
      if (pmotor->PID_type == PID_Mode_PID)          
        PID(pmotor);      
      else if (pmotor->PID_type == PID_Mode_PD)      
        PD(pmotor);      
      else if (pmotor->PID_type == PID_Mode_PI)     
        PI(pmotor);                      
    }
    else  //(pmotor->Ctrl_type == Control_Mode_FSMC)
    {
      if (pmotor->FSMC_type == FSMC_Mode_Rel)    
        RelFSMC(pmotor);
      else  //FSMC_Mode_Abs     
        AbsFSMC(pmotor);          
    }
    pmotor->count_old = pmotor->count;
    pmotor->diff_old = pmotor->diff;
    pmotor->error_old = pmotor->error;
    pmotor->error_diff_old = pmotor->error_diff;    
  }    
  flag_print += 1;   
  cnt_isr_Xms += 1; 
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();   
}

static void ArmTrajectoryPlan_J_Control(void)
{
}

static void ArmTrajectoryPlan_C_Control(void)
{
}

static void DualArmInit_FSMC(void* base, alt_u32 irq, alt_u32 freq, int _move_mode)
{
  /* set the system clock frequency */
  alt_sysclk_init (freq);

  /* set to free running mode */
  IOWR_ALTERA_AVALON_TIMER_CONTROL (base,
            ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  |
            ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
            ALTERA_AVALON_TIMER_CONTROL_START_MSK);

  /* register the interrupt handler, and enable the interrupt */
  if (_move_mode == Move_Mode_Normal)
  {    
    time_print = 300;  //0.02 * 300 = 6s
    flag_print = 0;
    alt_irq_register (irq, base, DualArm_FSMC_ISR);
  }
}

static void DualArm_FSMC_ISR(void* base, alt_u32 id)
{   
////////////記憶體存放註解區-配置一////////////
/*
  time_arr[num] = (float)(sample_time * cnt_isr_Xms); //time 
         
  int arm_No = 0;
  int j_No = 1;
//  float xd = 0.0;  //不行
    
  for (arm_No = 0; arm_No < 2; arm_No++)
  {
    for (j_No = 1; j_No < N; j_No++)
    { 
      PtrMotor pmotor = motor_list[arm_No][j_No];
//      xd = refVal_list[arm_No][j_No];
      Tref_arr[arm_No][j_No-1][num] = refVal_list[arm_No][j_No];
      pmotor->refVal = refVal_list[arm_No][j_No] * pmotor->deg2count;
//      pmotor->refVal = Tref_arr[arm_No][j_No-1][num] * pmotor->deg2count;
      // get encoder signal          
      pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);      
      pmotor->diff = pmotor->count - pmotor->count_old;
      pmotor->diff_d = pmotor->diff - pmotor->diff_old;               
      pmotor->displacement += pmotor->diff;
//      pmotor->error = (xd * pmotor->deg2count) - pmotor->displacement;
      pmotor->error = pmotor->refVal - pmotor->displacement;
      pmotor->error_diff = pmotor->error - pmotor->error_old;
      pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;      
      if (pmotor->FSMC_type == FSMC_Mode_Rel)
      { 
        RelFSMC(pmotor);
      }
      else  //FSMC_Mode_Abs 
      {       
        AbsFSMC(pmotor);        
      }      
      pmotor->count_old = pmotor->count;
      pmotor->diff_old = pmotor->diff;
      pmotor->error_old = pmotor->error;     
      pmotor->error_diff_old = pmotor->error_diff;
      
      count_arr[arm_No][j_No-1][num] = pmotor->count;
      error_arr[arm_No][j_No-1][num] = pmotor->error;
      controlVal_arr[arm_No][j_No-1][num] = pmotor->controlVal;
//      Tref_arr[arm_No][j_No-1][num] = xd;            
    }         
  }   
  num += 1;  
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();    
*/
////////////記憶體存放註解區-配置一////////////   
}
        
// calibration 
static void SingleInitJoint(void)  //單軸賦歸
{      
  PtrMotor pmotor = motor_list[LR_FB_tmp][No_FB_tmp];
  int rst_count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->rst_count_base);
  printf("IniJoint %c No:%d, counter:%d\n", pmotor->LR, pmotor->No, rst_count);
  
  Init_LS();      
  edge_capture = 0;
  while( edge_capture!=pmotor->limit_switch_reg ) { } //LS_XX
  printf("IniJoint: %c No:%d, edge_capture = %d\n", pmotor->LR, pmotor->No, edge_capture);
      
    if( rst_count!=0 )
    {
      printf("IniJoint: %c No:%d, init_LS()\n", pmotor->LR, pmotor->No);
      Init_LS();      
      edge_capture = 0;
      IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir_backward);
      IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, (int)pmotor->fullwidth*0.5);
      printf("IniJoint: %c No:%d, rst_count!=0\n", pmotor->LR, pmotor->No);
       
      while( edge_capture!=pmotor->limit_switch_reg ) { } //LS_XX
      printf("IniJoint: %c No:%d, edge_capture = %d\n", pmotor->LR, pmotor->No, edge_capture);

      IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, 0); //stop PWM
      IOWR_ALTERA_AVALON_PIO_IRQ_MASK(ROBOT_LS_BASE, 0x0); //disable LS interrupt
    }
   
  usleep(1000000); //sleep 1 sec
  SingleResetCounter();
  
  SingleMoveJoint(50, pmotor, -1, -1);  //離開極限開關感應
  usleep(1000000);
  SingleResetCounter();
  printf("87\n");
}

static void SingleForwardJoint(void)
{
  PtrMotor pmotor = motor_list[LR_FB_tmp][No_FB_tmp];  
  int tmp;
  int u;
  printf("Forward !\n");
  printf("Please input pwm duty cycle 0~100 percent:");
  scanf("%d", &tmp);  
  u = (tmp/100.0) * pmotor->fullwidth;
  printf("You pwm duty cycle:%d, %d bits\n", tmp, u);
  
  usleep(1000000); //sleep 1 sec
    
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir_forward);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, u);  
}

static void SingleBackwardJoint(void)
{
  PtrMotor pmotor = motor_list[LR_FB_tmp][No_FB_tmp];  
  int tmp;
  int u;
  printf("Backward !\n");
  printf("Please input pwm duty cycle 0~100 percent:");
  scanf("%d", &tmp);
  u = (tmp/100.0) * pmotor->fullwidth;
  printf("You pwm duty cycle:%d, %d bits\n", tmp, u);
  
  usleep(1000000); //sleep 1 sec
    
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir_backward);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, u);
}

static void SingleStopJoint(void)
{
  PtrMotor pmotor = motor_list[LR_FB_tmp][No_FB_tmp];
  // disable timer interrupt 
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, 0);
  int count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);
  printf("Stop!!!!!!!!!!!!\n");
  printf("Joint%c No%d counter = %d\n", pmotor->LR, pmotor->No, count);
}

static void SingleResetCounter(void)  //編碼器單軸歸零
{
  PtrMotor pmotor = motor_list[LR_FB_tmp][No_FB_tmp];
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->rst_count_base, 1); // data = 1 to reset,
  Init_Controller_Variable(pmotor);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->rst_count_base, 0); //1經過not變0, 0 and VCC (1), 恆0 error_count.v恆觸發reset故要給0, 詳情看硬體error_count module
  int count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);
  printf("Joint%c No%d counter = %d\n", pmotor->LR, pmotor->No, count);
} 

static void SingleStepControl(void)  //這邊保證能優化  暫時不管
{
  LR_tmp = LR_FB_tmp;
  No_tmp = No_FB_tmp;
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];  
  int tmp = 0;
  int move_mode = -1;
  int control_mode = -1;
  int type = -1;
  alt_8 *ch = '\0';
  while( ch == '\0' )
  {
    printf("Please choice move_mode, Normal=0, Tcurve=1, Scurve=2");
    scanf("%d", &tmp);    
    move_mode = tmp;
    if (move_mode == Move_Mode_Normal) ch = "Move_Mode_Normal";
    else if (move_mode == Move_Mode_Tcurve) ch = "Move_Mode_Tcurve";
    else if (move_mode == Move_Mode_Scurve) ch = "Move_Mode_Scurve";
    else  printf("ERROR!! invalid move_mode \n");
  }
  printf("You choice move mode:%s\n", ch);
  ch = '\0';
  while( ch == '\0' )
  {
    printf("Please choice control_mode, PID=0, FSMC=1");
    scanf("%d", &tmp);    
    control_mode = tmp;
    if (control_mode == Control_Mode_PID)
    {
      ch = "Control_Mode_PID";
      printf("You choice control mode mode:%s\n", ch);
      ch = '\0';      
      while( ch == '\0' )
      {
        printf("Please choice PID_type, PID=0, PD=1, PI=2");
        scanf("%d", &tmp);
        type = tmp;
        if (type == PID_Mode_PID)
        {
          ch = "Control_Mode_PID, PID type";
          pmotor->PID_type = PID_Mode_PID;
        }
        else if (type == PID_Mode_PD)
        {
          ch = "Control_Mode_PID, PD type";
          pmotor->PID_type = PID_Mode_PD;
        }
        else if (type == PID_Mode_PI)
        {
          ch = "Control_Mode_PID, PI type";
          pmotor->PID_type = PID_Mode_PI;
        }
        else  printf("ERROR!! invalid control_mode \n");        
      }                            
    }
    else if (control_mode == Control_Mode_FSMC) 
    {
      ch = "Control_Mode_FSMC";
      printf("You choice control mode mode:%s\n", ch);
      ch = '\0';      
      while( ch == '\0' )
      {
        printf("Please choice FSMC_type, Rel=0, Abs=1");
        scanf("%d", &tmp);
        type = tmp;
        if (type == FSMC_Mode_Rel)
        {
          ch = "Control_Mode_FSMC, Rel type";
          pmotor->FSMC_type = FSMC_Mode_Rel;
        }
        else if (type == FSMC_Mode_Abs)
        {
          ch = "Control_Mode_FSMC, Abs type";
          pmotor->FSMC_type = FSMC_Mode_Abs;
        }
        else  printf("ERROR!! invalid control_mode \n");        
      }                                  
      
    }
    else  printf("ERROR!! invalid control_mode \n");
  }
  printf("You choice control mode mode:%s\n", ch);
  while(1)
  {
    printf("Please input desired angle to rotate( %.1f ~ %.1f degree ):", pmotor->limitLower, pmotor->limitUpper);
    scanf("%d", &tmp);
    if( tmp > pmotor->limitUpper || tmp < pmotor->limitLower )
    {
      printf("ERROR!! ");
      printf("%d ", tmp);
      printf("Input angle out off range!\n");
    }
    else
    {
      break;
    }
  }
  SingleMoveJoint(tmp, pmotor, move_mode, control_mode);
}

static void StopAllJoint(void)
{
  // disable timer interrupt 
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  Init_PWM();  
}

static void SingleSetParameter_FSMC(void)
{
  LR_tmp = LR_FB_tmp;
  No_tmp = No_FB_tmp;
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];  
  int tmp = 0;  //fsmc_mode
  alt_8 *ch = '\0';  
  printf("Set parameter !\n");  
  printf("Please choice fsmc_mode, Relative = 0, Absolute = 1");
  scanf("%d", &tmp);    
  if (tmp == FSMC_Mode_Rel) ch = "FSMC_Mode_Rel";
  else if (tmp == FSMC_Mode_Abs) ch = "FSMC_Mode_Abs";
  else 
  {
    ch = "FSMC_Mode_Rel(default)";
    tmp = FSMC_Mode_Rel;  //default
  }
  printf("You choice fsmc mode:%s\n", ch);
  
  //此段為預留之後如果有例如第四軸會用到不同型(Rel, Abs, xxx...)在不同case下，
  //每個型又會有自己的參數例如lamda_Rel, lamda_Abs, 因此需要調整不同型的參數，故預留此打法, 
  //不過屆時Struct內的參數也要再新增, 或是再分支出來(Struct內包著Struct)
  //而目前某軸只有使用一種FSMC型，故無需做以上動作  
  float tmp1, tmp2, tmp3, tmp4;
  if (tmp == FSMC_Mode_Rel)
  {     
    printf("Please input desired param lamda, lamda_d, gu_rel_scale\n");
    printf("input type：10.0 20.0 30.0\n");
    printf("Before set, lamda, lamda_d, gu\n");
    printf("%f, %f, %f, %f\n", pmotor->lamda, pmotor->lamda_d, pmotor->lamda_dd, pmotor->gu_rel_scale);
    scanf("%f %f %f %f", &tmp1, &tmp2, &tmp3, &tmp4);
    pmotor->lamda = tmp1; 
    pmotor->lamda_d = tmp2;
    pmotor->lamda_dd = tmp3;
    pmotor->gu_rel_scale = tmp4;
    printf("After set, lamda, lamda_d, gu_rel_scale\n");
    printf("%f, %f, %f, %f\n", pmotor->lamda, pmotor->lamda_d, pmotor->lamda_dd, pmotor->gu_rel_scale);
  }
  else  //FSMC_Mode_Abs
  { 
    printf("Before set, lamda:%f, lamda_d:%f, gs[0]:%f, gs[1]:%f, gs[2]:%f, gu[0]:%f, gu[1]:%f, gu[2]:%f\n", 
    pmotor->lamda, pmotor->lamda_d, 
    pmotor->gs[0], pmotor->gs[1], pmotor->gs[2], pmotor->gu_abs[0], pmotor->gu_abs[1], pmotor->gu_abs[2]
    );    
    //等有要用到Abs再補充    
  }       
  usleep(1000000); //sleep 1 sec
}

static void SingleSetParameter_PID(void)
{
  LR_tmp = LR_FB_tmp;
  No_tmp = No_FB_tmp;
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];  
  int tmp = 0;  //fsmc_mode
  alt_8 *ch = '\0';  
  printf("Set parameter !\n");  
  printf("Please choice pid_mode, PID = 0, PD = 1, PI = 2"); // 預留
  scanf("%d", &tmp);    
  if (tmp == PID_Mode_PID) ch = "PID_Mode_PID";
  else if (tmp == PID_Mode_PD) ch = "PID_Mode_PD";
  else if (tmp == PID_Mode_PI) ch = "PID_Mode_PI";
  else 
  {
    ch = "PID_Mode_PID(default)";
    tmp = PID_Mode_PID;  //default
  }
  printf("You choice fsmc mode:%s\n", ch);
  
  //此段為預留之後如果有例如第四軸會用到不同型(Rel, Abs, xxx...)在不同case下，
  //每個型又會有自己的參數例如lamda_Rel, lamda_Abs, 因此需要調整不同型的參數，故預留此打法, 
  //不過屆時Struct內的參數也要再新增, 或是再分支出來(Struct內包著Struct)
  //而目前某軸只有使用一種FSMC型，故無需做以上動作  
  float tmp1, tmp2, tmp3, tmp4;
  if (tmp == Control_Mode_PID)
  {     
    printf("Please input desired param kp, ki, kd, kb\n");
    printf("input type：10.0 20.0 30.0 40.0\n");
    printf("Before set, kp, ki, kd\n");
    printf("%f, %f, %f %f\n", pmotor->kp, pmotor->ki, pmotor->kd, pmotor->kb);
    scanf("%f %f %f %f", &tmp1, &tmp2, &tmp3, &tmp4);
    pmotor->kp = tmp1; 
    pmotor->ki = tmp2;
    pmotor->kd = tmp3;
    pmotor->kb = tmp4;
    printf("After set, kp, ki, kd, kb\n");
    printf("%f, %f, %f, %f\n", pmotor->kp, pmotor->ki, pmotor->kd, pmotor->kb);
  }
  else if (tmp == PID_Mode_PD)//PD...
  { 
  }
  else if (tmp == PID_Mode_PI)//PI...
  { 
  }              
  usleep(1000000); //sleep 1 sec
}
  

static void SingleMoveJoint(float _degree, PtrMotor pmotor, int _move_mode, int _control_mode)  //deg2count, 在matlab做
{
  time = 0.0;
  cnt_isr_Xms = 1; 
  flag_print = 0;
  num = 0;
 
  pmotor->refVal = _degree * pmotor->deg2count;
  printf("default degree:1 for exit limit switch\n");
  printf("SingleMoveJoint: Moving degree:%f\n", _degree);
  printf("SingleMoveJoint: count = %d\n\n", pmotor->refVal);  
  Init_Controller_Variable(pmotor);
  SingleJointInit((void*)TIMER_20MS_BASE, TIMER_20MS_IRQ, TIMER_20MS_FREQ, _move_mode, _control_mode);

  while( cnt_isr_Xms <= time_print )  
  { 
    if( flag_print== 1 )  //每20ms看, 已測試過可正常print完全部      
    {                    
      //float Tref = pmotor->Tref / pmotor->deg2count;  //for Tcurve、Scurve、Normal, err = ref-count, 省print的時間及僅輸出必要的data(精簡)      
      printf("%.2f ", time);    
      printf("%d ", pmotor->count);          
      printf("%d ", pmotor->error);
      printf("%d ", pmotor->diff);          
      printf("%d ", pmotor->error_diff);
      printf("%d ", pmotor->diff_d);
      printf("%d ", pmotor->error_diff_d);  //視情況到時候加速度可能都捨棄用gradient或是捨棄一個剩下的ref a 再額外執行然後扣掉當前                           
      printf("%d ", pmotor->controlVal);        
      printf("%d ", p_tmp);
      printf("%d ", i_tmp);
      printf("%d ", d_tmp);
      printf("%d ", i_anti_tmp);       
      printf("%.3f ", fuck_u_tmp);
      printf("%.3f ", weight);
      printf("%.3f ", weight_sum);                
      printf("\n");
      flag_print = 0;
    }
  }
  printf("flag_print %d\n", flag_print);

  // disable LS interrupt
  //IOWR_ALTERA_AVALON_PIO_IRQ_MASK(ROBOT_LS_BASE, 0x0);  //我覺得這裡不應該關極限開關, 但以前的人都有關??? 
  // disable timer interrupt  //為因應3.4軸所以決定手動關 Timer, PWM, 否則其他軸動的時候都會因3.4軸沒辦法卡住飄掉, 但目前寫法無法如期(選擇關節的時候LR_tmp, No_tmp會跑掉, ISR內的pmotor就跟著變動了
  //IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  // stop joint
  //IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, 0);  //維持最後控制輛之電壓  
}

static void SingleJointInit(void* base, alt_u32 irq, alt_u32 freq, int _move_mode, int _contorl_mode)
{
  /* set the system clock frequency */
  alt_sysclk_init (freq);

  /* set to free running mode */
  IOWR_ALTERA_AVALON_TIMER_CONTROL (base,
            ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  |
            ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
            ALTERA_AVALON_TIMER_CONTROL_START_MSK);
            
  /* register the interrupt handler, and enable the interrupt */
  
  if (_contorl_mode == Control_Mode_PID)
  {
    if (_move_mode == Move_Mode_Normal)
    {    
      time_print = 300;  //0.02s * 300 = 6sec, 無規劃模式理論上若你的控制器參數不好且移動距離又很遠的話,有可能還沒追到6秒就到了，然後停下來XD
      flag_print = 0;
      alt_irq_register (irq, base, SingleJoint_PID_ISR);
    }
    else if (_move_mode == Move_Mode_Tcurve)
    {    
      SingleJointInit_Tcurve();
      time_print = round(TIME+1) * 50;  //1cnt / 0.02s => 50cnt / 1s, 0.005 => 200cnt 
      flag_print = 0;  //為了程式一致性才改來這否則原本在外面的Tcurve不影響
      alt_irq_register (irq, base, SingleJoint_Tcurve_PID_ISR);
    }
    else if (_move_mode == Move_Mode_Scurve)
    {    
      SingleJointInit_Scurve();
      time_print = round(TIME+1) * 50;  //1cnt / 0.02ms => 50cnt / 1s      
      flag_print = 0;  //加在這才有效, 不然有時候都只能跑一次之後的就不會printf了, 原因未知  
      alt_irq_register (irq, base, SingleJoint_Scurve_PID_ISR);           
    }           
  }
  else  // _contorl_mode == Control_Mode_Fsmc
  {
    if (_move_mode == Move_Mode_Normal)
    {    
      time_print = 300;  //0.02s * 300 = 6sec, 無規劃模式理論上若你的控制器參數不好且移動距離又很遠的話,有可能還沒追到6秒就到了，然後停下來XD
      flag_print = 0;
      alt_irq_register (irq, base, SingleJoint_FSMC_ISR);
    }
    else if (_move_mode == Move_Mode_Tcurve)
    {    
      SingleJointInit_Tcurve();
      time_print = round(TIME+1) * 50;  //1cnt / 0.02s => 50cnt / 1s, 0.005 => 200cnt 
      flag_print = 0;  //為了程式一致性才改來這否則原本在外面的Tcurve不影響   
      alt_irq_register (irq, base, SingleJoint_Tcurve_FSMC_ISR);
    }
    else if (_move_mode == Move_Mode_Scurve)
    {    
      SingleJointInit_Scurve();
      time_print = round(TIME+1) * 50;  //1cnt / 0.02ms => 50cnt / 1s      
      flag_print = 0;  //加在這才有效, 不然有時候都只能跑一次之後的就不會printf了, 原因未知    
      alt_irq_register (irq, base, SingleJoint_Scurve_FSMC_ISR);           
    }              
  } 
}

static void SingleJointInit_UART(void* base, alt_u32 irq, alt_u32 freq)
{
  /* set the system clock frequency */
  alt_sysclk_init (freq);

  /* set to free running mode */
  IOWR_ALTERA_AVALON_TIMER_CONTROL (base,
            ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  |
            ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
            ALTERA_AVALON_TIMER_CONTROL_START_MSK);
            
  /* register the interrupt handler, and enable the interrupt */
  
  flag_print = 0;
  alt_irq_register (irq, base, SingleJoint_FSMC_UART_ISR);
}

static void SingleJointInit_Tcurve(void)
{
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];        
  pmotor->Tcnt = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);  //速度規劃前的當前位置
  
  float xd = 0.0;//目標和當前位置的位移量, unit:degree, 因每軸必須在同一個scale下進行比較
  int  flag = 0;  
  float Ta = 0.0;
  float v = 0.0;
  float a = 0.0;
  int ref = pmotor->refVal;
  signed int  cnt = pmotor->Tcnt;
  float deg2count = pmotor->deg2count;
  float Vmax = pmotor->Vmax;
  float Amax = pmotor->Amax;
  float Time = pmotor->Time;
  
  if (ref >= cnt)
  {
    xd = (ref - cnt)/deg2count;
    flag = 1;
  }
  else
  {
    xd = (cnt - ref)/deg2count;
    flag = 0;
  }  
     
  if (xd >= powf(Vmax, 2) / Amax)
  {
    Ta = Vmax / Amax;
    Time = xd / Vmax +Ta;
    v = Vmax;
  }
  else
  {
    Ta = sqrt(xd / Amax);
    Time = 2 * Ta;
    v = sqrt(xd * Amax);
  }
  a = Amax;
  
  //單軸 Tcurve 恆最大 無需比較
  
  pmotor->Tflag = flag;
  pmotor->Ta = Ta;
  pmotor->v = v;
  pmotor->a = a;
  pmotor->Time = Time;
  TIME = Time;
  
  alt_u8 ch = '\0';
  if (LR_tmp == L) ch = 'L';
  else if (LR_tmp == R) ch = 'R';
  printf("arm %c%d\n", ch, No_tmp);
  printf("目標, 當下位置, 位移, 總時間, 加速度段開始時間\n");
  printf("ref = %d, cnt = %d, xd = %.3f, Time = %.3f, Ta = %.3f\n", pmotor->refVal, pmotor->Tcnt , xd, Time, Ta);
  printf("ref = %d, cnt = %d, xd = %.3f, Time = %.3f, Ta = %.3f\n", pmotor->refVal, pmotor->Tcnt , xd, pmotor->Time, pmotor->Ta);
  printf("最大速度, 最大加速度, 斜率\n");
  printf("v = %.3f, a = %.3f, flag = %d\n\n", v, a, flag);
  printf("v = %.3f, a = %.3f, flag = %d\n\n", pmotor->v, pmotor->a, pmotor->Tflag);     
}

static void SingleJointInit_Scurve(void)
{
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];        
  pmotor->Tcnt = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);  //速度規劃前的當前位置
  memset(&pmotor->t0, 0.0, sizeof(pmotor->t0[0]) * 8);
  memset(&pmotor->a0, 0.0, sizeof(pmotor->a0[0]) * 8);
  memset(&pmotor->v0, 0.0, sizeof(pmotor->v0[0]) * 8);
  memset(&pmotor->p0, 0.0, sizeof(pmotor->p0[0]) * 8);
  
  float xd = 0.0;//目標和當前位置的位移量, unit:degree, 因每軸必須在同一個scale下進行比較
  int  flag = 0;  
  float Ta = 0.0;
  float Ts = 0.0;
  float v = 0.0;
  float a = 0.0;  
  float j = 0.0;
  int ref = pmotor->refVal;
  int  cnt = pmotor->Tcnt;
  float deg2count = pmotor->deg2count;
  float Time = pmotor->Time;
  float Vmax = pmotor->Vmax;
  float Amax = pmotor->Amax;
  float s_factor = pmotor->s_factor;
  float Aave = (1.0 - 0.5*s_factor) * Amax;
    
  if (ref >= cnt)
  {
    xd = (ref - cnt)/deg2count;
    flag = 1;
  }
  else
  {
    xd = (cnt - ref)/deg2count;
    flag = 0;
  }  
  
  //單軸 Tcurve 恆最大 無需比較  
  
  if (xd >= powf(Vmax, 2) / Aave)
  {
    Ta = Vmax / Aave;
    Time = xd / Vmax + Ta;
    Ts = (0.5 * s_factor) * Ta;        
  }
  else
  {
    Ta = sqrt(xd / Aave);
    Time = 2 * Ta;
    Ts = (0.5 * s_factor) * Ta;            
  }
  v = xd / (Time - Ta);  // 驗算最大速度, 若距離過短不會等於Vmax
  a = v / (Ta - Ts);  // 驗算最大加速度
    
  printf("最終的最大速度:%f, 最終的最大加速度:%f \n", v, a); 
  
  if (Ts == 0)  j = 0; // 避免計算j時分母為零
  else  j = Amax / Ts; // 急衝度, 此處不是 Aave
  
  alt_u8 ch = '\0';
  if (LR_tmp == L) ch = 'L';
  else if (LR_tmp == R) ch = 'R';
  printf("arm %c%d\n", ch, No_tmp);

  float t0[8] = {0.0, Ts, Ta-Ts, Ta, Time-Ta, Time-Ta+Ts, Time-Ts, Time};
  float a0[8] = {0.0, a, a, 0.0, 0.0, -a, -a, 0.0};  
  float v0[8];
  v0[0] = 0;
  v0[1] = 0.5*j*powf(t0[1],2); //用^2會有隱性轉換的錯誤
  v0[2] = a0[1]*(t0[2]-t0[1])+v0[1];
  v0[3] = v;
  v0[4] = v;
  v0[5] = -0.5*j*powf((t0[5]-t0[4]),2)+v0[4];
  v0[6] = a0[5]*(t0[6]-t0[5])+v0[5];
  v0[7] = 0.5*j*powf((t0[7]-t0[6]),2)+a0[6]*(t0[7]-t0[6])+v0[6];
  float p0[8];
  p0[0] = 0; 
  p0[1] = (1.0/6)*j*powf(t0[1],3);  //1/6會變0, 要打1.0/6
  p0[2] = 0.5*a0[1]*powf((t0[2]-t0[1]),2)+v0[1]*(t0[2]-t0[1])+p0[1];
  p0[3] = -(1.0/6)*j*powf((t0[3]-t0[2]),3)+0.5*a0[2]*powf((t0[3]-t0[2]),2)+v0[2]*(t0[3]-t0[2])+p0[2];
  p0[4] = v0[3]*(t0[4]-t0[3])+p0[3];
  p0[5] = -(1.0/6)*j*powf((t0[5]-t0[4]),3)+v0[4]*(t0[5]-t0[4])+p0[4];
  p0[6] = 0.5*a0[5]*powf((t0[6]-t0[5]),2)+v0[5]*(t0[6]-t0[5])+p0[5];
  p0[7] = (1.0/6)*j*powf((t0[7]-t0[6]),3)+0.5*a0[6]*powf((t0[7]-t0[6]),2)+v0[6]*(t0[7]-t0[6])+p0[6];   
  
  printf("所規劃的時間點:\n");
  printf("t0, t1, t2, t3, t4, t5, t6, t7\n"); //8個時間點7個時段
  int i = 0;  
  for (i = 0;i < 8;i++)  
  {
     printf("%.3f, ", t0[i]);
     pmotor->t0[i] = t0[i];     
  }
  printf("\n");
  
  printf("各時間點的加速度:\n");
  printf("a0, a1, a2, a3, a4, a5, a6, a7\n");
  int z = 0;  
  for (z = 0; z < 8; z++)  
  {
     printf("%.3f, ", a0[z]);
     pmotor->a0[z] = a0[z];     
  }
  printf("\n");
  
  printf("各時間點的速度:\n");
  printf("v0, v1, v2, v3, v4, v5, v6, v7\n");
  int x  = 0;  
  for (x = 0;x < 8;x++)  
  {
     printf("%.3f, ", v0[x]);
     pmotor->v0[x] = v0[x];     
  }
  printf("\n");
  
  printf("各時間點的位置:\n");
  printf("p0, p1, p2, p3, p4, p5, p6, p7\n");
  int y = 0;  
  for (y = 0;y < 8;y++) 
  {
     printf("%.3f, ", p0[y]);
     pmotor->p0[y] = p0[y];     
  }
  printf("\n");
                               
  pmotor->Tflag = flag;
  pmotor->Ta = Ta;
  pmotor->Ts = Ts;
  pmotor->v = v;
  pmotor->a = a;
  pmotor->j = j;
  pmotor->Time = Time;
  TIME = Time;  //TIME = Time = t0[7];
  
  printf("目標, 當下位置, 位移, 總時間, 等加速度段開始時間, 等速度段開始時間\n");  
  printf("ref = %d, cnt = %d, xd = %.3f, Time = %.3f, Ts = %.3f, Ta = %.3f\n", pmotor->refVal, pmotor->Tcnt , xd, Time, Ts, Ta);
  printf("ref = %d, cnt = %d, xd = %.3f, Time = %.3f, Ts = %.3f, Ta = %.3f\n", pmotor->refVal, pmotor->Tcnt , xd, pmotor->Time, pmotor->Ts, pmotor->Ta);
  
  printf("急衝度, 最大加速度, 最大速度, 斜率\n");
  printf("j = %.3f, a = %.3f, v = %.3f, flag = %d\n", j, a, v, flag);
  printf("j = %.3f, a = %.3f, v = %.3f, flag = %d\n", pmotor->j, pmotor->a, pmotor->v, pmotor->Tflag);   
}

static void SingleJoint_FSMC_ISR(void* base, alt_u32 id)
{          
  time = (double)(sample_time * cnt_isr_Xms); //time
        
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];  

  // get encoder signal        
  pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);            
  pmotor->diff = pmotor->count - pmotor->count_old;
  pmotor->diff_d = pmotor->diff - pmotor->diff_old;
  pmotor->displacement += pmotor->diff;
  pmotor->error = pmotor->refVal - pmotor->displacement;    
  pmotor->error_diff = pmotor->error - pmotor->error_old;
  pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;      
  if (pmotor->FSMC_type == FSMC_Mode_Rel)
  {     
    RelFSMC(pmotor);
  }
  else  //FSMC_Mode_Abs 
  {     
    AbsFSMC(pmotor);
  }     
  pmotor->count_old = pmotor->count;
  pmotor->diff_old = pmotor->diff;
  pmotor->error_old = pmotor->error;
  pmotor->error_diff_old = pmotor->error_diff;  

  flag_print += 1;   
  cnt_isr_Xms += 1;  
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();       
}

static void SingleJoint_FSMC_UART_ISR(void* base, alt_u32 id)
{          
  time = (double)(sample_time * cnt_isr_Xms); //time
        
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];  

  pmotor->refVal = refVal_list[0][1]* pmotor->deg2count;  
  
  // get encoder signal        
  pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);            
  pmotor->diff = pmotor->count - pmotor->count_old;
  pmotor->diff_d = pmotor->diff - pmotor->diff_old;
  pmotor->displacement += pmotor->diff;
  pmotor->error = pmotor->refVal - pmotor->displacement;    
  pmotor->error_diff = pmotor->error - pmotor->error_old;
  pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;      
  if (pmotor->FSMC_type == FSMC_Mode_Rel)
  {     
    RelFSMC(pmotor);
  }
  else  //FSMC_Mode_Abs 
  {     
    AbsFSMC(pmotor);
  }     
  pmotor->count_old = pmotor->count;
  pmotor->diff_old = pmotor->diff;
  pmotor->error_old = pmotor->error;
  pmotor->error_diff_old = pmotor->error_diff;  

//  time_arr[num] = time;
  count_arr[num] = pmotor->count;
  error_arr[num] = pmotor->error;
  controlVal_arr[num] = pmotor->controlVal;  
  num += 1;     

  flag_print += 1;   
  cnt_isr_Xms += 1;  
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();       
}

static void SingleJoint_Tcurve_FSMC_ISR(void* base, alt_u32 id)
{     
  int xd = 0;  //Tcurve所規劃的點(較遠的軸以最大速度規劃), unit:count, 每個時間點各軸所要各別追的點, 最終會在同時間點一起到達各自所要到的點
  time = (double)(sample_time * cnt_isr_Xms); //time
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];
  float deg2count = pmotor->deg2count;
  float Ta = pmotor->Ta;
  float v = pmotor->v;
  float a = pmotor->a;
  signed int  cnt = pmotor->Tcnt;
  
  if (time < Ta)
  {
    if (pmotor->Tflag ==1 )  xd = cnt + Tpart1(a, time)*deg2count;  //當時規劃的scale是degree, 要再轉回count
    else  xd = cnt - Tpart1(a, time)*deg2count;
  }
  else if ((time >= Ta) && (time <= (TIME - Ta)))
  {
    if (pmotor->Tflag == 1)  xd = cnt + Tpart2(a, v, Ta, time)*deg2count;
    else  xd = cnt - Tpart2(a, v, Ta, time)*deg2count;
  }
  else if ((time > (TIME - Ta)) && (time < TIME))
  {
    if (pmotor->Tflag == 1)  xd = cnt + Tpart3(a, v, Ta, time)*deg2count;
    else  xd = cnt - Tpart3(a, v, Ta, time)*deg2count;
  }
  else if (time >= TIME)
  {
    xd = pmotor->refVal;
  }
  
  pmotor->Tref = xd;  //為了check 速度規劃曲線所追的位置軌跡
  // get encoder signal      
  pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);            
  pmotor->diff = pmotor->count - pmotor->count_old;
  pmotor->diff_d = pmotor->diff - pmotor->diff_old;
  pmotor->displacement += pmotor->diff;
  pmotor->error = xd - pmotor->displacement;    
  pmotor->error_diff = pmotor->error - pmotor->error_old;
  pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;      
  if (pmotor->FSMC_type == FSMC_Mode_Rel)
  {     
    RelFSMC(pmotor);
  }
  else  //FSMC_Mode_Abs 
  {     
    AbsFSMC(pmotor);
  }     
  pmotor->count_old = pmotor->count;
  pmotor->diff_old = pmotor->diff;
  pmotor->error_old = pmotor->error;
  pmotor->error_diff_old = pmotor->error_diff;
  
//  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, 153);
//  IOWR_ALTERA_AVALON_PIO_DATA(MOTOR_R_PWM1_BASE, 153);
    
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();      
}

static void SingleJoint_Scurve_FSMC_ISR(void* base, alt_u32 id)
{
  int xd = 0;  //Scurve所規劃的點(較遠的軸以最大速度規劃), unit:count, 每個時間點各軸所要各別追的點, 最終會在同時間點一起到達各自所要到的點
  time = (double)(sample_time * cnt_isr_Xms); //time
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];
  float deg2count = pmotor->deg2count;  
  float j = pmotor->j;    
  signed int  cnt = pmotor->Tcnt;
  float at, vt, pt;  //acc_tmp, vel_tmp, pos_tmp;
  float *t0 = pmotor->t0; //same with, float *t0 = &pmotor->t0[0];
  float *a0 = pmotor->a0;
  float *v0 = pmotor->v0; 
  float *p0 = pmotor->p0;    
  
  if (time <= t0[1])
  {
    pt = (1.0/6)*j*powf(time,3);
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;          
    else     
      xd = cnt - pt*deg2count;             
  }
  else if ((t0[1] < time) && (time < t0[2]))
  {
    pt = 0.5*a0[1]*powf((time-t0[1]),2)+v0[1]*(time-t0[1])+p0[1];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;    
    else     
      xd = cnt - pt*deg2count;        
  }
  else if ((t0[2] <= time) && (time <= t0[3]))
  {
    pt = -(1.0/6)*j*powf((time-t0[2]),3)+0.5*a0[2]*powf((time-t0[2]),2)+v0[2]*(time-t0[2])+p0[2];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;   
    else     
      xd = cnt - pt*deg2count;    
  }
  else if ((t0[3] < time) && (time < t0[4]))
  {
    pt = v0[3]*(time-t0[3])+p0[3];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;    
    else     
      xd = cnt - pt*deg2count;     
  }
  else if ((t0[4] <= time) && (time <= t0[5]))
  {
    pt = -(1.0/6)*j*powf((time-t0[4]),3)+v0[4]*(time-t0[4])+p0[4];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;     
    else     
      xd = cnt - pt*deg2count;     
  }
  else if ((t0[5] < time) && (time < t0[6]))
  {
    pt = 0.5*a0[5]*powf((time-t0[5]),2)+v0[5]*(time-t0[5])+p0[5];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;     
    else     
      xd = cnt - pt*deg2count;     
  }
  else if ((t0[6] <= time) && (time <= t0[7]))
  {
    pt = (1.0/6)*j*powf((time-t0[6]),3)+0.5*a0[6]*powf((time-t0[6]),2)+v0[6]*(time-t0[6])+p0[6];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;     
    else     
      xd = cnt - pt*deg2count;     
  }
  else if (t0[7] < time)
  {
    xd = pmotor->refVal;
  }
  
  pmotor->Tref = xd;  //為了check 速度規劃曲線所追的位置軌跡
  // get encoder signal      
  pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);            
  pmotor->diff = pmotor->count - pmotor->count_old;
  pmotor->diff_d = pmotor->diff - pmotor->diff_old;
  pmotor->displacement += pmotor->diff;
  pmotor->error = xd - pmotor->displacement;    
  pmotor->error_diff = pmotor->error - pmotor->error_old;
  pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;      
  if (pmotor->FSMC_type == FSMC_Mode_Rel)
  {     
    RelFSMC(pmotor);
  }
  else  //FSMC_Mode_Abs 
  {     
    AbsFSMC(pmotor);
  }     
  pmotor->count_old = pmotor->count;
  pmotor->diff_old = pmotor->diff;
  pmotor->error_old = pmotor->error;
  pmotor->error_diff_old = pmotor->error_diff;
    
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);       
  alt_tick(); 
}

static void SingleJoint_PID_ISR(void* base, alt_u32 id)
{
  time = (double)(sample_time * cnt_isr_Xms); //time
        
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];
  // get encoder signal         
  pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);            
  pmotor->diff = pmotor->count - pmotor->count_old;
  pmotor->diff_d = pmotor->diff - pmotor->diff_old;
  pmotor->displacement += pmotor->diff;
  pmotor->error = pmotor->refVal - pmotor->displacement;    
  pmotor->error_diff = pmotor->error - pmotor->error_old;
  pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;           
  if (pmotor->PID_type == PID_Mode_PID)
  {     
    PID(pmotor);
  }
  else if (pmotor->PID_type == PID_Mode_PD)
  {
    PD(pmotor);
  }
  else if (pmotor->PID_type == PID_Mode_PI)
  {
    PI(pmotor);
  } 
  pmotor->count_old = pmotor->count;
  pmotor->diff_old = pmotor->diff;
  pmotor->error_old = pmotor->error;
  pmotor->error_diff_old = pmotor->error_diff;    
  
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);       
  alt_tick();   
}
static void SingleJoint_Tcurve_PID_ISR(void* base, alt_u32 id)
{
  int xd = 0;  //Tcurve所規劃的點(較遠的軸以最大速度規劃), unit:count, 每個時間點各軸所要各別追的點, 最終會在同時間點一起到達各自所要到的點
  time = (double)(sample_time * cnt_isr_Xms); //time
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];
  float deg2count = pmotor->deg2count;
  float Ta = pmotor->Ta;
  float v = pmotor->v;
  float a = pmotor->a;
  signed int  cnt = pmotor->Tcnt;
  
  if (time < Ta)
  {
    if (pmotor->Tflag ==1 )  xd = cnt + Tpart1(a, time)*deg2count;  //當時規劃的scale是degree, 要再轉回count
    else  xd = cnt - Tpart1(a, time)*deg2count;
  }
  else if ((time >= Ta) && (time <= (TIME - Ta)))
  {
    if (pmotor->Tflag == 1)  xd = cnt + Tpart2(a, v, Ta, time)*deg2count;
    else  xd = cnt - Tpart2(a, v, Ta, time)*deg2count;
  }
  else if ((time > (TIME - Ta)) && (time < TIME))
  {
    if (pmotor->Tflag == 1)  xd = cnt + Tpart3(a, v, Ta, time)*deg2count;
    else  xd = cnt - Tpart3(a, v, Ta, time)*deg2count;
  }
  else if (time >= TIME)
  {
    xd = pmotor->refVal;
  }
  
  pmotor->Tref = xd;  //為了check 速度規劃曲線所追的位置軌跡
  // get encoder signal      
  pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);            
  pmotor->diff = pmotor->count - pmotor->count_old;
  pmotor->diff_d = pmotor->diff - pmotor->diff_old;
  pmotor->displacement += pmotor->diff;
  pmotor->error = xd - pmotor->displacement;    
  pmotor->error_diff = pmotor->error - pmotor->error_old;
  pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;    
  if (pmotor->PID_type == PID_Mode_PID)
  {     
    PID(pmotor);
  }
  else if (pmotor->PID_type == PID_Mode_PD)
  {
    PD(pmotor);
  }
  else if (pmotor->PID_type == PID_Mode_PI)
  {
    PI(pmotor);
  }           
  pmotor->count_old = pmotor->count;
  pmotor->diff_old = pmotor->diff;
  pmotor->error_old = pmotor->error;
  pmotor->error_diff_old = pmotor->error_diff;  
 
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);       
  alt_tick();   

}
static void SingleJoint_Scurve_PID_ISR(void* base, alt_u32 id)
{
  int xd = 0;  //Scurve所規劃的點(較遠的軸以最大速度規劃), unit:count, 每個時間點各軸所要各別追的點, 最終會在同時間點一起到達各自所要到的點
  time = (double)(sample_time * cnt_isr_Xms); //time
  PtrMotor pmotor = motor_list[LR_tmp][No_tmp];
  float deg2count = pmotor->deg2count;  
  float j = pmotor->j;    
  signed int  cnt = pmotor->Tcnt;
  float at, vt, pt;  //acc_tmp, vel_tmp, pos_tmp;
  float *t0 = pmotor->t0; //same with, float *t0 = &pmotor->t0[0];
  float *a0 = pmotor->a0;
  float *v0 = pmotor->v0; 
  float *p0 = pmotor->p0;    
  
  if (time <= t0[1])
  {
    pt = (1.0/6)*j*powf(time,3);
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;          
    else     
      xd = cnt - pt*deg2count;             
  }
  else if ((t0[1] < time) && (time < t0[2]))
  {
    pt = 0.5*a0[1]*powf((time-t0[1]),2)+v0[1]*(time-t0[1])+p0[1];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;    
    else     
      xd = cnt - pt*deg2count;        
  }
  else if ((t0[2] <= time) && (time <= t0[3]))
  {
    pt = -(1.0/6)*j*powf((time-t0[2]),3)+0.5*a0[2]*powf((time-t0[2]),2)+v0[2]*(time-t0[2])+p0[2];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;   
    else     
      xd = cnt - pt*deg2count;    
  }
  else if ((t0[3] < time) && (time < t0[4]))
  {
    pt = v0[3]*(time-t0[3])+p0[3];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;    
    else     
      xd = cnt - pt*deg2count;     
  }
  else if ((t0[4] <= time) && (time <= t0[5]))
  {
    pt = -(1.0/6)*j*powf((time-t0[4]),3)+v0[4]*(time-t0[4])+p0[4];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;     
    else     
      xd = cnt - pt*deg2count;     
  }
  else if ((t0[5] < time) && (time < t0[6]))
  {
    pt = 0.5*a0[5]*powf((time-t0[5]),2)+v0[5]*(time-t0[5])+p0[5];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;     
    else     
      xd = cnt - pt*deg2count;     
  }
  else if ((t0[6] <= time) && (time <= t0[7]))
  {
    pt = (1.0/6)*j*powf((time-t0[6]),3)+0.5*a0[6]*powf((time-t0[6]),2)+v0[6]*(time-t0[6])+p0[6];
    if (pmotor->Tflag ==1 )    
      xd = cnt + pt*deg2count;     
    else     
      xd = cnt - pt*deg2count;     
  }
  else if (t0[7] < time)
  {
    xd = pmotor->refVal;
  }  
  
  pmotor->Tref = xd;  //為了check 速度規劃曲線所追的位置軌跡
  // get encoder signal      
  pmotor->count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base);            
  pmotor->diff = pmotor->count - pmotor->count_old;
  pmotor->diff_d = pmotor->diff - pmotor->diff_old;
  pmotor->displacement += pmotor->diff;
  pmotor->error = xd - pmotor->displacement;    
  pmotor->error_diff = pmotor->error - pmotor->error_old;
  pmotor->error_diff_d = pmotor->error_diff - pmotor->error_diff_old;      
  if (pmotor->PID_type == PID_Mode_PID)
  {     
    PID(pmotor);
  }
  else if (pmotor->PID_type == PID_Mode_PD)
  {
    PD(pmotor);
  }
  else if (pmotor->PID_type == PID_Mode_PI)
  {
    PI(pmotor);
  }        
  pmotor->count_old = pmotor->count;
  pmotor->diff_old = pmotor->diff;
  pmotor->error_old = pmotor->error;
  pmotor->error_diff_old = pmotor->error_diff;
      
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);       
  alt_tick();   
}

static int compare5(float a, float b, float c, float d, float e)
{
    int temp = 0;
    if(      a>=b && a>=c && a>=d && a>=e ) temp = 1;
    else if( b>=a && b>=c && b>=d && b>=e ) temp = 2;
    else if( c>=a && c>=b && c>=d && c>=e ) temp = 3;
    else if( d>=a && d>=b && d>=c && d>=e ) temp = 4;
    else if( e>=a && e>=b && e>=c && e>=d ) temp = 5;
    return temp;
}

static float Tpart1(float _a, float _t) //accelrate, current time
{
  float xd = 0.5*_a*pow(_t,2);
  return xd;
}

static float Tpart2(float _a, float _v, float _Ta, float _t)
{
  float xd = (_v)*(_t-_Ta) + 0.5*_a*pow(_Ta,2);
  return xd;
}

static float Tpart3(float _a, float _v, float _Ta, float _t)
{
  float xd = (_v)*(TIME-_Ta) - 0.5*_a*pow((_t-TIME),2);
  return xd;
}

static void AbsFSMC(PtrMotor pmotor)
{
//  pmotor->controlVal = (pmotor->lamda) * pmotor->error + (pmotor->lamda_d) * (pmotor->error_diff);
  float RoughRel = pmotor->deg2count;     
  float ThinSlid = RoughRel*0.05; // 精調gain 
//  pmotor->lamda = 2.0;
//  pmotor->lamda_d = 1.0;
  float sliding_value = (pmotor->lamda_d) * (pmotor->error_diff) + (pmotor->lamda) * pmotor->error;    
  float gs = 0.0; //gs_tmp
//  float gu = 0.0; //gu_tmp
  float gu = pmotor->gu_rel_scale * pmotor->controlMax;  //每次變化量為controlMax 的 XX %
  float u = 0.0;
  
  if (abs(sliding_value) > RoughRel )
  {
    gs = RoughRel;
//    gu = 4095*0.7;  
  }
  else 
  {
    if (abs(sliding_value) < ThinSlid)
    {   
      sliding_value = 0.0;
      gu = 0.0;
      gs = 1.0;
    }   
    else
    {
      gs = abs(sliding_value)+ThinSlid;
//      gu = 4095*0.2 + abs(sliding_value) / gs;
      gu = exp(abs(gs)/RoughRel-1)*gs;
    }      
  }  

  if(pmotor->FMSC_rule_type == FSMC_ruleSym)
  { 
    u = FSMC1( gs, gu, sliding_value, fsmc_vector); 
  }
  else  //FSMC_ruleASym
  { 
    u = FSMC2( gs, gu, sliding_value, fsmc_vector); 
  }    
   
  pmotor->controlVal = round(u);  //四捨五入
  fuck_u_tmp = u;
          
  if (pmotor->controlVal >= 0)
  { 
    pmotor->dir = pmotor->dir_forward;
  }
  else  //(pmotor->controlVal < 0)
  { 
    pmotor->dir = pmotor->dir_backward;
//    pmotor->controlVal = -pmotor->controlVal;
  }
  if (pmotor->controlVal > pmotor->controlMax)
  { 
    pmotor->controlVal = pmotor->controlMax; 
  }
   
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, abs(pmotor->controlVal));
}

static void RelFSMC(PtrMotor pmotor)
{
  float lamda = pmotor->lamda;
  float lamda_d = pmotor->lamda_d;
  float lamda_dd = pmotor->lamda_dd;
  float err = pmotor->error;
  float err_d = pmotor->error_diff;
  float err_dd = pmotor->error_diff_d;
  float sliding_value = lamda*err + lamda_d*err_d + lamda_dd*err_dd;      
  float gs = pmotor->deg2count*pmotor->gs_scale;
  //float gu = pmotor->gu_rel_scale * pmotor->gu_rel;  //每次變化量為fullWidth 的 XX %
  float gu = pmotor->gu_rel_scale * pmotor->controlMax;  //每次變化量為controlMax 的 XX %
  float u_f = 0.0;
  int u_i = 0;
    
  if (pmotor->FMSC_rule_type == FSMC_ruleSym)
  { 
    u_f = FSMC1( gs, gu, sliding_value, fsmc_vector);   
  }
  else
  { 
    u_f = FSMC2( gs, gu, sliding_value, fsmc_vector); 
  }
  fuck_u_tmp = u_f;
  u_i = round(u_f);  //四捨五入
         
  if (cnt_isr_Xms == 1) //first
//  if (cnt_isr_Xms == 0) //讓他進不去
  {
    if (u_i >= 0)
    { 
      pmotor->controlVal = pmotor->lowerWidth + u_i; 
    }
    else        
    { 
      pmotor->controlVal = -pmotor->lowerWidth + u_i; 
    } 
  }  
  else
  {
    pmotor->controlVal = pmotor->controlVal + u_i;   
//    if (pmotor->dir == pmotor->dir_forward)    
//    { 
//      pmotor->controlVal = pmotor->controlVal + u_f; 
//    }    
//    else  //pmotor->dir == pmotor->dir_backward
//    { 
//      pmotor->controlVal = -(pmotor->controlVal) + u_f; 
//    }
  }
  if (pmotor->controlVal >= 0)
  { 
    pmotor->dir = pmotor->dir_forward;
  }
  else  //(pmotor->controlVal < 0)
  { 
    pmotor->dir = pmotor->dir_backward;
//    pmotor->controlVal = -pmotor->controlVal;
  }
  if (pmotor->controlVal > pmotor->controlMax)
    pmotor->controlVal = pmotor->controlMax; 
  else if(pmotor->controlVal < -pmotor->controlMax)
    pmotor->controlVal = -pmotor->controlMax;
   
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, abs(pmotor->controlVal));
//  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir_backward);
//  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, 255);
}


float FSMC1(float _gs, float _gu, float x, float *v)  //x:silding value, *v:fsmcvector
{
  //fuzzify
  int i;
  for(i=0;i<11;i++)
    v[i]=0.0;

  if(x>=1.0*_gs)
    v[10]=1.0;
  else if(x>=0.8*_gs && x<=1.0*_gs)
  {
    v[10]=5.0/_gs*x-4.0;
    v[9]=-5.0/_gs*x+5.0;
  }
  else if(x>=0.6*_gs && x<=0.8*_gs)
  {
    v[9]=5.0/_gs*x-3.0;
    v[8]=-5.0/_gs*x+4.0;
  }
  else if(x>=0.4*_gs && x<=0.6*_gs)
  {
    v[8]=5.0/_gs*x-2.0;
    v[7]=-5.0/_gs*x+3.0;
  }
  else if(x>=0.2*_gs && x<=0.4*_gs)
  {
    v[7]=5.0/_gs*x-1.0;
    v[6]=-5.0/_gs*x+2.0;
  }
  else if(x>=0.0*_gs && x<=0.2*_gs)
  {
    v[6]=5.0/_gs*x-0.0;
    v[5]=-5.0/_gs*x+1.0;
  }
  else if(x>=-0.2*_gs && x<=0.0*_gs)
  {
    v[5]=5.0/_gs*x+1.0;
    v[4]=-5.0/_gs*x-0.0;
  }
  else if(x>=-0.4*_gs && x<=-0.2*_gs)
  {
    v[4]=5.0/_gs*x+2.0;
    v[3]=-5.0/_gs*x-1.0;
  }
  else if(x>=-0.6*_gs && x<=-0.4*_gs)
  {
    v[3]=5.0/_gs*x+3.0;
    v[2]=-5.0/_gs*x-2.0;
  }
  else if(x>=-0.8*_gs && x<=-0.6*_gs)
  {
    v[2]=5.0/_gs*x+4.0;
    v[1]=-5.0/_gs*x-3.0;
  }
  else if(x>=-1.0*_gs && x<=-0.8*_gs)
  {
    v[1]=5.0/_gs*x+5.0;
    v[0]=-5.0/_gs*x-4.0;
  }
  else if(x<=-1.0*_gs)
    v[0]=1.0;

  //sliding mode control defuzzy
//  float rule[11]={-10.0,-8,-6,-4,-2,0.0,2,4,6,8,10};
  float rule[11]={-1.0,-0.8,-0.6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0};
//  float rule[11]={-1.0,-0.6,-0.56,-0.53,-0.5,0.0,0.5,0.53,0.56,0.6,1.0};
//  float rule[11]={-0.5,-0.4,-0.3,-0.2,-0.1,0.0,0.05,0.1,0.2,0.35,0.5};
  float u;
//  float weight=0.0,weight_sum=0.0;
  weight=0.0,weight_sum=0.0;

  for(i=0;i<11;i++){
    if(v[i]>0.0){
      weight+=v[i];
      weight_sum+=(v[i])*rule[i];
    }
  }
  u=(weight_sum/weight)*_gu;
  return u;
}

float FSMC2(float _gs,float _gu,float x,float *v) //x:silding value, *v:fsmcvector
{
  //fuzzify
  int i;
  for( i=0; i<11; i++ )
    v[i]=0;
    
  if(x>=1.0*_gs)
    v[10]=1.0;
  else if(x>=0.8*_gs && x<=1.0*_gs)
  {
    v[10]=5.0/_gs*x-4.0;
    v[9]=-5.0/_gs*x+5.0;
  }
  else if(x>=0.6*_gs && x<=0.8*_gs)
  {
    v[9]=5.0/_gs*x-3.0;
    v[8]=-5.0/_gs*x+4.0;
  }
  else if(x>=0.4*_gs && x<=0.6*_gs)
  {
    v[8]=5.0/_gs*x-2.0;
    v[7]=-5.0/_gs*x+3.0;
  }
  else if(x>=0.2*_gs && x<=0.4*_gs)
  {
    v[7]=5.0/_gs*x-1.0;
    v[6]=-5.0/_gs*x+2.0;
  }
  else if(x>=0.0*_gs && x<=0.2*_gs)
  {
    v[6]=5.0/_gs*x-0.0;
    v[5]=-5.0/_gs*x+1.0;
  }
  else if(x>=-0.2*_gs && x<=0.0*_gs)
  {
    v[5]=5.0/_gs*x+1.0;
    v[4]=-5.0/_gs*x-0.0;
  }
  else if(x>=-0.4*_gs && x<=-0.2*_gs)
  {
    v[4]=5.0/_gs*x+2.0;
    v[3]=-5.0/_gs*x-1.0;
  }
  else if(x>=-0.6*_gs && x<=-0.4*_gs)
  {
    v[3]=5.0/_gs*x+3.0;
    v[2]=-5.0/_gs*x-2.0;
  }
  else if(x>=-0.8*_gs && x<=-0.6*_gs)
  {
    v[2]=5.0/_gs*x+4.0;
    v[1]=-5.0/_gs*x-3.0;
  }
  else if(x>=-1.0*_gs && x<=-0.8*_gs)
  {
    v[1]=5.0/_gs*x+5.0;
    v[0]=-5.0/_gs*x-4.0;
  }
  else if(x<=-1.0*_gs)
    v[0]=1.0;

  //sliding mode control defuzzy
  //asymmetry table
  //float rule[11]={-1.0,-0.8,-0.6,-0.4,-0.2,0.0,0.05,0.1,0.2,0.4,0.6};
  float rule[11]={-0.6, -0.4, -0.2, -0.1, -0.05, 0.0, 0.2, 0.4, 0.2, 0.4, 0.6};
  int u;
  float weight=0.0,weight_sum=0.0;

  for(i=0;i<11;i++){
    if(v[i]>0.0){
      weight+=v[i];
      weight_sum+=(v[i])*rule[i];
    }
  }
  u=(weight_sum/weight)*_gu;

  return u;
}

static void PID(PtrMotor pmotor)
{
  pmotor->integral = pmotor->integral + pmotor->ki*pmotor->error - pmotor->integral_anti;
  if (pmotor->integral > pmotor->integral_max)
  {
    float e =  pmotor->integral - pmotor->integral_max;
    pmotor->integral_anti = e * pmotor->kb;
  }
  else if (pmotor->integral < -pmotor->integral_max)
  {
    float e =  pmotor->integral + pmotor->integral_max;
    pmotor->integral_anti = e * pmotor->kb;
  }
  else
  {
    pmotor->integral_anti = 0.0;
  }
  float p = pmotor->kp * pmotor->error;
  float d = pmotor->kd * pmotor->error_diff; 
  p_tmp = round(p);
  i_tmp = round(pmotor->integral);  
  d_tmp = round(d);      
  i_anti_tmp = round(pmotor->integral_anti);
  float u = p + pmotor->integral + d;
  pmotor->controlVal = round(u); 
         
  if (pmotor->controlVal >= 0)
  { 
    pmotor->dir = pmotor->dir_forward;
  }
  else  //(pmotor->controlVal < 0)  
  { 
    pmotor->dir = pmotor->dir_backward;
    //pmotor->controlVal = -pmotor->controlVal;
  }
  if (pmotor->controlVal > pmotor->controlMax)
  { 
    pmotor->controlVal = pmotor->controlMax; 
  }         
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, abs(pmotor->controlVal));  
}

static void PD(PtrMotor pmotor)
{
  float p = pmotor->kp * pmotor->error;
  float d = pmotor->kd * pmotor->error_diff; 
  p_tmp = round(p);
  i_tmp = 0;  
  d_tmp = round(d);      
  i_anti_tmp = 0;
  float u = p + d;
  pmotor->controlVal = round(u); 
         
  if (pmotor->controlVal >= 0)
  { 
    pmotor->dir = pmotor->dir_forward;
  }
  else  //(pmotor->controlVal < 0)  
  { 
    pmotor->dir = pmotor->dir_backward;
    //pmotor->controlVal = -pmotor->controlVal;
  }
  if (pmotor->controlVal > pmotor->controlMax)
  { 
    pmotor->controlVal = pmotor->controlMax; 
  }         
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, abs(pmotor->controlVal));  
}

static void PI(PtrMotor pmotor)  //把FSMC當PD 最後額外再加I補, 非正常PI
{
  pmotor->integral = pmotor->integral + pmotor->ki*pmotor->error - pmotor->integral_anti;
  if (pmotor->integral > pmotor->integral_max)
  {
    float e =  pmotor->integral - pmotor->integral_max;
    pmotor->integral_anti = e * pmotor->kb;
  }
  else if (pmotor->integral < -pmotor->integral_max)
  {
    float e =  pmotor->integral + pmotor->integral_max;
    pmotor->integral_anti = e * pmotor->kb;
  }
  else
  {
    pmotor->integral_anti = 0.0;
  }
  float p = pmotor->kp * pmotor->error;
  float d = pmotor->kd * pmotor->error_diff; 
  p_tmp = round(p);
  i_tmp = round(pmotor->integral);  
  d_tmp = round(d);      
  i_anti_tmp = round(pmotor->integral_anti);
  float u = p + d;
  float RoughRel = pmotor->deg2count; 
  float sliding_value = u;    
  float gs = RoughRel; //gs_tmp
  float ThinSlid = RoughRel*0.05;
  float gu = pmotor->gu_rel_scale * pmotor->controlMax;  //每次變化量為controlMax 的 XX %

  if (abs(sliding_value) > RoughRel )
  {
    gs = RoughRel;
//    gu = 4095*0.7;  
  }
  else 
  {
    if (abs(sliding_value) < ThinSlid)
    {   
      sliding_value = 0.0;
      gu = 0.0;
      gs = 1.0;
    }   
    else
    {
      gs = abs(sliding_value)+ThinSlid;
//      gu = 4095*0.2 + abs(sliding_value) / gs;
      gu = exp(abs(gs)/RoughRel-1)*gs;
    }      
  }  
  
  if (pmotor->FMSC_rule_type == FSMC_ruleSym)
  { 
    u = FSMC1( gs, gu, sliding_value, fsmc_vector);   
  }
  else
  { 
    u = FSMC2( gs, gu, sliding_value, fsmc_vector); 
  }
  u = u + pmotor->integral;
  pmotor->controlVal = round(u);  
         
  if (pmotor->controlVal >= 0)
  { 
    pmotor->dir = pmotor->dir_forward;
  }
  else  //(pmotor->controlVal < 0)  
  { 
    pmotor->dir = pmotor->dir_backward;
    //pmotor->controlVal = -pmotor->controlVal;
  }
  if (pmotor->controlVal > pmotor->controlMax)
  { 
    pmotor->controlVal = pmotor->controlMax; 
  }         
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->dir_base, pmotor->dir);
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, abs(pmotor->controlVal));      
}

static void Init_Controller_Variable(PtrMotor pmotor)
{  
  //FSMC
  pmotor->controlVal = 0;
  pmotor->displacement = 0;  
  pmotor->count_old = 0;
  pmotor->diff_old = 0;    
  pmotor->error_old = 0;
  pmotor->error_diff_old = 0; 
  printf("%d %d %d %d %d \n", 
  pmotor->displacement, pmotor->count_old, pmotor->diff_old, pmotor->error_old, pmotor->error_diff_old);
  //PID
  pmotor->integral = 0.0;
  pmotor->integral_anti = 0.0;  
  printf("%.3f %.3f\n", pmotor->integral, pmotor->integral_anti);      
  usleep(100000); //sleep 0.1 sec  
}

static void Init_PWM(void)
{
  int arm_No;
  for (arm_No = 0; arm_No < 2; arm_No++)
  {
    int joint_No;
    for (joint_No = 1; joint_No < N + 2; joint_No++)
    {
      PtrMotor pmotor = motor_list[arm_No][joint_No];
      IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, 0);
    }
  }
  printf("\nReset pwm end\n");
}

// Limit switch's initial
static void Init_LS()
{
    // Recast the edge_capture pointer to match the alt_irq_register() function
    //  * prototype.
    void* edge_capture_ptr = (void*) &edge_capture;
  
    IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ROBOT_LS_BASE, 0x0);
    IOWR_ALTERA_AVALON_PIO_IRQ_MASK(ROBOT_LS_BASE, 0x0); //disable LS interrupt
    // Enable all 4 button interrupts.
    //IOWR_ALTERA_AVALON_PIO_IRQ_MASK(ROBOT_LS_BASE, 0x3ff);
    IOWR_ALTERA_AVALON_PIO_IRQ_MASK(ROBOT_LS_BASE, 0x3fff);
    // Reset the edge capture register.
    IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ROBOT_LS_BASE, 0x0);
    // Register the interrupt handler.
    alt_irq_register( ROBOT_LS_IRQ, edge_capture_ptr, LS_Interrupts );
}

// Limit switch's interrupt
static void LS_Interrupts(void* context, alt_u32 id)
{
  volatile int* edge_capture_ptr = (volatile int*) context;
  /* Store the value in the Button's edge capture register in *context. */
  *edge_capture_ptr = IORD_ALTERA_AVALON_PIO_EDGE_CAP(ROBOT_LS_BASE);
  /* Reset the Button's edge capture register. */
  IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ROBOT_LS_BASE, 0);
} 

//RS232 uart
static void UART_Init()
{  //中斷初始化函數
    void* status_ptr = 0;
    IOWR_ALTERA_AVALON_UART_CONTROL(UART_BASE, 0x80);
    IOWR_ALTERA_AVALON_UART_STATUS(UART_BASE, 0x0);
    IOWR_ALTERA_AVALON_UART_RXDATA(UART_BASE, 0x0);
    alt_irq_register(UART_IRQ,status_ptr,UART_ISR);
    //printf("a");
}

static void UART_Variable_Init()
{
  memset(uart_recevie_list, '0', sizeof(uart_recevie_list[0][0]) * 12 * 8);
  memset(refVal_list, 0.0, sizeof(refVal_list[0][0]) * 2 * 6);    
  //變數排列順序有稍微照著程式執行順序
  //Uart receive IRQ
  txdata = 0; //分別用來保存需要寫入到uart IP核的一些數據, 發送數據, alt_u8 = unsigned char 
  rxdata = 0; //以及保存需要從uart IP核裡面讀出的一個數據, 接收數據, 給全域可以方便debug
  flag_size = 0;
  data_size = 0;
  scan_idx = 0;
  URMT = 1; //Uart Receive M Temp
  flag_uart_receive_finish = 0;
  //Updata reference value from UART
  plus_or_negative = 0;
  float_index = 0;
  flag_float_index = 0;  
  double_final_value = 0.0;
  pendulum_degree = 0.0;
  flag_uart_upd_ref_finish = 0;  
}

static void UART_ISR(void* context, alt_u32 id)//UART中斷服務函數
{   
  volatile char* status_ptr =(volatile char*)context;
  *status_ptr =IORD_ALTERA_AVALON_UART_STATUS(UART_BASE);  
    
  if((IORD_ALTERA_AVALON_UART_STATUS(UART_BASE)&(0x80)) ==0x80)
  {
    rxdata = IORD_ALTERA_AVALON_UART_RXDATA(UART_BASE);  //將radata暫存器中儲存的值讀入變數rxdata中,
                                                         //UART名字為根據你qsys uart IP核名字怎麼命名而定,
                                                         //可在system.h查詢
    //txdata = rxdata;                                   //進行串口自動收發, 將變數rxdata中的值賦值給變數txdata    
      
    //while( !(IORD_ALTERA_AVALON_UART_STATUS(UART_BASE) & //查詢發送是否準備好了的訊號, 如果沒有準備好, 則等待
    //        ALTERA_AVALON_UART_STATUS_TRDY_MSK) );
    if (rxdata != receive_finish)
    {
      if (flag_size)
      {          
        if (scan_idx <= data_size + 1)  //加完正負標誌及小數點位置後再加上一開始的位數長度
        {
          uart_recevie_list[URMT][scan_idx - 1] = rxdata;  //從第1列開始存
          scan_idx += 1;
          if (scan_idx == data_size + 1)
          {
            URMT += 1 ;        
            flag_size = 0;
            scan_idx = 0;
          }          
        }   
      }
      else
      {
        data_size = ((int)rxdata - 48) + 2;  //加上正負標誌及小數點位置
        flag_size = 1;  //確定該筆資料為幾位數(含小數點)
        scan_idx += 1;  //現在掃描的索引數, uart 傳 9個字元該值就會是9
      }  
    }  
    else
    {
      flag_uart_receive_finish = 1;
      UartUpdRefVal();      
      led_light = led_light ^ 1 ;
      IOWR_ALTERA_AVALON_PIO_DATA(LED_BASE, led_light);         
    }     
    //IOWR_ALTERA_AVALON_UART_TXDATA(UART_BASE, txdata);   //發送準備好了, 發送txdata
//    IOWR_ALTERA_AVALON_UART_STATUS(UART_BASE, 0x0);
  }    
}

////////////記憶體存放註解區-配置一////////////
/*
static void UartReceiveTX2(void)  //EXP 1-2
{
  int arm_No = 0;
  int j_No = 0;  //joint_No, 因儲存空間有限, 需斤斤計較, 故不能以較為直觀的1為開頭,
  int n = 0; 
  num = 0;  
  memset(count_arr, 0, sizeof(count_arr[0][0][0]) * 2 * 4 * URDT);
  memset(error_arr, 0, sizeof(error_arr[0][0][0]) * 2 * 4 * URDT);    
  memset(controlVal_arr, 0, sizeof(controlVal_arr[0][0][0]) * 2 * 4 * URDT);
  memset(Tref_arr, 0.0, sizeof(Tref_arr[0][0][0]) * 2 * 4 * URDT);
  
//  for (n = 0; n < URDT; n++)
//  {
//    printf("%.2f \n", Tref_arr[0][0][n]);
//  }
  
  time = 0.0;
  cnt_isr_Xms = 1;
  flag_print = 0;
  printf("Hello Uart Test Ver2!\n");
  UART_Variable_Init();
  usleep(1000000);
//  UART_Upd_Init((void*)TIMER_100MS_UPD_UART_BASE, TIMER_100MS_UPD_UART_IRQ, TIMER_100MS_UPD_UART_FREQ);
  DualArmInit_FSMC((void*)TIMER_20MS_BASE, TIMER_20MS_IRQ, TIMER_20MS_FREQ, Move_Mode_Normal);
//  PrintfTest_Init((void*)TIMER_20MS_BASE, TIMER_20MS_IRQ, TIMER_20MS_FREQ);
  UART_Init();
  
  while(cnt_isr_Xms <= URDT)  //1000 * 0.02 = 20, 20s / 0.1s  = 200 個資料, 正常會小於且運動時間不要超過15s, 因為目前設定timer會先跑不會等對方設備開始傳開始計時, 所以可能已經跑了1s人員才操作好執行傳送程式
  {    
//    UartUpdRefVal();       
  }   
  IOWR_ALTERA_AVALON_UART_STATUS(UART_BASE, 0x0);
  IOWR_ALTERA_AVALON_UART_CONTROL(UART_BASE, 0x00);
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_100MS_UPD_UART_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);  
  usleep(1000000);
  
  printf("num %d n %d\n", num, n);
  
  for (n = 0; n < num; n++)
  {
    printf("%.2f ", time_arr[n]);
    for (arm_No = 0; arm_No < 2; arm_No++)
    {
      for(j_No = 0; j_No < N - 1; j_No++)  //N = 5
      {
        printf("%d %d %d %.2f "               
        , count_arr[arm_No][j_No][n], error_arr[arm_No][j_No][n]
        , controlVal_arr[arm_No][j_No][n], Tref_arr[arm_No][j_No][n]);          
      }
    }  
    printf("\n");
  }
  printf("Uart Test Ver2 end\n");  
}
*/
////////////記憶體存放註解區-配置一////////////


////////////記憶體存放註解區-配置二////////////

static void UartReceiveTX2(void)  //EXP 1-2
{  
  int n = 0; 
  num = 0;
  for ( n = 0; n < URDT; n++)
  {
//    time_arr[n] = 0.0;
    count_arr[n] = 0;
    error_arr[n] = 0;
    controlVal_arr[n] = 0;
  }
  LR_tmp = 0; No_tmp = 1;
  time = 0.0; cnt_isr_Xms = 1; flag_print = 0;
  printf("Hello Uart Receive TX2!\n");
  UART_Variable_Init();
  usleep(1000000);
  UART_Init();
  SingleJointInit_UART((void*)TIMER_2MS_BASE, TIMER_2MS_IRQ, TIMER_2MS_FREQ);
//  SingleJointInit_UART((void*)TIMER_20MS_BASE, TIMER_20MS_IRQ, TIMER_20MS_FREQ);
//  SingleJointInit_UART((void*)TIMER_100MS_UPD_UART_BASE, TIMER_100MS_UPD_UART_IRQ, TIMER_100MS_UPD_UART_FREQ);  

  //最多可存12000 * 0.02 = 240s,
  //最多可存20000 * 0.002 = 40s, 
  //而正常實驗會小於且運動時間應該不會超過60s,
  //目前設定timer會先跑不會等對方設備開始傳開始計時, 也就是先開接收端才開傳送端, 所以可能已經跑了Xs人員才操作好執行傳送程式  
  while(cnt_isr_Xms*0.002 <= 40)  
  {     
  }   
  IOWR_ALTERA_AVALON_UART_STATUS(UART_BASE, 0x0);
  IOWR_ALTERA_AVALON_UART_CONTROL(UART_BASE, 0x00);
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_2MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_100MS_UPD_UART_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);  
  usleep(1000000);
  
  printf("num %d n %d\n", num, n);
  
  for (n = 0; n < num; n++)
  {
//    printf("%.2f %d %d %d \n", time_arr[n], count_arr[n], error_arr[n], controlVal_arr[n]);
    printf("%d %d %d \n", count_arr[n], error_arr[n], controlVal_arr[n]); 
  }
  printf("Uart Test Ver2 end\n");  
}

////////////記憶體存放註解區-配置二////////////

static void UartUpdRefVal(void)  //uart updata ref value
{
  if (!flag_uart_receive_finish)
  {
    ;
  }
  else
  {
    //處理 uart_recevie_list 轉換為 refVal_list (char -> double)
    int i, j;
    for (i = 1; i < URM; i++)
    {      
      for (j = 0; j < URN; j++)  //直接全掃 
      {
        alt_u8 rxdata = uart_recevie_list[i][j];
        if ( rxdata == '+' )        
          plus_or_negative = 1;       
        else if ( rxdata == '-' )        
          plus_or_negative = -1;        
        else
        {
          if ( flag_float_index)
          { 
            if ( plus_or_negative == 1)
            {
              if ( scan_idx < float_index)
              {
                double_data[scan_idx] = (float)(((int)rxdata - 48) * pow(10, float_index-scan_idx-1));
                double_final_value += double_data[scan_idx]; 
              }
              else if ( scan_idx > float_index)
              {
                double_data[scan_idx] = (float)(((int)rxdata - 48) * pow(10, float_index-scan_idx));
                double_final_value += double_data[scan_idx];
              }
              else
              {
                ;  //此時是傳小數點     
              }
              scan_idx += 1;
            }
            else if ( plus_or_negative == -1)
            {
              if ( scan_idx < float_index)
              {
                double_data[scan_idx] = (float)((int)rxdata - 48) * pow(10, float_index-scan_idx-1);
                double_data[scan_idx] = -double_data[scan_idx];
                double_final_value += double_data[scan_idx];
              }
              else if ( scan_idx > float_index)
              {
                double_data[scan_idx] = (float)((int)rxdata - 48) * pow(10, float_index-scan_idx);
                double_data[scan_idx] = -double_data[scan_idx];
                double_final_value += double_data[scan_idx];
              }
              else
              {
                ;  //此時是傳小數點 
              }
              scan_idx += 1;
            }
          }
          else
          {
            float_index = (int)rxdata - 48;
            flag_float_index = 1;
          }
        }
      }  //第一列資料處理完成           
      if (i < 6)      
      {
        refVal_list[0][i] = double_final_value;  // left joint1 ~ joint5 refVal, 全部目標角度一起刷新, 之後動單隻手臂的程式取他要的就好, 因為考量到到時候有兩隻手同動的中斷, 所以乾脆傳值就都一起傳過來        
        
        //motor_list[0][i]->refVal = double_final_value * motor_list[0][i]->deg2count;
      }
      else if ((i >= 6) && (i <= 10))      
      {
        refVal_list[1][i - 5] = double_final_value;  // right joint1 ~ joint5 refVal
        //motor_list[1][i-5]->refVal = double_final_value * motor_list[1][i-5]->deg2count;
      }
      else  // i == 11
      {
        pendulum_degree = double_final_value;
      }
      flag_float_index = 0;
      scan_idx = 0;
      double_final_value = 0.0;
    }          
    URMT = 1;  //處理完資料後重置剛剛接收資料時的換行temp, 因為沒辦法在UART IRQ內重置所以改在Updata ref的時候
    //memset(uart_recevie_list, 0, sizeof(uart_recevie_list[0][0]) * 12 * 8);  //不重置也沒差
    flag_uart_receive_finish = 0;  //重置接收完成的訊號, 因為沒辦法在UART IRQ內重置所以改在Updata ref的時候
    flag_uart_upd_ref_finish = 1;//flag uart updata reference finish
  } 
}

static void PrintfTest(void)
{  
  time = 0.0;
  cnt_isr_Xms = 1;
  PrintfTest_Init((void*)TIMER_20MS_BASE, TIMER_20MS_IRQ, TIMER_20MS_FREQ);  
  while(cnt_isr_Xms <= 500)  //500*0.02 = 10 秒
  {
    if (flag_print == 1)
    {
//      printf("%.2f ", time);
//      //L
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); 
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//      //R
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//      printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);      
//      printf("\n");
      
      printf("%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f \n"
      ,time
      , time, time, time, time, time, time, time
      , time, time, time, time, time, time, time
      , time, time, time, time, time, time, time
      , time, time, time, time, time, time, time
      , time, time, time, time, time, time, time
      , time, time, time, time, time, time, time
      , time, time, time, time, time, time, time
      , time, time, time, time, time, time, time);
      /***      
            上面會有這種情形
      1 1 1 1 1 1 2
            下面不會
      1 1 1 1 1 1 1
            似乎下面打法可以保證資料是同一時間戳記的!? 
            不像上面打法, 會有出現此時看得的值可能是已經被更新過的
      *///
      flag_print = 0;
    }
  }
  IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_20MS_BASE, ALTERA_AVALON_TIMER_CONTROL_STOP_MSK);    
}

static void PrintfTest_Init(void* base, alt_u32 irq, alt_u32 freq)
{
  /* set the system clock frequency */
  alt_sysclk_init (freq);

  /* set to free running mode */
  IOWR_ALTERA_AVALON_TIMER_CONTROL (base,
            ALTERA_AVALON_TIMER_CONTROL_ITO_MSK  |
            ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
            ALTERA_AVALON_TIMER_CONTROL_START_MSK);

  alt_irq_register (irq, base, PrintfTest_ISR);
}

static void PrintfTest_ISR(void* base, alt_u32 id)
{
  time = (double)(sample_time * cnt_isr_Xms); //time
  int arm_No = 0;
  int j_No = 0;
  for (arm_No = 0; arm_No < 2; arm_No++)
  {
    for(j_No = 1; j_No < N; j_No++)  //N = 5
    {
//      Tref[arm_No][j_No-1][num] = refVal_list[arm_No][j_No];
    }
  }    
  num += 1;
  flag_print += 1;   
  cnt_isr_Xms += 1;
  IOWR_ALTERA_AVALON_TIMER_STATUS(base, 0);   
  alt_tick();
}

static void CodeCalTime(void)
{      
  //#define alt_timestamp_type alt_u64  from altera_avalon_timer.h
  alt_u64 tt0, tt1;
  alt_timestamp_start(); // 開啟時間戳服務
  tt0 = alt_timestamp(); // 測量時間戳t0
//////us *10^6 = 1s  , 約48xxxxxx, 0.96...s
    //usleep(1000000);  
    
/*****part of prinf*****/
//////約206XXX ticks, 0.00412s
//    printf("%.2f ", time);
//    //L
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); 
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//    //R
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);
//    printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time); printf("%.2f ", time);      
//    printf("\n");
//////約107XXX ticks, 0.00214592s
//    printf("%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f \n"
//    ,time
//    , time, time, time, time, time, time, time
//    , time, time, time, time, time, time, time
//    , time, time, time, time, time, time, time
//    , time, time, time, time, time, time, time
//    , time, time, time, time, time, time, time
//    , time, time, time, time, time, time, time
//    , time, time, time, time, time, time, time
//    , time, time, time, time, time, time, time);  

/******************************************************  未儲存陣列前的測試
*   SingleJoint_FSMC_ISR, 約23xxx ticks, 0.00046s
*   SingleJoint_Tcurve_FSMC_ISR, 約25xxx ticks, 0.0005s
*   SingleJoint_Scurve_FSMC_ISR, 約28xxx ticks, 0.00056s
*
*   Arm_ISR, 約102xxx ticks, 0.00204s
*   Arm_Tcurve_ISR, 約116xxx ticks, 0.00232s
*   Arm_Scurve_ISR, 約126xxx ticks, 0.00252s
* 
*   SingleJoint_PID_ISR, 6000 ticks, 0.00012s
*   SingleJoint_Tcurve_PID_ISR, 約8000 ticks, 0.00016s
*   SingleJoint_Scurve_PID_ISR, 約10200 ticks, 0.000204s
* 
*   UartUpdRefVal, 約200 ticks, 0.000004s, flag_uart_receive_finish = 0
*   UartUpdRefVal, 約6400 ticks, 0.000128s, flag_uart_receive_finish = 1
******************************************************/        


  tt1 = alt_timestamp(); // 測量時間戳1    
  printf("tt1 - tt0: %lld\n", (tt1 - tt0));
}

 
/* 暫時不加，看不太懂在幹嘛  貌似是雙臂程式特有的
// check overflow and borrow
static void ChkOverANDBor( int newCounterChk, int oldCounterChk, int *diffChk )
{
    if( (newCounterChk < encoder1Of5) && (oldCounterChk > encoder4Of5) )
        *diffChk = (newCounterChk+encoderFull-oldCounterChk);
    else if( (oldCounterChk < encoder1Of5) && (newCounterChk > encoder4Of5) )
        *diffChk = (newCounterChk-(oldCounterChk+encoderFull));
    else
        *diffChk = newCounterChk-oldCounterChk;
}
*/

/* 保留寫法，若到時候通式某些軸很差再分別做特規處理，不同數量的if敘述及粗調細調值等等
if (pmotor->LR == 'L'){
    if (pmotor->No == 1) turnParamL1();
    else if (pmotor->No == 2) turnParamL2();
    else if (pmotor->No == 3) turnParamL3();
    else if (pmotor->No == 4) turnParamL4();
    else if (pmotor->No == 5) turnParamL5();
    else if (pmotor->No == 6) turnParamL6();
  }
  else if (pmotor->LR == 'R'){
    if (pmotor->No == 1) turnParamR1();
    else if (pmotor->No == 2) turnParamR2();
    else if (pmotor->No == 3) turnParamR3();
    else if (pmotor->No == 4) turnParamR4();
    else if (pmotor->No == 5) turnParamR5();
    else if (pmotor->No == 6) turnParamR6();
  }
}  
*/

