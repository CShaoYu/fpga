#ifndef ROBOT_ARM_H_
#define ROBOT_ARM_H_

#endif /*ROBOT_ARM_H_*/
#include <nios2.h>
#include <system.h>
#include <stdio.h>
#include <alt_types.h>
#include <unistd.h>
#include <altera_avalon_pio_regs.h>  //沒有include的話會出現warning: implicit declaration of function  IOWR_ALTERA_AVALON_PIO... 
#include <altera_avalon_uart_regs.h> //沒有include的話會出現warning: implicit declaration of function  IORD_ALTERA_AVALON_UART...
#include <sys/alt_irq.h> //沒有include的話會出現warning: implicit declaration of function  alt_irq_register...
#include <altera_avalon_timer_regs.h>
#include <sys/alt_alarm.h> //...alt_sysclk_init...
#include <math.h> //...pow...
#include <stdlib.h> //...abs...
#include <sys/alt_timestamp.h> //...alt_timestamp... & alt_timestamp_start...
//#include <sys/alt_stdio.h> //..alt_printf ..

#define HAL_PLATFORM_RESET() NIOS2_WRITE_STATUS(0); NIOS2_WRITE_IENABLE(0); ((void (*) (void)) NIOS2_RESET_ADDR) ()
  
//副程式命名規則:完整單詞我不會隔開 若是簡稱會以底線隔開, 變數命名規則:大部分都會分開 除了少數看起來順眼的
//副程式引數我都會以_開頭
//Single開頭表單顆馬達
//Arm開頭表單隻手臂(不含夾爪)
//All開頭表兩手(不含夾爪)
#define deg2count1L 22.22
#define deg2count1 22459.73  //* 2
#define deg2count2 40.13   //40.13, 2628.27
#define deg2count3 2628.27  //3925.33
#define deg2count4 2628.27
#define deg2count5 12.33
#define deg2count6 12.33

#define N 5  //joint NO 
#define L 0  //arm No 手臂編號
#define R 1
#define FSMC_ruleASym 0  //非對稱, FSMC2
#define FSMC_ruleSym 1 //對稱, FSMC1

#define URM 12  //Uart Receive M(列), 從索引1開始, 接收的ref字元組, 1表左1, 2表左2....6表右1
#define URN 8  //Uart Receive N(行), 從索引0開始
#define RVM 2  //Ref Value M(列), 從索引0開始
#define RVN 6  //Ref Value N(行), 從索引0開始
/* when uart running, the value is max stored value of data
 * 配置一 1500, 0.02*1500=30s, 只可儲存uart運行時30秒內的所有資料
 * 配置二 12000, 0.02*12000=240s, 只可儲存uart運行時240秒內的所有資料, 或20000, 0.002*20000=40s, 只可儲存uart運行時40秒內的所有資料
 */
#define URDT 20000 

#define ESC 27
#define EOT 0x4
//  One nice define for going to menu entry functions. 
#define MenuCase(letter,proc) case letter:proc(); break;

struct Motor{      
  char LR;
  int No;
  int limit_switch_reg;
  int fullwidth; 
  int dir_forward; //dir 每顆馬達不同會和M+ M- encoderA、B 馬達驅動器的連接順序有關，所以不一樣很正常 而且旋轉同方向在雙臂上也會互為反向
  int dir_backward; //會因應到時候也會有移動fun 所以也要有正轉和反轉  所以拆為兩個變數
  int dir;  //dir_tmp, 預設與dir_forward同
  //int countCalib;  //因為接線沒有設計所以int count = IORD_ALTERA_AVALON_PIO_DATA(pmotor->count_base); 可能會需要加上負號反向過來
  int lowerWidth;  //每顆馬達的最小驅動量, 在此設定為各馬達所能驅使所銜接連桿0-90度的最小電壓
  int controlMax;  
  float limitLower;  //deg 
  float limitUpper;  //deg
  
  int refVal;  //count  
  signed int count_old;  
  signed int count;  //unsigned -> signed 有正有負
  int diff_old;
  int diff;  //每次取樣時間的count差值, 除上取樣時間就是速度
  int diff_d;  //每次取樣時間的diff差值, 除上取樣時間就是加速度
  int displacement;  //總位移, count
  int error_diff_old;
  int error_old;
  int error;
  int error_diff; //誤差微分  
  int error_diff_d;
  int controlVal;
    
  float lamda;   //sliding surface 
  float lamda_d; //我記得二階滑動平面沒有這個才對 只是以前的都有設此參數(從巫神開始)
  float lamda_dd;
  float gs[3];  //fsmc param for 增益調變絕對型FSMC的gs
  float gu_abs[3];  ////fsmc param for 增益調變絕對型FSMC的gu
  float gu_rel;  //控制量百分比變化的參考對象, 詳細看RelFMSC def
  float gu_rel_scale;  //相對型FSMC每次最多的控制量變化(delta u)
  float gs_scale;
  
  float kp;
  float ki;
  float kd;
  float kb;
  float integral;  
  float integral_anti;
  float integral_max;
  
//Tcurve、Scurve(3階), 本專案策略都使用最短時間(給定X1, Vmax, Amax, s_factor)去規劃, 沒有設計給定X1, Time, Ta, Ts的Case(matlab有)   
  float Tref;  //tmp ref, 速度規劃曲線所追的位置軌跡, Normal的reference
  int Tcnt;  //tmp cnt, 儲存每次新的速度規劃前的當時位置
  float Vmax;  //deg/sec
  float Amax;  //
  float Time;  //各軸各自規畫所需的時間, 在進行比較誰比花的時間比較久
  float Aave;  //only for Scurve, 整體的平均加速度
  float s_factor;  //only for Scurve, s系數0~1之間, 0會降階為Tcurve, 1為Scurve  
  int Tflag;  //tmp flage, 根據位移正或負改變斜率, 
  float Ta;  //等速度段開始的時間
  float Ts;  //等加速度段開始的時間
  float v;  //判斷距離是否過短, 距離是否是最遠的軸後, 所決定的該軸最終的最大速度, 不一定是Vmax, 
  float a;  //判斷距離是否過短, 距離是否是最遠的軸後, 所決定的該軸最終的最大加速度
  float j;  //急衝度(加速度再微分)
  float t0[8];  //only for Scurve, 8個時間點7個時段
  float a0[8]; //only for Scurve, 各時間點的加速度
  float v0[8]; //only for Scurve, 各時間點的速度
  float p0[8]; //only for Scurve, 各時間點的位置
  
  int FSMC_type;
  int FMSC_rule_type;
  int PID_type;
  int Ctrl_type;
        
  float deg2count;  // = RoughRel = gs = RoughSlid
  alt_u32 dir_base;     
  alt_u32 pwm_base;
  alt_u32 count_base;
  alt_u32 rst_count_base;        
};
typedef volatile struct Motor Motor, *PtrMotor;

static void MenuBegin( alt_8 *title );
static void MenuItem( alt_8 letter, alt_8 *name );
static int MenuEnd( alt_u8 lowLetter, alt_u8 highLetter );
static alt_u8 TopMenu(void);
static void SingleControlMenu(void);
static void SingleJointMenu(void);
static void ShowAllCounters(void);

static void SingleInitJoint(void);  //改無引數和MenuCase機制有關----
static void SingleForwardJoint(void);  
static void SingleBackwardJoint(void);
static void SingleStopJoint(void);  
static void SingleResetCounter(void);
static void SingleStepControl(void);  
static void StopAllJoint(void); 
static void SingleSetParameter_FSMC(void);  
static void SingleSetParameter_PID(void);  //----改無引數和MenuCase機制有關
static void SingleMoveJoint(float _degree, PtrMotor pmotor, int _move_mode, int _control_mode);
static void SingleJointInit(void* base, alt_u32 irq, alt_u32 freq, int _move_mode, int _control_mode);
static void SingleJointInit_UART(void* base, alt_u32 irq, alt_u32 freq);  //move_mode固定Normal, control_mode固定FSMC
static void SingleJointInit_Tcurve(void);
static void SingleJointInit_Scurve(void);
static void SingleJoint_FSMC_ISR(void* base, alt_u32 id);
static void SingleJoint_FSMC_UART_ISR(void* base, alt_u32 id);
static void SingleJoint_Tcurve_FSMC_ISR(void* base, alt_u32 id);
static void SingleJoint_Scurve_FSMC_ISR(void* base, alt_u32 id);

static void SingleJoint_PID_ISR(void* base, alt_u32 id);
static void SingleJoint_Tcurve_PID_ISR(void* base, alt_u32 id);
static void SingleJoint_Scurve_PID_ISR(void* base, alt_u32 id);


static void ArmMenu(void);
static void ArmPTP_Control(void);  //Point to point, Tcurve、Scurve, 只有Point to Point應該無分joint space、Cartesian space
static void ArmMove(float *_degree, int _move_mode);
static void ArmInit(void* base, alt_u32 irq, alt_u32 freq, int _move_mode);
static void Arm_ISR(void* base, alt_u32 id);
static void ArmInit_Tcurve(void);
static void Arm_Tcurve_ISR(void* base, alt_u32 id);
static void ArmInit_Scurve(void);  //預留, 待更新, 理論上s_factor = 0 會等於Tcurve, 但有一些限制條件還不明白故先分開來
static void Arm_Scurve_ISR(void* base, alt_u32 id);

static void ArmTrajectoryPlan_J_Control(void);  //預留, 待更新
static void ArmTrajectoryPlan_C_Control(void);  //預留, 待更新

static void DualArmInit_FSMC(void* base, alt_u32 irq, alt_u32 freq, int _move_mode);
static void DualArm_FSMC_ISR(void* base, alt_u32 id);

static int compare5(float a, float b, float c, float d, float e);
static float Tpart1(float _a, float _t);
static float Tpart2(float _a, float _v, float _Ta, float _t);
static float Tpart3(float _a, float _v, float _Ta, float _t);

static void AbsFSMC(PtrMotor pmotor);
static void RelFSMC(PtrMotor pmotor);
float FSMC1(float _gs,float _gu,float x,float *v);  //對稱型模糊規則
float FSMC2(float _gs,float _gu,float x,float *v);  //非對稱型行模糊規則

static void PID(PtrMotor pmotor);
static void PD(PtrMotor pmotor);
static void PI(PtrMotor pmotor);  //把FSMC當PD 最後額外再加I補, 非正常PI

static void Init_Controller_Variable(PtrMotor pmotor);
static void Init_PWM(void);
static void Init_LS();
static void LS_Interrupts(void* context, alt_u32 id);

static void UART_Variable_Init();
static void UART_Init();
static void UART_ISR();

static void UartReceiveTX2(void);
static void UartUpdRefVal(void);


static void PrintfTest(void);
static void PrintfTest_Init(void* base, alt_u32 irq, alt_u32 freq);
static void PrintfTest_ISR(void* base, alt_u32 id);

static void CodeCalTime(void);

enum ControlMode{
  Control_Mode_PID,
  Control_Mode_FSMC,
};

enum MoveMode{
  Move_Mode_Normal,
  Move_Mode_Tcurve,  //懶得將Tcurve和Scurve合併, 實際上將s_factor設為0就可以降階為Tcurve了
  Move_Mode_Scurve
};

enum InputMode{
  Input_Mode_Normal,
  Input_Mode_Uart
};

enum FSMCMode{
  FSMC_Mode_Rel,
  FSMC_Mode_Abs
};

enum PIDMode{
  PID_Mode_PID,
  PID_Mode_PD,
  PID_Mode_PI,
};

/*
enum Direction{
  Forward,
  Backward
};
enum Joint_Number{ //馬達數量  
  Joint_Number_One = 1,  //較直觀
  Joint_Number_All = 10  //手臂運動學不包含第6軸
};
*/




