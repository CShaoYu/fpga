clc ;
clear; 
close all;
a =load('R1motor.txt');
time = a(:,1);   %列行列行
ref = a(:,2);
err = a(:,3);
nowdeg = a(:,4);
ctrl = a(:,5);

figure
grid on;hold on;
plot(time, ref);
plot(time, nowdeg);
title('Tracking position');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('Desired', 'Real');  %real or actual
savefig('tracking pos.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking pos.png');  %沒上一步也可输出成png


figure
grid on;hold on;
zero = time(:,1);
zero(:,1) = 0;
plot(time , zero, 'R');
plot(time, err,'B');
title('Error position');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('zero', 'error'); 
savefig('error pos.fig');  
saveas(gcf,'error pos.png');

figure
grid on;hold on;
plot(time, ctrl*24/256);
title('Control voltage');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage.fig');  
saveas(gcf,'Control voltage.png');

