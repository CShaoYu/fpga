clc ; clear; close all;  
sample_time = 0.02; 
% deg2cnt = 22459.73;  %22459.73, 40.13, 2628.27, 2628.27, 12.33, 12.33
deg2cnt = 40.13;
% deg2cnt = 2628.27;
% deg2cnt = 12.33;
% bit2volt = 24/255;  % 24/255, 12/65535, 24/4095, 24/4095, 24/65535, 24/65535
bit2volt = 12/65535;  
% bit2volt =  24/4095; 
% bit2volt = 24/65535;  
a =load('rt2.txt');
time = a(:,1);   %列行列行
time_j = time(4:1:end,1);
cnt = a(:,2) / deg2cnt;
err = a(:,3) / deg2cnt;
dif = (a(:,4) / deg2cnt) / sample_time;
err_d = (a(:,5) / deg2cnt) / sample_time;
diff_d = ((a(:,6) / deg2cnt) / sample_time)/ sample_time;
err_diff_d = ((a(:,7) / deg2cnt) / sample_time)/ sample_time;
diff_j = diff(diff_d) ;
diff_j = diff_j(3:1:end,1);
err_diff_j = diff(err_diff_d);
err_diff_j = err_diff_j(3:1:end,1);
% ctrl = a(:,8);
ctrl = a(:,8) * bit2volt;
% ctrl_p = a(:,9) * bit2volt;
% ctrl_i = a(:,10) * bit2volt;
% ctrl_d = a(:,11) * bit2volt;
% ctrl_i_anti = a(:,12) * bit2volt;
ref = err + cnt;
ref_v = err_d + dif;
ref_a = err_diff_d + diff_d;
ref_j = err_diff_j + diff_j;
% fuck_u_tmp = a(:,13);
% fuck_u_tmp = a(:,13) * bit2volt;

%% tracking angle 
figure
grid on;hold on;
plot(time, ref);
plot(time, cnt);
title('Tracking Angle');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('Desired', 'Real');  %real or actual
savefig('tracking angle.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking angle.png');  %沒上一步也可输出成png

%% error angle
figure
grid on;hold on;
zero = time(:,1);
zero(:,1) = 0;
plot(time , zero, 'R');
plot(time, err,'B');
title('Error position');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('zero', 'error'); 
savefig('err angle.fig');  
saveas(gcf,'err angle.png');

%% tracking angular velocity
figure
grid on;hold on;
plot(time, ref_v);
plot(time, dif);
title('Tracking Angular Velocity');
xlabel('Time(sec)');ylabel('Angular Velocity(deg/s)');
legend('Desired', 'Real');  %real or actual
savefig('tracking angular velocity.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking angular velocity.png');  %沒上一步也可输出成png

%% error angular velocity
figure
grid on;hold on;
zero = time(:,1);
zero(:,1) = 0;
plot(time , zero, 'R');
plot(time, err_d,'B');
title('Error Angular rate');
xlabel('Time(sec)');ylabel('Angular rate(deg/s)');
legend('zero', 'err vel'); 
savefig('err angular velocity.fig');  
saveas(gcf,'err angular velocity.png');

% %% tracking angular acceleration
% figure
% grid on;hold on;
% plot(time, ref_a);
% plot(time, diff_d);
% title('Tracking Angular Acceleration');
% xlabel('Time(sec)');ylabel('Angular Acceleration(deg/s^2)');
% legend('Desired', 'Real');  %real or actual
% savefig('tracking angular acceleration.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
% saveas(gcf,'tracking angular acceleration.png');  %沒上一步也可输出成png
% 
% %% error angular acceleration
% figure
% grid on;hold on;
% zero = time(:,1);
% zero(:,1) = 0;
% plot(time , zero, 'R');
% plot(time, err_diff_d,'B');
% title('Error Angular Acceleration');
% xlabel('Time(sec)');ylabel('Angular Acceleration(deg/s^2)');
% legend('zero', 'err acc'); 
% savefig('err angular acceleration.fig');  
% saveas(gcf,'err angular acceleration.png');
% 
% %% tracking jerk
% figure
% grid on;hold on;
% plot(time_j, ref_j);
% plot(time_j, diff_j);
% title('Tracking Jerk');
% xlabel('Time(sec)');ylabel('Angular Jerk(deg/s^3)');
% legend('Desired', 'Real');  %real or actual
% savefig('tracking jerk.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
% saveas(gcf,'tracking jerk.png');  %沒上一步也可输出成png
% 
% %% error jerk
% figure
% grid on;hold on;
% zero = time_j(:,1);
% zero(:,1) = 0;
% plot(time_j , zero, 'R');
% plot(time_j, err_diff_j,'B');
% title('Error Jerk');
% xlabel('Time(sec)');ylabel('Angular Jerk(deg/s^3)');
% legend('zero', 'err jerk'); 
% savefig('err jerk.fig');  
% saveas(gcf,'err jerk.png');

%% Control value
figure
grid on;hold on;
plot(time, ctrl);
title('Control voltage');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage.fig');  
saveas(gcf,'Control voltage.png');

%% Control value P
figure
grid on;hold on;
plot(time, ctrl_p);
title('Control voltage P');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage P.fig');    
saveas(gcf,'Control voltage P.png');

%% Control value I
figure
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           grid on;hold on;
plot(time, ctrl_i);
title('Control voltage I');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage I.fig');  
saveas(gcf,'Control voltage I.png');

%% Control value D
figure
grid on;hold on;
plot(time, ctrl_d);
title('Control voltage D');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage D.fig');  
saveas(gcf,'Control voltage D.png');

%% Control value I_anti
figure
grid on;hold on;
plot(time, ctrl_i_anti);
title('Control voltage I Anti');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage I Anti.fig');  
saveas(gcf,'Control voltage I Anti.png');
%%  fuck_u_tmp
% figure
% grid on;hold on;
% plot(time, fuck_u_tmp);
% title('Controller u ');
% xlabel('Time(sec)');ylabel('Control(voltage)');
% % legend(''); 
% savefig('Control u.fig');  
% saveas(gcf,'Control u.png');

%% phase diagram
figure
grid on;hold on;
title('phase diagram');
xlabel('Angle(deg)');ylabel('Angular rate(deg/s)');
plot(err, err_d, 'b');
plot(err(end), err_d(end), 'r*')