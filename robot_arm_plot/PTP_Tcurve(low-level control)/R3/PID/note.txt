Vmax 36.0
Amax 144.0
sample time 0.02
0->90
pmotor->controlMax = 4095* 0.7
pmotor->lowerWidth = 4095* 0.05
pmotor->integral_max = 4095*0.1
PID
kp ki kd kb
gu = 4095*0.5;

0.2 0.1 1.0 0.2 tt 
0.2 0.1 1.0 0.2 tt2 scurve

I忘記乘dt, D忘記除dt
pmotor->integral = pmotor->integral + pmotor->ki*pmotor->error - pmotor->integral_anti;
float d = pmotor->kd * pmotor->error_diff ;
0 -> 90
0.2 0.1 1.0 0.2 tt3 Tcurve  PID
0.2 0.0 1.0 0.0 tt4 Tcurve  PD
0.2 0.1 1.0 0.2 tt5 Tcurve  PD Sliding value + I
0.2 0.0 1.0 0.0 tt6 Tcurve  PD Sliding value
0 -> 90
0.2 0.1 1.0 0.2 tt8 Scurve  PID
0.2 0.0 1.0 0.0 tt9 Scurve  PD
0.2 0.1 1.0 0.2 tt10 Scurve  PD Sliding value + I
0.2 0.0 1.0 0.0 tt11 Scurve  PD Sliding value

I有乘dt, D有除dt  //感覺沒差 最後懶得做
pmotor->integral = pmotor->integral + pmotor->ki*pmotor->error*sample_time - pmotor->integral_anti;
float d = pmotor->kd * pmotor->error_diff * control_freq;
0 -> 90
0.2 0.1 0.02 0.2 tt12 Tcurve  PID
0.2 0.0 0.02 0.0 tt13 Tcurve  PD
0.2 0.1 0.02 0.2 tt14 Tcurve  PD Sliding value + I
0.2 0.0 0.02 0.0 tt15 Tcurve  PD Sliding value
0 -> 90
0.2 0.1 0.02 0.2 tt16 Scurve  PID
0.2 0.0 0.02 0.0 tt17 Scurve  PD
0.2 0.1 0.02 0.2 tt18 Scurve  PD Sliding value + I
0.2 0.0 0.02 0.0 tt19 Scurve  PD Sliding value

0.2 0.1 1.0 0.11 tt7 Tcurve  PD Sliding value + I

------------------------------------------------
Final test, 控制量無正負
0->90
0.2 0.1 1.0 0.2 tt3 Tcurve  PID
0.2 0.1 0.02 0.2 tt16 Scurve  PID
.35 .35 .7 ft3 Tcurve FSMC Abs
.35 .3 .7 ft4 Scurve FSMC Abs

Final test2, 控制量有正負
float RoughRel = pmotor->deg2count * 1.0; 
0->90
0.2 0.08 0.8 0.1 rt  Tcurve  PID
0.2 0.08 0.8 0.1 rt2 Scurve  PID
.35 .35 .7 rt3 Tcurve FSMC Abs  //同參數跟之前比起來速度差很多  可能是我起始點又變了
.35 .35 .7 rt4 Scurve FSMC Abs
-----------------------------------------
Vmax 36.0
Amax 144.0
sample time 0.02
0->90
pmotor->controlMax = 4095* 0.7
pmotor->lowerWidth = 4095* 0.05
pmotor->integral_max = 4095*0.1
------------------------------------------
static void AbsFSMC(PtrMotor pmotor)
{
//  pmotor->controlVal = (pmotor->lamda) * pmotor->error + (pmotor->lamda_d) * (pmotor->error_diff);
  float RoughRel = pmotor->deg2count;     
  float ThinSlid = RoughRel*0.05; // 精調gain 
  float sliding_value = (pmotor->lamda_d) * (pmotor->error_diff) + (pmotor->lamda) * pmotor->error;    
  float gs = 0.0; //gs_tmp
  float gu = pmotor->gu_rel_scale * pmotor->controlMax;  //每次變化量為controlMax 的 XX %
  float u = 0.0;
  
  if (abs(sliding_value) > RoughRel )
  {
    gs = RoughRel;
  }
  else 
  {
    if (abs(sliding_value) < ThinSlid)
    {   
      sliding_value = 0.0;
      gu = 0.0;
      gs = 1.0;
    }   
    else
    {
      gs = abs(sliding_value)+ThinSlid;
      gu = exp(abs(gs)/RoughRel-1)*gs;
    }      
  }  

  if(pmotor->FMSC_rule_type == FSMC_ruleSym)
  ...
  ...
  ...
  IOWR_ALTERA_AVALON_PIO_DATA(pmotor->pwm_base, pmotor->controlVal);
}
--------------------------------------
  //sliding mode control defuzzy
  float rule[11]={-1.0,-0.8,-0.6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0};
--------------------------------------