clc ; clear; close all;  
sample_time = 0.02; 
% deg2cnt = 22459.73;  %22459.73, 40.13, 2628.27, 2628.27, 12.33, 12.33
% deg2cnt = 40.13;
deg2cnt = 2628.27;
% deg2cnt = 12.33;
% bit2volt = 24/255;  % 24/255, 12/65535, 24/4095, 24/4095, 24/65535, 24/65535
% bit2volt = 12/65535;  
bit2volt =  24/4095; 
% bit2volt = 24/65535;  
a =load('R4motor.txt');
time = a(:,1);   %列行列行
time_j = time(4:1:end,1);
cnt = a(:,2) / deg2cnt;
err = a(:,3) / deg2cnt;
dif = (a(:,4) / deg2cnt)  / sample_time;
err_d = (a(:,5) / deg2cnt) / sample_time;
diff_d = ((a(:,6) / deg2cnt) / sample_time)/ sample_time;
err_diff_d = ((a(:,7) / deg2cnt) / sample_time)/ sample_time;
diff_j = diff(diff_d) ;
diff_j = diff_j(3:1:end,1);
err_diff_j = diff(err_diff_d);
err_diff_j = err_diff_j(3:1:end,1);
% ctrl = a(:,8);
ctrl = a(:,8) * bit2volt;
ctrl_p = a(:,9) * bit2volt;;
ctrl_i = a(:,10) * bit2volt;;
ctrl_d = a(:,11) * bit2volt;;
ctrl_i_anti = a(:,12) * bit2volt;;
ref = err + cnt;
ref_v = err_d + dif;
ref_a = err_diff_d + diff_d;
ref_j = err_diff_j + diff_j;
fuck_u_tmp = a(:,13)* bit2volt;

%% tracking angle 
figure
grid on;hold on;
plot(time, ref);
plot(time, cnt);
title('Tracking Angle');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('Desired', 'Real');  %real or actual
savefig('tracking angle.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking angle.png');  %沒上一步也可输出成png

%% error angle
figure
grid on;hold on;
zero = time(:,1);
zero(:,1) = 0;
plot(time , zero, 'R');
plot(time, err,'B');
title('Error position');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('zero', 'error'); 
savefig('err angle.fig');  
saveas(gcf,'err angle.png');

%% tracking angular velocity
figure
grid on;hold on;
plot(time, ref_v);
plot(time, dif);
title('Tracking Angular Velocity');
xlabel('Time(sec)');ylabel('Angular Velocity(deg/s)');
legend('Desired', 'Real');  %real or actual
savefig('tracking angular velocity.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking angular velocity.png');  %沒上一步也可输出成png

%% error angular velocity
figure
grid on;hold on;
zero = time(:,1);
zero(:,1) = 0;
plot(time , zero, 'R');
plot(time, err_d,'B');
title('Error Angular rate');
xlabel('Time(sec)');ylabel('Angular rate(deg/s)');
legend('zero', 'err vel'); 
savefig('err angular velocity.fig');  
saveas(gcf,'err angular velocity.png');

%% tracking angular acceleration
% figure
% grid on;hold on;
% plot(time, ref_a);
% plot(time, diff_d);
% title('Tracking Angular Acceleration');
% xlabel('Time(sec)');ylabel('Angular Acceleration(deg/s^2)');
% legend('Desired', 'Real');  %real or actual
% savefig('tracking angular acceleration.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
% saveas(gcf,'tracking angular acceleration.png');  %沒上一步也可输出成png
% 
% %% error angular acceleration
% figure
% grid on;hold on;
% zero = time(:,1);
% zero(:,1) = 0;
% plot(time , zero, 'R');
% plot(time, err_diff_d,'B');
% title('Error Angular Acceleration');
% xlabel('Time(sec)');ylabel('Angular Acceleration(deg/s^2)');
% legend('zero', 'err acc'); 
% savefig('err angular acceleration.fig');  
% saveas(gcf,'err angular acceleration.png');
% 
% %% tracking jerk
% figure
% grid on;hold on;
% plot(time_j, ref_j);
% plot(time_j, diff_j);
% title('Tracking Jerk');
% xlabel('Time(sec)');ylabel('Angular Jerk(deg/s^3)');
% legend('Desired', 'Real');  %real or actual
% savefig('tracking jerk.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
% saveas(gcf,'tracking jerk.png');  %沒上一步也可输出成png
% 
% %% error jerk
% figure
% grid on;hold on;
% zero = time_j(:,1);
% zero(:,1) = 0;
% plot(time_j , zero, 'R');
% plot(time_j, err_diff_j,'B');
% title('Error Jerk');
% xlabel('Time(sec)');ylabel('Angular Jerk(deg/s^3)');
% legend('zero', 'err jerk'); 
% savefig('err jerk.fig');  
% saveas(gcf,'err jerk.png');

%% Control value
figure
grid on;hold on;
plot(time, ctrl);
title('Control voltage');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage.fig');  
saveas(gcf,'Control voltage.png');
% %% Control value P
figure
grid on;hold on;
plot(time, ctrl_p);
title('Control voltage P');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage P.fig');    
saveas(gcf,'Control voltage P.png');

% Control value I
figure
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           grid on;hold on;
plot(time, ctrl_i);
title('Control voltage I');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage I.fig');  
saveas(gcf,'Control voltage I.png');

% Control value D
figure
grid on;hold on;
plot(time, ctrl_d);
title('Control voltage D');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage D.fig');  
saveas(gcf,'Control voltage D.png');

% Control value I_anti
figure
grid on;hold on;
plot(time, ctrl_i_anti);
title('Control voltage I Anti');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage I Anti.fig');  
saveas(gcf,'Control voltage I Anti.png');

%%  fuck_u_tmp
% figure
% grid on;hold on;
% plot(time, fuck_u_tmp, 'x-');
% title('Controller u ');
% xlabel('Time(sec)');ylabel('Control(voltage)');
% % legend(''); 
% savefig('Control u.fig');  
% saveas(gcf,'Control u.png');

%% phase diagram
figure
grid on;hold on;
title('phase diagram');
xlabel('Angle(deg)');ylabel('Angular rate(deg/s)');
plot(err, err_d, 'b');
plot(err(end), err_d(end), 'r*')

% figure
% grid on;hold on;
% plot(time, ctrl_p+ctrl_d);

%% sliding value
% figure
% grid on;hold on;
% title('sliding value');
% xlabel('Time(sec)');ylabel('S');
% three = time(:,1);
% three(:,1) = deg2cnt*0.5/deg2cnt;
% err = err*deg2cnt/deg2cnt;
% err_d = err_d*deg2cnt*sample_time/deg2cnt;
% plot(time, 0.5*err+7*err_d, 'b');
% plot(time, three*0.2, 'r');
% plot(time, -three*0.2, 'r');
% plot(time, three, 'g');
% plot(time, -three, 'g');
% % plot(time, (err+3.0*err_d)/1+3, 'b');
% % plot(err(end), err_d(end), 'r*')
% savefig('sliding value.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
% saveas(gcf,'sliding value.png');  %沒上一步也可输出成png
% 
% %%
% lamda = 0.01;
% lamda_d = 0.1;
% x = lamda*err + lamda_d*err_d;
% gs = 300;
% gu = 4095*0.7*0.06;
% for i=1:length(x) 
%   for j=1:11
%     v(j) = 0;
%   end
%   if(x(i)>=1.0*gs)
%     v(11)=1.0;
%   elseif(x(i)>=0.8*gs) && (x(i)<=1.0*gs)  
%     v(11)=5.0/gs*x(i)-4.0;
%     v(10)=-5.0/gs*x(i)+5.0;  
%   elseif(x(i)>=0.6*gs && x(i)<=0.8*gs)  
%     v(10)=5.0/gs*x(i)-3.0;
%     v(9)=-5.0/gs*x(i)+4.0;  
%   elseif(x(i)>=0.4*gs && x(i)<=0.6*gs)  
%     v(9)=5.0/gs*x(i)-2.0;
%     v(8)=-5.0/gs*x(i)+3.0;  
%   elseif(x(i)>=0.2*gs && x(i)<=0.4*gs)  
%     v(8)=5.0/gs*x(i)-1.0;
%     v(7)=-5.0/gs*x(i)+2.0;  
%   elseif(x(i)>=0.0*gs && x(i)<=0.2*gs)  
%     v(7)=5.0/gs*x(i)-0.0;
%     v(6)=-5.0/gs*x(i)+1.0;  
%   elseif(x(i)>=-0.2*gs && x(i)<=0.0*gs)  
%     v(6)=5.0/gs*x(i)+1.0;
%     v(5)=-5.0/gs*x(i)-0.0;  
%   elseif(x(i)>=-0.4*gs && x(i)<=-0.2*gs)  
%     v(5)=5.0/gs*x(i)+2.0;
%     v(4)=-5.0/gs*x(i)-1.0;  
%   elseif(x(i)>=-0.6*gs && x(i)<=-0.4*gs)  
%     v(4)=5.0/gs*x(i)+3.0;
%     v(3)=-5.0/gs*x(i)-2.0;  
%   elseif(x(i)>=-0.8*gs && x(i)<=-0.6*gs)  
%     v(3)=5.0/gs*x(i)+4.0;
%     v(2)=-5.0/gs*x(i)-3.0;  
%   elseif(x(i)>=-1.0*gs && x(i)<=-0.8*gs)  
%     v(2)=5.0/gs*x(i)+5.0;
%     v(1)=-5.0/gs*x(i)-4.0;  
%   elseif(x(i)<=-1.0*gs)
%     v(1)=1.0;
%   end
%   v;
%   rule=[-1.0,-0.8,-0.6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0];
%   weight = 0;
%   weight_sum = 0;
%   for j=1:11
%       if(v(j)>0.0)
%           weight = weight + v(j);
%           weight_sum = weight_sum + v(j)*rule(j);
%       end
%   end
%   w(i,1) = weight;
%   ws(i,1) = weight_sum;
%   ww(i,1) = ws(i,1)/w(i,1);
%   u =(weight_sum/weight)*gu;
%                                     
% end