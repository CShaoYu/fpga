ControlMax 4095*0.7
Vmax 36.0
Amax 36.0
sample time 0.02
0->90
integral_max = 4095*0.1

kp ki kd kb
1
0.3 0.08 0.4 0.1
2
0.3 0.08 0.6 0.1
3
0.3 0.08 0.8 0.1

else if (pmotor->integral < -pmotor->integral_max)
4
0.3 0.08 0.4 0.2
5
0.3 0.08 0.45 0.2
6
0.2 0.08 0.8  0.1

Scurve
7
0.3 0.08 0.4 0.2
8
0.3 0.08 0.6 0.1