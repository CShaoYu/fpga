clc ; clear; close all;
sample_time = 0.02; 
deg2cnt = 22459.73;  %22459.73, 40.13, 2628.27, 2628.27, 12.33, 12.33
% deg2cnt = 40.13;
% deg2cnt = 2628.27;
% deg2cnt = 12.33;
bit2volt = 24/255;  % 24/255, 12/65535, 24/4095, 24/4095, 24/65535, 24/65535
% bit2volt = 12/65535;  
% bit2volt =  24/4095; 
% bit2volt = 24/65535;  
a =load('R1motor.txt');
time = a(:,1);   %列行列行
time_v = time(2:1:end,1);
time_a = time(3:1:end,1);
time_j = time(4:1:end,1);
cnt = a(:,2) / deg2cnt;
err = a(:,3) / deg2cnt;
ref = err + cnt;
ref_v = diff(ref) / sample_time;
ref_a = diff(ref_v) / sample_time;
ref_j = diff(ref_a) / sample_time;
dif = (a(:,4) / deg2cnt) / sample_time;
dif = dif(2:1:end,:);
err_d = ref_v - dif;
diff_d = ((a(:,6) / deg2cnt) / sample_time)/ sample_time;
diff_d = diff_d(3:1:end,:);
err_diff_d = ref_a - diff_d;
diff_j = diff(diff_d);
err_diff_j = ref_j - diff_j;
ctrl = a(:,8);

%% tracking angle 
figure
grid on;hold on;
plot(time, ref);
plot(time, cnt);
title('Tracking Angle');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('Desired', 'Real');  %real or actual
savefig('tracking angle.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking angle.png');  %沒上一步也可输出成png

%% error angle
figure
grid on;hold on;
zero = time(:,1);
zero(:,1) = 0;
plot(time , zero, 'R');
plot(time, err,'B');
title('Error position');
xlabel('Time(sec)');ylabel('Angle(deg)');
legend('zero', 'error'); 
savefig('err angle.fig');  
saveas(gcf,'err angle.png');

%% tracking angular velocity
figure
grid on;hold on;
plot(time_v, ref_v);
plot(time_v, dif);
title('Tracking Angular Velocity');
xlabel('Time(sec)');ylabel('Angular Velocity(deg/s)');
legend('Desired', 'Real');  %real or actual
savefig('tracking angular velocity.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking angular velocity.png');  %沒上一步也可输出成png

%% error angular velocity
figure
grid on;hold on;
zero = time_v(:,1);
zero(:,1) = 0;
plot(time_v, zero, 'R');
plot(time_v, err_d,'B');
title('Error Angular rate');
xlabel('Time(sec)');ylabel('Angular rate(deg/s)');
legend('zero', 'err vel'); 
savefig('err angular velocity.fig');  
saveas(gcf,'err angular velocity.png');

%% tracking angular acceleration
figure
grid on;hold on;
plot(time_a, ref_a);
plot(time_a, diff_d);
title('Tracking Angular Acceleration');
xlabel('Time(sec)');ylabel('Angular Acceleration(deg/s^2)');
legend('Desired', 'Real');  %real or actual
savefig('tracking angular acceleration.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking angular acceleration.png');  %沒上一步也可输出成png

%% error angular acceleration
figure
grid on;hold on;
zero = time_a(:,1);
zero(:,1) = 0;
plot(time_a , zero, 'R');
plot(time_a, err_diff_d,'B');
title('Error Angular Acceleration');
xlabel('Time(sec)');ylabel('Angular Acceleration(deg/s^2)');
legend('zero', 'err acc'); 
savefig('err angular acceleration.fig');  
saveas(gcf,'err angular acceleration.png');

%% tracking jerk
figure
grid on;hold on;
plot(time_j, ref_j);
plot(time_j, diff_j);
title('Tracking Jerk');
xlabel('Time(sec)');ylabel('Angular Jerk(deg/s^3)');
legend('Desired', 'Real');  %real or actual
savefig('tracking jerk.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'tracking jerk.png');  %沒上一步也可输出成png

%% error jerk
figure
grid on;hold on;
zero = time_j(:,1);
zero(:,1) = 0;
plot(time_j , zero, 'R');
plot(time_j, err_diff_j,'B');
title('Error Jerk');
xlabel('Time(sec)');ylabel('Angular Jerk(deg/s^3)');
legend('zero', 'err jerk'); 
savefig('err jerk.fig');  
saveas(gcf,'err jerk.png');

%% Control value
figure
grid on;hold on;
plot(time, ctrl*bit2volt);
title('Control voltage');
xlabel('Time(sec)');ylabel('Control(voltage)');
% legend(''); 
savefig('Control voltage.fig');  
saveas(gcf,'Control voltage.png');

%% phase diagram
% figure
% grid on;hold on;
% title('phase diagram');
% xlabel('Angle(deg)');ylabel('Angular rate(deg/s)');
% plot(err, err_d, 'b');
% plot(err(end), err_d(end), 'r*')